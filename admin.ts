// Pro list
// const proList = [
//   "cYBDK8rDdpSzzPBdK", //Vaan yann.rebourg@gmail.com
//   "CZkx3H8St4gXWoow7", //Just contact@j-assurances.fr
//   // "6GQ6gfMuHYZpEkJTu", //Arona
// ];

// export let isProFunction = () => {
//   const userId = Meteor.userId();
//   const local = Meteor.absoluteUrl() == "http://localhost:3000/";
//   if (local) {
//     console.log("local");
//     return true;
//   } else {
//     return proList.includes(userId);
//   }
// };

//Not in use at the moment
export let isProFunction = async (userId) => {
  const user = await Meteor.userAsync();
  return user?.roles?.includes("pro");
};

export let isAdminFunction = () => {
  const userId = Meteor.userId();
  const local = Meteor.absoluteUrl() == "http://localhost:3000/";
  if (local) {
    return true;
  } else {
    return adminList.includes(userId);
  }
};

//TO DO: create a function isProServer that returns Meteor.user(id).roles.pro ==true (so we can stock the info on who is pro in the DB)
// Problem, we need to subscribe to the user.roles, if the onmount subscription is on the adminlayout or prolayout everything ,
// the subscription will need to occurs and look not professional.

// const adminList = [
//   "cYBDK8rDdpSzzPBdK", //Vaan yann.rebourg@gmail.com
// ];

export let isAdminServer = async () => {
  const user = await Meteor.userAsync();
  return user.roles?.includes("admin");
};

export const isAdminOrlocal = async () => {
  if (Meteor.absoluteUrl() == "http://localhost:3000/") return true;
  const user = await Meteor.userAsync({ fields: { roles: 1 } });
  return user?.roles?.includes("admin");
};

export const adminOrlocalAuthorization = async () => {
  // if (Meteor.absoluteUrl() == "http://localhost:3000/") return;
  const user = await Meteor.userAsync({ fields: { roles: 1 } });
  if (!user?.roles?.includes("admin")) {
    console.log("not-authorized");
  }
};

export const proOrlocalAuthorization = async () => {
  // if (Meteor.absoluteUrl() == "http://localhost:3000/") return;
  const thisuser = await Meteor.userAsync({ fields: { roles: 1 } });
  if (thisuser?.roles?.includes("pro")) return;
  console.log("not-authorized");
};

export const proOrlocalAuthorizationBoolean = async () => {
  if (Meteor.absoluteUrl() == "http://localhost:3000/") return true;

  const thisuser = await Meteor.userAsync({ fields: { roles: 1 } });

  if (thisuser?.roles?.includes("pro")) {
    return true;
  }
};

export const loggedInAuthorization = () => {
  if (Meteor.absoluteUrl() != "http://localhost:3000/" && !Meteor.userId()) {
    throw new Meteor.Error("not-authorized");
  }
};

//Old
// export let isAdminServer = (userId) => {
//   const local = Meteor.absoluteUrl() == "http://localhost:3000/";
//   if (local) {
//     // console.log("local");
//     return true;
//   } else {
//     return adminList.includes(userId);
//   }
// };
