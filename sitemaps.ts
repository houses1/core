// https://github.com/gadicc/meteor-sitemaps

import { Assets_sale } from "./api/assets_sale.js";
import { Assets_rent } from "./api/assets_rent.js";
import { Blogs } from "./api/blogs.js";
import sitemap_customization from "../sitemap_customization";
const { placeAds } = sitemap_customization;

sitemaps.add("/sitemap.xml", function () {
  // required: page
  // optional: lastmod, changefreq, priority, xhtmlLinks, images, videos

  let saleAssets = Assets_sale.find({ isVisible: true });
  let rentAssets = Assets_rent.find({ isVisible: true });
  let blogs = Blogs.find({ isVisible: true });

  const users = Meteor.users
    .find({ roles: { $in: ["pro"] }, contact: { $exists: true } })
    .fetch();

  out = [
    { page: "/contact", lastmod: "2024-08-15" },

    //   { page: '/z', lastmod: new Date().getTime(), changefreq: 'monthly', priority: 0.8 },
    // https://support.google.com/webmasters/answer/178636?hl=en

    //////////
    //   { page: '/pageWithViedeoAndImages',

    //////Be careful -> The images MUST be store in the server with the domain name.
    //     images: [
    //       { loc: '/myImg.jpg', },        // Only loc is required
    //       { loc: '/myOtherImg.jpg',      // Below properties are optional
    //         caption: "..", geo_location: "..", title: "..", license: ".."}
    //     ],
    //     videos: [
    //       { loc: '/myVideo.jpg', },      // Only loc is required
    //       { loc: '/myOtherVideo.jpg',    // Below properties are optional
    //         thumbnail_loc: "..", title: "..", description: ".." etc }
    //     ]
    //   },
    //////////
    // https://support.google.com/webmasters/answer/2620865?hl=en
    //   { page: 'lang/english', xhtmlLinks: [
    //     { rel: 'alternate', hreflang: 'de', href: '/lang/deutsch' },
    //     { rel: 'alternate', hreflang: 'de-ch', href: '/lang/schweiz-deutsch' },
    //     { rel: 'alternate', hreflang: 'en', href: '/lang/english' }
    //   ]}
  ];

  if (placeAds == true) {
    out.push({ page: "/placeAds", lastmod: "2022-01-10" });
  }

  saleAssets.forEach((page) => {
    const id = page.slug || page._id;
    out.push({
      page: "for-sale/" + id,
      lastmod: page.modifiedAt || page.createdAt,
    });
  });

  rentAssets.forEach((page) => {
    const id = page.slug || page._id;
    out.push({
      page: "for-rent/" + id,
      lastmod: page.modifiedAt || page.createdAt,
    });
  });

  blogs.forEach((blog) => {
    const id = blog.slug || blog._id;
    out.push({
      page: "blog/" + id,
      lastmod: blog.modifiedAt || blog.createdAt,
    });
  });

  //users
  users.forEach((user) => {
    const id = user.username || user._id;

    if (user.isReferenced && !user.isHidden) {
      out.push({
        page: "conseiller-immobilier/" + id,
        lastmod: user.createdAt,
      });
    }
  });

  return out;
});
