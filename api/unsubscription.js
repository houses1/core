import { Searches } from "./searches.js";
import { Contacts } from "./contacts.js";
// Plus also need to check in the profile of the email

Meteor.methods({
  async unsubscribe(email) {
    const contacts = Contacts.find({ emails: [email] }).fetch();
    contacts.forEach(async (contact) => {
      await Meteor.callAsync("contact.update.hasUnsubscribed", {
        id: contact._id,
      });
    });

    const searches = Searches.find({ email: email }).fetch();
    searches.forEach(async (search) => {
      await Meteor.callAsync("searches.update.hasUnsubscribed", {
        id: search._id,
      });
    });
  },
});
