import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Prospects = new Mongo.Collection("prospects");

if (Meteor.isServer) {
  // This code only runs on the server

  Meteor.publish("prospects", function publication() {
    return Prospects.find();
  });

  Meteor.publish("myprospects", function publication() {
    return Prospects.find({ createdBy: Meteor.userId() });
  });

  Meteor.publish("prospectsandowners", function publication() {
    const prospects = Prospects.find();
    const users = Meteor.users.find();
    return [prospects, users];
  });

  Meteor.publish("myprospectsandowners", function publication() {
    const prospects = Prospects.find({
      $or: [
        { createdBy: Meteor.userId() },
        { sharedWith: Meteor.userId() },
        { userId: Meteor.userId() },
      ],
    });

    const users = Meteor.users.find();
    return [prospects, users];
  });

  Meteor.publish("prospects_sorted", function publication(limit) {
    return Prospects.find({}, { sort: { publication_date: -1 }, limit: limit });
  });

  Meteor.publish("visible_prospects", function publication() {
    return Prospects.find({ isVisible: true });
  });

  Meteor.publish("prospect", function publication(id) {
    const result = Prospects.find({ _id: id });
    return result;
  });
}

Meteor.methods({
  "prospect.insert.creation"(prospectObject) {
    const date = new Date();
    return Prospects.insertAsync({
      gender: prospectObject.gender,
      surname: prospectObject.surname,
      firstname: prospectObject.firstname,
      emails: [prospectObject.email],
      phone: [prospectObject.phone],
      address: prospectObject.address,
      birthdate: prospectObject.birthdate,
      nationality: prospectObject.nationality,
      createdAt: date,
      createdBy: Meteor.userId(),
      userId: Meteor.userId(),
    });
  },

  "prospect.insert"() {
    const date = new Date();
    return Prospects.insertAsync({
      createdAt: date,
      createdBy: Meteor.userId(),
      userId: Meteor.userId(),
    });
  },

  "prospect.update"({
    id,
    gender,
    surname,
    firstname,
    email,
    phone,
    address,
    birthdate,
    birthplace,
    iban,
    bic,
    representedBy,
    nationality,
    notes,
  }) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        gender,
        surname: surname,
        firstname: firstname,
        emails: [email],
        phone: [phone],
        address: address,
        birth: { birthdate: birthdate, birthplace: birthplace },
        bank: { bic: bic, iban: iban },
        representedBy: representedBy,
        nationality: nationality,
        notes: notes,
      },
    });
  },

  // Duplicate
  async "prospect.duplicate"(id) {
    proOrlocalAuthorization();

    const prospect = await Prospects.findOneAsync(id);
    if (!prospect) return;

    const date = new Date();
    prospect.createdAt = date;

    delete prospect["_id"];
    delete prospect["mandate"];
    delete prospect["status"];

    return Prospects.insertAsync(prospect);
  },

  "prospect.update.title"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "prospect.update.gender"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        gender: value,
      },
    });
  },

  "prospect.update.maritalstatus"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        maritalstatus: value,
      },
    });
  },

  "prospect.update.mandate"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  "prospect.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  "prospect.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Prospects.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "prospect.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();

    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Prospects.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  "prospect.push.type"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $addToSet: {
        type: value,
      },
    });
  },

  "prospect.pull.type"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $pull: {
        type: value,
      },
    });
  },

  "prospects.push.searches"({ id, min, max, city, type, assetType }) {
    proOrlocalAuthorization();
    const createdAt = new Date();

    return Prospects.updateAsync(id, {
      $push: {
        searches: { min, max, city, createdAt, type, assetType },
      },
    });
  },

  async "prospects.pull.searches"({ id, i }) {
    proOrlocalAuthorization();
    const prospect = await Prospects.findOneAsync(id);
    const toberemoved = prospect?.searches?.[i];
    if (!toberemoved) return;

    return Prospects.updateAsync(id, {
      $pull: {
        searches: toberemoved,
      },
    });
  },

  "prospect.update.address"(id, postalCode, streetAddress, city, country = "") {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "prospect.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "prospect.push.category"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "prospect.update.description"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  // Add security
  "prospect.remove"(id) {
    proOrlocalAuthorization();
    return Prospects.removeAsync({ _id: id });
  },

  //Boolean
  "prospect.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Prospects.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
