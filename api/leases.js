import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Leases = new Mongo.Collection("leases");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("leases", function publication() {
    return Leases.find();
  });

  Meteor.publish("currentLeases", function publication() {
    return Leases.find({
      isArchived: { $ne: true },
    });
  });

  Meteor.publish("myleases", function publication() {
    return Leases.find({ userId: Meteor.userId() });
  });

  Meteor.publish("leases_owner", function publication() {
    const items = Leases.find();
    const users = Meteor.users.find();
    return [items, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("leases_list", function publication() {
    return Leases.find();
  });

  Meteor.publish("visible_leases", function publication() {
    return Leases.find({ isVisible: true });
  });

  Meteor.publish("for_lease", function publication(id) {
    const result = Leases.find({ _id: id });
    return result;
  });

  Meteor.publish("for_lease_slug", function publication(id) {
    let result = Leases.find({ slug: id }, { limit: 1 });
    if (result.count() == 0) {
      result = Leases.find({ _id: id }, { limit: 1 });
    }
    return result;
  });
}

Meteor.methods({
  async "lease.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();
    const contactAgent = user?.contact;

    return Leases.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      // mandate: mandate,
      contact: {
        agent: contactAgent,
      },
    });
  },

  "lease.update.title"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "lease.update.asset"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        asset: value,
      },
    });
  },

  "lease.unset.asset"(id) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $unset: {
        asset: "",
      },
    });
  },

  "lease.update.type"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  // "lease.update.mandate"(id, value) {
  //   proOrlocalAuthorization();
  //   return Leases.updateAsync(id, {
  //     $set: {
  //       mandate: value,
  //     },
  //   });
  // },

  // "lease.push.images"(id, value) {
  //   proOrlocalAuthorization();
  //   let result = [];
  //   value = value.forEach((e) => {
  //     result.push({ title: "", url: e });
  //   });

  //   return Leases.updateAsync(id, {
  //     $push: {
  //       images: {
  //         $each: result,
  //       },
  //     },
  //   });
  // },

  // "lease.update.images.title"(id, i, value) {
  //   proOrlocalAuthorization();
  //   // let result = [];
  //   // value = value.forEach(e => {
  //   //     result.push({ title: "", url: e })
  //   // });

  //   let update = { $set: {} };
  //   update["$set"]["images." + i + ".title"] = value;

  //   return Leases.updateAsync(id, update);
  // },

  // "leases.pull.images"(id, value) {
  //   proOrlocalAuthorization();
  //   return Leases.updateAsync(
  //     id,
  //     { $pull: { images: { url: value } } },
  //     false,
  //     false
  //   );
  // },

  "lease.update.address"(id, postalCode, streetAddress, city, country = "") {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "lease.update.tenant"({
    id,
    name,
    phone,
    picture,
    email,
    birthplace,
    birthdate,
    iban,
    bic,
    officialId,
    socialSecurity,
    nationality,
    notes,
  }) {
    proOrlocalAuthorization();

    return Leases.updateAsync(id, {
      $set: {
        tenant: {
          name,
          phone,
          picture,
          email,
          birthplace,
          birthdate,
          bank: { iban: iban, bic: bic },
          officialId,
          socialSecurity,
          nationality,
          notes,
        },
      },
    });
  },

  "lease.update.landlordSelect"(id, object) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        landlord: object,
      },
    });
  },

  "lease.unset.landlord"(id) {
    proOrlocalAuthorization();

    return Leases.updateAsync(id, {
      $unset: {
        landlord: "",
      },
    });
  },

  "lease.update.landlord"(
    id,
    name,
    phone,
    address,
    picture,
    email,
    birthplace,
    birthdate,
    iban,
    bic,
    representedBy
  ) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        landlord: {
          name: name,
          phone: phone,
          picture: picture,
          address: address,
          email: email,
          birthplace: birthplace,
          birthdate: birthdate,
          bank: { iban: iban, bic: bic },
          representedBy: representedBy,
        },
      },
    });
  },

  "lease.update.rentpermonth"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        rentpermonth: value,
      },
    });
  },

  "lease.update.rental_charges"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        rental_charges: value,
      },
    });
  },

  "lease.update.furnitureInventory"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        furnitureInventory: value,
      },
    });
  },

  "lease.update.assetCondition.entry"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        "assetCondition.entry": value,
      },
    });
  },

  "lease.update.assetCondition.exit"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        "assetCondition.exit": value,
      },
    });
  },

  "lease.update.deposit"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        deposit: Number(value),
      },
    });
  },

  "lease.update.visale"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        visale: value,
      },
    });
  },

  "lease.update.workStartDate"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        workStartDate: value,
      },
    });
  },

  "lease.update.startingDate"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        startingDate: value,
      },
    });
  },

  "lease.update.paymentDate"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        paymentDate: value,
      },
    });
  },

  "lease.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "lease.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "leases.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "leases.push.category"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "lease.update.description"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "lease.update.rentReferenceIndex"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        rentReferenceIndex: value,
      },
    });
  },

  "lease.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "leases.duplicate"(id) {
    proOrlocalAuthorization();
    const lease = Leases.findOneAsync(id);
    if (lease) {
      return Leases.insertAsync({
        rentpermonth: lease.rentpermonth,
        landlord: lease.landlord,
        tenant: lease.tenant,
        userId: Meteor.userId(),
      });
    }
  },

  "leases.archive"(id, value) {
    proOrlocalAuthorization();
    return Leases.updateAsync(id, {
      $set: {
        isArchived: true,
      },
    });
  },

  "leases.remove"(id, value) {
    proOrlocalAuthorization();
    return Leases.removeAsync(id);
  },
});
