import { Mandates_For_Sale } from "./mandates_sale.js";
import { Mandates_For_Rent } from "./mandates_rent.js";
import { Mandates } from "./mandates.js";

// In Use in France by MSM
// EX: "1, 2, 3"
export const generateMandateNumber_NN = async () => {
  if (!Meteor.isServer) return;

  // Get the maximum (generic) mandate number for rent
  let MandateMax = await Mandates.findOneAsync(
    { "mandate.number": { $exists: true } },
    { sort: { "mandate.number": -1 } }
  );

  MandateMax = MandateMax?.mandate?.number || 0;

  // Get the maximum mandate number for rent
  let rentMandateMax = await Mandates_For_Rent.findOneAsync(
    { "mandate.number": { $exists: true } },
    { sort: { "mandate.number": -1 } }
  );

  rentMandateMax = rentMandateMax?.mandate?.number || 0;

  // Get the maximum mandate number for sale
  let saleMandateMax = await Mandates_For_Sale.findOneAsync(
    { "mandate.number": { $exists: true } },
    { sort: { "mandate.number": -1 } }
  );

  saleMandateMax = saleMandateMax?.mandate?.number || 0;

  const mandateCount = Math.max(saleMandateMax, rentMandateMax, MandateMax) + 1;

  return String(mandateCount);
};

// IN USE PF for Anoa, Lexinesia
// EX: "22-01, 22-02, 22-03"
export const generateMandateNumber_YY_NN = async () => {
  if (!Meteor.isServer) return; // We need to be in the server side for counting

  const year = new Date().getFullYear().toString().slice(-2);

  const MandateCount = await Mandates.find({
    "mandate.number": { $regex: new RegExp("^" + year) },
  }).countAsync();

  const rentMandateCount = await Mandates_For_Rent.find({
    "mandate.number": { $regex: new RegExp("^" + year) },
  }).countAsync();

  const saleMandateCount = await Mandates_For_Sale.find({
    "mandate.number": { $regex: new RegExp("^" + year) },
  }).countAsync();

  let mandateCount = String(
    saleMandateCount + rentMandateCount + MandateCount + 1
  );

  if (mandateCount.length == 1) {
    mandateCount = "0" + mandateCount;
  }

  let mandateNumber = String(year) + "-" + mandateCount;

  return mandateNumber;
};
