import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";
import enterprise_customization from "../../enterprise_customization.ts";
import { Contacts } from "./contacts.js";

export const Mandates = new Mongo.Collection("mandates");

import { Mandates_For_Sale } from "./mandates_sale.js";
import { Mandates_For_Rent } from "./mandates_rent.js";

// This code only runs on the server
if (Meteor.isServer) {
  Meteor.publish("mandates_from_asset", function publication(assetId) {
    return Mandates.find({
      "asset.value": assetId,
    });
  });

  Meteor.publish("mandates_all", function publication() {
    return Mandates.find();
  });

  Meteor.publish("mandates_seller", function publication(id) {
    return Mandates.find({ seller: id });
  });

  Meteor.publish("mandates_record", function publication() {
    return Mandates.find({ mandate: { $exists: true } });
  });

  Meteor.publish("mandates_statistics", function publication(year) {
    let result;
    if (year) {
      result = Mandates.find({
        mandate: { $exists: true },
        createdAt: {
          $gte: new Date(year, 1, 1, 0, 0, 0),
          $lt: new Date(Number(year) + 1, 1, 1, 0, 0, 0),
        },
      });
    } else {
      result = Mandates.find({ mandate: { $exists: true } });
    }

    return result;
  });

  Meteor.publish("my_mandates", function publication() {
    return Mandates.find({ userId: Meteor.userId() });
  });

  Meteor.publish("mandates_confirmed", function publication() {
    return Mandates.find({ mandate: { $exists: true } });
  });

  Meteor.publish("mandates_owner", function publication() {
    const mandates = Mandates.find();
    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });

  Meteor.publish("my_mandates_owner", function publication() {
    const mandates = Mandates.find({
      $or: [
        { userId: Meteor.userId() },
        { sharedWith: { $in: [Meteor.userId()] } },
      ],
    });

    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("mandates_list", function publication() {
    return Mandates.find();
  });

  // Meteor.publish("visible_mandates", function publication() {
  //   return Mandates.find({ isVisible: true });
  // });
  // Meteor.publish("mandate", function publication(id) {
  //   const result = Mandates.find({ _id: id });
  //   return result;
  // });

  Meteor.publish("mandate_id", function (id) {
    return Mandates.find({ _id: id }, { limit: 1 });
  });

  Meteor.publish("mandate_number_id", function (id) {
    return Mandates.find({ _id: id }, { limit: 1, fields: { mandate: 1 } });
  });

  Meteor.publish("mandate_id_protected", async function (id) {
    const user = await Meteor.userAsync();
    if (user?.roles?.includes("admin")) {
      result = Mandates.find({ _id: id }, { limit: 1 });
    } else {
      result = Mandates.find(
        {
          _id: id,
          $or: [
            { userId: Meteor.userId() },
            { sharedWith: { $in: [Meteor.userId()] } },
          ],
        },
        { limit: 1 }
      );
    }
    return result;
  });

  Meteor.publish("mandate_id_complete", function publication(id) {
    let mandate = Mandates.find({ _id: id }, { limit: 1 }).fetch();

    let contacts;
    let contactsArray = [];
    let asset = [];

    if (!mandate?.[0]) return;
    if (mandate[0].seller) {
      contactsArray.push(mandate[0].seller);
    }
    if (mandate[0].buyer) {
      contactsArray.push(mandate[0].buyer);
    }
    if (mandate[0].buyerNotary) {
      contactsArray.push(mandate[0].buyerNotary);
    }

    asset = Assets_sale.find({ _id: mandate[0]?.asset?.value }, { limit: 1 });

    mandate = Mandates.find({ _id: id }, { limit: 1 });
    contacts = Contacts.find({ _id: { $in: contactsArray } });

    return [mandate, contacts, asset];
  });

  Meteor.publish("mandate_arrayid", function publication(array) {
    let result = Mandates.find({ _id: { $in: array } });
    // if (result.count() == 0) {
    //   result = Mandates.find({ _id: id }, { limit: 1 });
    // }
    return result;
  });

  Meteor.publish("mandate_number", function publication(id) {
    let result = Mandates.find({ mandate: id }, { limit: 1 });
    // if (result.count() == 0) {
    //   result = Mandates.find({ _id: id }, { limit: 1 });
    // }
    return result;
  });
}

Meteor.methods({
  async "mandate.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();

    // agent contact
    const contactAgent = user?.contact;

    //Email
    if (contactAgent && !contactAgent.email) {
      contactAgent.email = user?.emails?.[0]?.address;
    }

    return Mandates.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      status: "draft",
      contact: {
        agent: contactAgent,
      },
    });
  },

  // Duplicate
  async "mandate.duplicate"(id) {
    proOrlocalAuthorization();
    const mandate = await Mandates.findOneAsync(id);
    if (!mandate) return;

    const date = new Date();
    mandate.createdAt = date;

    delete mandate["_id"];
    delete mandate["mandate"];
    delete mandate["status"];
    delete mandate["signatureDate"];
    delete mandate["startingDate"];
    delete mandate["deed"];

    return Mandates.insertAsync(mandate);
  },

  async "mandate.insertwithasset"(assetId) {
    proOrlocalAuthorization();
    const date = new Date();
    // agent contact
    const user = await Meteor.userAsync();
    const contactAgent = user?.contact;
    const asset = await Assets_sale.findOneAsync(assetId);

    // Careful because mandate and assets dont use the same .... saleprice object....
    let saleprice;
    if (asset?.saleprice) {
      saleprice = asset?.saleprice;
    }

    const resultId = Mandates.insertAsync({
      // _id: mandate,
      // title: title,
      createdAt: date,
      userId: Meteor.userId(),
      asset: { value: assetId },
      contact: {
        agent: contactAgent,
      },
      saleprice: saleprice,
    });

    await Assets_sale.updateAsync(assetId, {
      $push: {
        mandates: String(resultId),
      },
    });

    return resultId;
  },

  "mandate.update.saleprice"(id, price_feesincluded, price_feesnotincluded) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        "saleprice.fees_included": Number(price_feesincluded),
        "saleprice.fees_not_included": Number(price_feesnotincluded),
      },
    });
  },

  async "mandate.update.isSold"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = Mandates.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isSold", asset, value);
    }

    return Mandates.updateAsync(id, {
      $set: { isSold: value },
    });
  },

  async "mandate.update.isSoldByOthers"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = Mandates.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isSoldByOthers", asset, value);
    }

    return Mandates.updateAsync(id, {
      $set: { isSoldByOthers: value },
    });
  },

  async "mandate.update.isCancelledBySeller"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = Mandates.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync(
        "asset_sale.update.isCancelledBySeller",
        asset,
        value
      );
    }

    return Mandates.updateAsync(id, {
      $set: { isCancelledBySeller: value },
    });
  },

  async "mandate.update.isExpired"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = Mandates.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isVisible", asset, !value);
    }

    return Mandates.updateAsync(id, {
      $set: { isExpired: value },
    });
  },

  "mandate.update.financing"(id, result) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        financing: result,
      },
    });
  },

  // For give
  "mandate.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value)) {
      throw new Meteor.Error("Error", "User not found");
    }

    return Mandates.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // For share
  "mandate.update.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates.updateAsync(id, {
      $set: {
        sharedWith: [value],
      },
    });
  },

  "mandate.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "mandate.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  "mandate.update.furnitureValue"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        furnitureValue: value,
      },
    });
  },

  "mandate.update.feesPaidBy"(id, value) {
    proOrlocalAuthorization();

    return Mandates.updateAsync(id, {
      $set: {
        "saleprice.feesPaidBy": value,
      },
    });
  },

  "mandate.update.startingDate"({ mandateId, date }) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(mandateId, {
      $set: {
        startingDate: date,
      },
    });
  },

  "mandate.unset.startingDate"({ mandateId }) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(mandateId, {
      $unset: {
        startingDate: "",
      },
    });
  },

  "mandate.update.signatureDate"(id, value) {
    proOrlocalAuthorization();

    return Mandates.updateAsync(id, {
      $set: {
        signatureDate: value,
      },
    });
  },

  "mandate.update.type"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "mandate.unset.type"(id) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $unset: {
        type: "",
      },
    });
  },

  "mandate.update.seller"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        seller: value,
      },
    });
  },

  "mandate.update.buyer"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        buyer: value,
      },
    });
  },

  "mandate.update.buyerNotary"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        buyerNotary: value,
      },
    });
  },

  // Notaire du mandant / Notary of the seller
  "mandate.update.notary"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        notary: value,
      },
    });
  },

  "mandate.update.validityDuration"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        validityDuration: value,
      },
    });
  },

  async "mandate.update.asset"(id, asset) {
    proOrlocalAuthorization();
    // Push a reference on the asset
    await Assets_sale.updateAsync(asset.value, {
      $push: {
        mandates: String(id),
      },
    });
    return Mandates.updateAsync(id, {
      $set: {
        asset: asset,
      },
    });
  },

  "mandate.unset.asset"(id) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $unset: {
        asset: "",
      },
    });
  },

  "mandate.update.rentalSituation"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        rentalSituation: value,
      },
    });
  },

  "mandate.update.status"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        status: value,
      },
    });
  },

  "mandate.update.deed"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        deed: value,
      },
    });
  },

  "mandate.update.deedDate"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        deedDate: value,
      },
    });
  },

  "mandate.update.saleAgreementDate"({ id, saleAgreementDate }) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        saleAgreementDate: saleAgreementDate,
      },
    });
  },

  "mandate.update.title"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  async "mandate.add.mandatenumber"(id) {
    console.log({ id });
    proOrlocalAuthorization();
    const mandateNumber =
      await enterprise_customization.generateMandateNumber();
    if (!mandateNumber) return;
    const mandate = Mandates.findOneAsync(id);
    if (mandate?.mandate)
      throw new Meteor.Error("Error", "Mandate  already has a number");

    // Check
    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });
    const saleMandateNumerCheck = await Mandates_For_Sale.findOneAsync({
      "mandate.number": mandateNumber,
    });

    const MandateNumerCheck = await Mandates.findOneAsync({
      "mandate.number": mandateNumber,
    });
    if (rentMandateNumerCheck || saleMandateNumerCheck)
      throw new Meteor.Error("Error", "Mandate Number already used");

    // Update mandate
    Mandates.updateAsync(
      { _id: id },
      {
        $set: {
          "mandate.number": mandateNumber,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );

    // Update asset ref
    // const asset = await Assets_sale.findOneAsync(mandate.asset);

    // if (mandate?.asset?.value) {
    //   Assets_sale.updateAsync(mandate.asset.value, {
    //     $set: {
    //       mandateRef: mandateNumber,
    //     },
    //   });
    // }

    return mandateNumber;
  },

  async "mandate.remove"(id) {
    proOrlocalAuthorization();
    //A mandate with mandate number cannot be deleted
    const thismandate = await Mandates.findOneAsync(id);

    if (!thismandate) {
      throw new Meteor.Error("Error", "Mandate not found");
    }
    if (thismandate.mandate) {
      throw new Meteor.Error("Error", "Mandate has number");
    }

    return Mandates.removeAsync(id);
  },

  "mandate.update.isArchived"(id) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        isArchived: true,
      },
    });
  },

  "mandate.push.links"(id, title, url) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $push: {
        links: {
          title: title,
          url: url,
        },
      },
    });
  },

  "mandate.update.address"(id, postalCode, streetAddress, city, country = "") {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "mandate.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "mandate.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "mandate.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "mandate.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "mandate.push.offer"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $push: {
        offers: value,
      },
    });
  },

  "mandate.push.category"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "mandate.update.description"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "mandate.update.thumbnail"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        thumbnail: value,
      },
    });
  },

  "mandate.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "mandate.push.amendments"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $push: {
        amendments: value,
      },
    });
  },

  "mandate.update.amendments.saleprice"(id, index, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        [`amendments.${index}.price_feesincluded`]: value,
      },
    });
  },

  "mandate.update.amendments.fees"(id, index, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        [`amendments.${index}.fees`]: value,
      },
    });
  },

  //Boolean
  "mandate.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  /**
   * ADMIN
   */

  async "admin.mandate.update.mandatenumber"(id, value) {
    adminOrlocalAuthorization();
    const mandate = await Mandates.findOneAsync(id);
    if (mandate?.mandate)
      throw new Meteor.Error("Error", "Mandate  already has a number");

    // Check
    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });
    const saleMandateNumerCheck = await Mandates.findOneAsync({
      "mandate.number": mandateNumber,
    });

    if (saleMandateNumerCheck || rentMandateNumerCheck)
      throw new Meteor.Error("Error", "Mandate Number already used");
    return Mandates.updateAsync(
      { _id: id },
      {
        $set: {
          "mandate.number": value,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );
  },

  //unset
  "admin.mandate.unset.mandate"(id) {
    if (!Meteor.isServer) return;
    adminOrlocalAuthorization();
    return Mandates.updateAsync(id, {
      $unset: {
        mandates: "",
      },
    });
  },

  /**
   * DEPRECATED
   */

  //Reset
  // "mandates.reset"() {
  //   adminOrlocalAuthorization();
  //   return Mandates.removeAsync({}, { multi: true });
  // },

  async "mandate.add.mandatenumber_replace"(id) {
    proOrlocalAuthorization();
    const lastMandate = Mandates.find(
      { status: { $ne: "draft" } },
      { sort: { mandate: -1 }, limit: 1, fields: { mandate: 1 } }
    ).fetch();

    let lastnumber = lastMandate?.[0]?._id || 0;
    lastnumber = Number(lastnumber) + Number(1);
    const thismandate = await Mandates.findOneAsync(id);

    // store the document in a variable
    const doc = await Mandates.findOneAsync({ _id: id });
    if (!doc) return;
    if (doc.mandate) {
      throw new Meteor.Error("Error", "This mandate has already a number");
    }

    // set a new _id on the document
    doc._id = String(lastnumber);
    doc.status = "";
    doc.mandate = lastnumber;

    // insert the document, using the new _id
    return Mandates.insertAsync(doc, function (err, res) {
      if (err) {
        console.log(err);
      } else {
        // remove the document with the old _id
        Mandates.removeAsync({ _id: id });
      }
    });
  },

  // "mandates.change"(id, num) {
  //   //Was used to change the number of mandate, in the context of FR
  //   adminOrlocalAuthorization();

  //   let number = Number(num);
  //   const thismandate = await Mandates.findOneAsync(id);
  //   // store the document in a variable
  //   const doc = await Mandates.findOneAsync({ _id: id });

  //   if (!doc) return;
  //   if (doc.mandate)
  //     throw new Meteor.Error("Error", "This mandate has already a number");

  //   // set a new _id on the document
  //   doc._id = String(number);
  //   doc.status = "";
  //   doc.mandate = number;

  //   // insert the document, using the new _id
  //   return Mandates.insertAsync(doc)
  //     // remove the document with the old _id
  //       Mandates.removeAsync({ _id: id });
  //     }
  //   });
  // },
});
