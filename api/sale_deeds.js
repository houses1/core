import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Sale_Deeds = new Mongo.Collection("sale_deeds");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("sale_deeds", function publication() {
    return Sale_Deeds.find();
  });

  Meteor.publish("sale_deed", function publication(id) {
    return Sale_Deeds.find({ _id: id });
  });

  //Update with a list, an array of IDs
  Meteor.publish("sale_deeds_list", function publication() {
    return Sale_Deeds.find();
  });

  Meteor.publish("visible_sale_deeds", function publication() {
    return Sale_Deeds.find({ isVisible: true });
  });
  Meteor.publish("sales_deed", function publication(id) {
    const result = Sale_Deeds.find({ _id: id });
    return result;
  });

  Meteor.publish("sales_deed_owner", function publication() {
    const item = Sale_Deeds.find();
    const users = Meteor.users.find();

    return [item, users];
  });
}

const generateIdNumber = async () => {
  //Mandate Number
  const date = new Date();
  const year = date.getFullYear() % 100;
  const month = date.getMonth() + 1;
  const day = ("0" + date.getDate()).slice(-2);
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const charactersLength = characters.length;

  // 2 figures at the mandate number
  const length = 2;

  let mandate = String(year) + String(month) + String(day);

  for (var i = 0; i < length; i++) {
    mandate += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  if (!mandate.length == 8) {
    throw new Meteor.Error("Mandate has not the correct format");
  }

  const found = await Sale_Deeds.findOneAsync(mandate);

  console.log({ found });
  if (found) return;

  console.log({ mandate });
  return mandate;
};

Meteor.methods({
  async "sale_deed.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const id = await generateIdNumber();

    // agent contact
    const contactAgent = Meteor.user()?.contact;

    return Sale_Deeds.insertAsync({
      _id: id,
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      // mandate: mandate,
      contact: {
        agent: contactAgent,
      },
    });
  },

  "sale_deed.update.title"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "sale_deed.update.notary"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        notary: value,
      },
    });
  },

  "sale_deed.update.signatureDate"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        signatureDate: value,
      },
    });
  },

  "sale_deed.update.asset"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        asset: value,
      },
    });
  },

  "sale_deed.update.mandate"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  "sale_deeds.push.images"(id, value) {
    proOrlocalAuthorization();

    let result = [];
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return Sale_Deeds.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
    });
  },

  "sale_deed.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "sale_deed.update.saleprice"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "sale_deeds.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "sale_deeds.push.category"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "sale_deed.update.description"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "sale_deed.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  //Boolean
  "sale_deed.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Sale_Deeds.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  "sale_deeds.reset"() {
    proOrlocalAuthorization();
    return Sale_Deeds.removeAsync({}, { multi: true });
  },
});
