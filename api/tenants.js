import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Tenants = new Mongo.Collection("tenants");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("tenants", function () {
    return Tenants.find();
  });

  Meteor.publish("tenantsandowners", function () {
    const tenants = Tenants.find();
    const users = Meteor.users.find();
    return [tenants, users];
  });

  Meteor.publish("tenants_sorted", function (limit) {
    return Tenants.find({}, { sort: { _date: -1 }, limit: limit });
  });

  Meteor.publish("visible_tenants", function () {
    return Tenants.find({ isVisible: true });
  });
  Meteor.publish("tenant", function (id) {
    const result = Tenants.find({ _id: id });
    return result;
  });
  // Meteor.publish('tenants_slug', function (id) {
  //     let result = Tenants.find({ slug: id }, { limit: 1 });
  //     if (result.count() == 0) {
  //         result = Tenants.find({ _id: id }, { limit: 1 });
  //     }
  //     return result;
  // });
}

Meteor.methods({
  "tenant.insert.creation"(tenantObject) {
    const date = new Date();
    return Tenants.insertAsync({
      gender: tenantObject.gender,
      surname: tenantObject.surname,
      firstname: tenantObject.firstname,
      emails: [tenantObject.email],
      phone: [tenantObject.phone],
      address: tenantObject.address,
      birthdate: tenantObject.birthdate,
      nationality: tenantObject.nationality,
      createdAt: date,
      createdBy: Meteor.userId(),
    });
  },

  "tenant.insert"() {
    console.log("Creation");
    const date = new Date();
    return Tenants.insertAsync({
      createdAt: date,
      createdBy: Meteor.userId(),
    });
  },

  "tenant.update"({
    id,
    gender,
    surname,
    firstname,
    email,
    phone,
    address,
    birthdate,
  }) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        gender: gender,
        surname: surname,
        firstname: firstname,
        emails: [email],
        phone: [phone],
        address: address,
        birthdate: birthdate,
      },
    });
  },

  "tenant.update.title"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "tenant.update.gender"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        gender: value,
      },
    });
  },

  "tenant.push.type"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $addToSet: {
        type: value,
      },
    });
  },

  "tenant.pull.type"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $pull: {
        type: value,
      },
    });
  },

  "tenant.push.images"(id, value) {
    proOrlocalAuthorization();
    let result = [];
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return Tenants.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
    });
  },
  "tenant.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;

    return Tenants.updateAsync(id, update);
  },

  "tenant.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "tenant.update.address"(id, postalCode, streetAddress, city, country = "") {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "tenant.update.tenant"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        tenant: {
          agent: {
            name: name,
            phone: phone,
            picture: picture,
            email: email,
          },
        },
      },
    });
  },

  "tenant.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "tenant.push.category"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "tenant.update.description"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  // Add security
  "tenant.remove"(id) {
    proOrlocalAuthorization();
    return Tenants.removeAsync({ _id: id }, { multi: false });
  },

  //Boolean
  "tenant.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Tenants.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
