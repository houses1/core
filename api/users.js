// Import Collection /Methods  in main.js (Compulsory)
// import { Mongo } from 'meteor/mongo'
import dayjs from "dayjs";

import {
  adminOrlocalAuthorization,
  loggedInAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

import { Assets_rent } from "./assets_rent.js";
import { Assets_sale } from "./assets_sale.js";

// var relativeTime = require("dayjs/plugin/relativeTime");
// dayjs.extend(relativeTime);

if (Meteor.isServer) {
  // Publish anytime  (it is for the admin / pro)
  Meteor.publish(null, function () {
    const id = Meteor.userId();
    if (id) {
      return Meteor.users.find(id, {
        limit: 1,
        fields: { roles: 1, contact: 1 },
      });
    }
  });

  // This code only runs on the server
  Meteor.publish("users", function () {
    return Meteor.users.find();
  });

  Meteor.publish("users_roles", function () {
    return Meteor.users.find({ roles: { $exists: true } });
  });

  Meteor.publish("user", function (id) {
    return Meteor.users.find(id, { limit: 1 });
  });

  Meteor.publish("user_first_lastname", function (id) {
    if (!id) return;
    return Meteor.users.find(id, { limit: 1, fields: { contact: 1 } });
  });

  Meteor.publish("user_id_username", function (id) {
    if (!id) return;
    return Meteor.users.find(
      { $or: [{ username: id }, { _id: id }] },
      { limit: 1 }
    );
  });

  Meteor.publish("thisuser", async function () {
    return Meteor.users.find(Meteor.userId(), {
      limit: 1,
      fields: { createdAt: 0 },
    });
  });
}

//Methods
Meteor.methods({
  serverCreateUser({ email, password }) {
    adminOrlocalAuthorization();
    const userId = Accounts.createUser({ email, password });
    // send enrollment mail
    // Accounts.sendEnrollmentEmail(userId)
    return userId;
  },

  // "user.create"(id, email, password) {
  //   adminOrlocalAuthorization();

  //   Accounts.createUser({
  //     _id: id,
  //     email: email,
  //     password: password,
  //   });
  // },

  serverChangeUserPw({ id, password }) {
    adminOrlocalAuthorization();
    return Accounts.setPasswordAsync(id, password);
  },

  async "admin.user.update.username"(id, value) {
    adminOrlocalAuthorization();
    // check if no taken
    const findUser = await Meteor.users.findOneAsync({ username: value });
    if (findUser) throw new Meteor.Error("username-taken");

    // Change all username from sale assets
    const saleAssets = await Assets_sale.updateAsync(
      { userId: id },
      { $set: { username: value } },
      { multi: true }
    );

    // Change all username from rent assets
    const rentAssets = await Assets_rent.updateAsync(
      { userId: id },
      { $set: { username: value } },
      { multi: true }
    );

    return Meteor.users.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  async "user.change.id"(old_id, new_id) {
    adminOrlocalAuthorization();
    const account = await Meteor.users.findOneAsync(old_id);
    account.setUserId;
    account._id = String(new_id);
    delete account.username;

    await Meteor.users.updateAsync(old_id, { $set: { _id: new_id } });

    // insert the document, using the new _id
    // return Meteor.users.insertAsync(account, function (err, res) {
    //   if (err) {
    //     console.log(err);
    //   } else {
    //     // remove the document with the old _id
    //     // Mandates_For_Sale.removeAsync({ _id: id });
    //   }
    // });
  },

  //Searches
  async "user.push.search"(zipcode, city, budget_max) {
    loggedInAuthorization();
    // if not found => Push
    const thisuser = await Meteor.userAsync();
    let found = false;

    //To change if matches the budget too
    if (thisuser?.searches) {
      found = thisuser.searches.find(
        (element) =>
          element.zipcode == zipcode && element.budgetMax == budget_max
      );
    }

    if (found) {
      console.log("Found");
      return;
    }
    return Meteor.users.updateAsync(Meteor.userId(), {
      $push: {
        searches: {
          city: city,
          zipcode: zipcode,
          createdAt: new Date(),
          budgetMax: budget_max,
        },
      },
    });
  },

  async "user.pull.search"(zipcode, budget_max) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $pull: {
        searches: {
          zipcode: zipcode,
          budgetMax: budget_max,
        },
      },
    });
  },

  //Roles
  async "user.push.role"(userId, role) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(userId, {
      $addToSet: { roles: role },
    });
  },

  "user.pull.role"(id, role) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $pull: { roles: role },
    });
  },

  //favorites
  async "user.push.favorite"(value) {
    loggedInAuthorization();

    const note = "";
    const type = "";

    // if not found => Push
    const thisuser = await Meteor.userAsync();
    let found = false;
    if (thisuser?.favorites) {
      found = thisuser.favorites.find((element) => element.assetId == value);
    }

    if (found) {
      console.log("Found");
      return;
    }

    return Meteor.users.updateAsync(Meteor.userId(), {
      $push: {
        favorites: {
          assetId: value,
          assetType: type,
          createdAt: new Date(),
          note: note,
        },
      },
    });
  },

  "user.pull.favorite"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $pull: {
        favorites: {
          assetId: value,
        },
      },
    });
  },

  "user.update.surname"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.surname": value,
      },
    });
  },

  "user.update.firstname"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.firstname": value,
      },
    });
  },

  "user.update.name"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.name": value,
      },
    });
  },

  "user.update.enterprise"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        enterprise: value,
      },
    });
  },

  "user.update.description"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        description: value,
      },
    });
  },

  "user.update.birthdate"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        birthdate: value,
      },
    });
  },

  async "user.update.birthplace"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        birthplace: value,
      },
    });
  },

  async "user.update.nationality"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        nationality: value,
      },
    });
  },

  async "user.update.officialId"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        officialId: value,
      },
    });
  },

  // Admin
  async "user.update.notes"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        notes: value,
      },
    });
  },

  async "admin.user.update.videoFolderLink"({ id, value }) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        videoFolderLink: value,
      },
    });
  },

  async "admin.user.update.isVisible"({ id, value }) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
  // For google
  async "admin.user.update.isReferenced"({ id, value }) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        isReferenced: value,
      },
    });
  },

  async "admin.user.update.isHidden"({ id, value }) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        isHidden: value,
      },
    });
  },

  async "user.push.links"(id, title, url) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $push: {
        links: {
          title: title,
          url: url,
        },
      },
    });
  },

  async "user.push.sharedlinks"(id, title, url) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $push: {
        sharedlinks: {
          title: title,
          url: url,
        },
      },
    });
  },

  async "user.update.positionTitle"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        positionTitle: value,
      },
    });
  },

  async "user.admin.update.email"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        "contact.email": value,
      },
    });
  },

  async "user.admin.update.phone"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        "contact.phone": value,
      },
    });
  },

  async "user.admin.update.address"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        "contact.address": value,
      },
    });
  },

  async "user.admin.update.tahiti"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        tahiti: value,
      },
    });
  },

  async "user.admin.update.rsac"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        rsac: value,
      },
    });
  },

  async "user.admin.update.rib"(id, value) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        rib: value,
      },
    });
  },

  async "admin.user.update.firstname"({ id, value }) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        "contact.firstname": value,
      },
    });
  },
  async "admin.user.update.surname"({ id, value }) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $set: {
        "contact.surname": value,
      },
    });
  },

  async "user.update.referral"(id, value) {
    adminOrlocalAuthorization();

    const usertofind = await Meteor.users.findOneAsync({
      $or: [
        { _id: value },
        { email: value },
        { emails: { $elemMatch: { address: value } } },
      ],
    });

    // const usertofind = await Meteor.users.findOneAsync({
    //   emails: { $elemMatch: { address: value } },
    // });

    if (!usertofind) {
      throw new Meteor.Error("not-found");
    }

    return Meteor.users.updateAsync(id, {
      $push: {
        referral: {
          id: usertofind._id,
          email: usertofind.emails[0].address,
          date: new Date(),
        },
      },
    });
  },

  "user.update.rsac"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        rsac: value,
      },
    });
  },

  "user.update.socialSecurityNumber"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        socialSecurityNumber: value,
      },
    });
  },

  "user.update.siret"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        siret: value,
      },
    });
  },

  "user.update.tahiti"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        tahiti: value,
      },
    });
  },

  "user.update.rib"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        rib: value,
      },
    });
  },

  "user.update.isVisible"({ value }) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        isVisible: value,
      },
    });
  },

  "user.update.tvaNumber"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        tvaNumber: value,
      },
    });
  },

  "user.update.insuranceEndDate"(value) {
    proOrlocalAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "insurance.endDate": value,
      },
    });
  },

  "user.update.email"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.email": value,
      },
    });
  },

  "user.update.phone"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.phone": value,
      },
    });
  },

  "user.update.linkedin"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.linkedin": value,
      },
    });
  },

  "user.update.facebook"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.facebook": value,
      },
    });
  },

  "user.update.instagram"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.instagram": value,
      },
    });
  },

  "user.update.tiktok"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.tiktok": value,
      },
    });
  },

  "user.update.telegram"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.telegram": value,
      },
    });
  },

  "user.update.signal"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.signal": value,
      },
    });
  },

  "user.update.whatsapp"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.whatsapp": value,
      },
    });
  },

  "user.update.twitter"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.twitter": value,
      },
    });
  },

  "user.update.threads"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.threads": value,
      },
    });
  },

  "user.update.youtube"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.youtube": value,
      },
    });
  },

  "user.update.address"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.address": value,
      },
    });
  },

  "user.update.picture"(value) {
    loggedInAuthorization();
    return Meteor.users.updateAsync(Meteor.userId(), {
      $set: {
        "contact.picture": value,
      },
    });
  },

  "user.substract.wallet"({ id, value, notes = "" }) {
    loggedInAuthorization();
    // Value should be negative
    if (value > 0) {
      return;
    }

    return Meteor.users.updateAsync(id, {
      $inc: {
        "wallet.amount": value,
      },
      $set: {
        "wallet.date": new Date(),
        "wallet.notes": notes,
      },
    });
  },

  "user.admin.increment.wallet"({ id, value, notes = "" }) {
    adminOrlocalAuthorization();
    return Meteor.users.updateAsync(id, {
      $inc: {
        "wallet.amount": value,
      },
      $set: {
        "wallet.date": new Date(),
        "wallet.notes": notes,
      },
    });
  },

  async "user.remove"(value) {
    adminOrlocalAuthorization();
    return Meteor.users.removeAsync({ _id: value });
  },

  async "user.update.IpAndLastConnection"() {
    const now = dayjs();
    const user = await Meteor.userAsync({ fields: { lastConnection: 1 } });
    const lastConnection = user?.lastConnection?.date;

    if (
      (user && !user.lastConnection) ||
      (lastConnection && now.diff(lastConnection, "day") >= 1)
    ) {
      const ip = this.connection?.clientAddress;
      await Meteor.users.updateAsync(
        {
          _id: Meteor.userId(),
        },
        {
          $set: {
            lastConnection: {
              ip_address: ip,
              date: new Date(),
            },
          },
          $inc: { connectionNumber: 1 },
        }
      );
    }
  },
});
