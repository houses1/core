import { Mongo } from "meteor/mongo";
// import { isAdminServer } from "../admin.ts";

import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Valuations = new Mongo.Collection("valuations");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("valuations", function publication() {
    return Valuations.find();
  });

  Meteor.publish("myvaluations", function publication() {
    return Valuations.find({ userId: Meteor.userId() });
  });

  Meteor.publish("valuations_from_asset", function publication(assetId) {
    return Valuations.find({ "asset.value": assetId });
  });

  Meteor.publish("valuation", function publication(id) {
    const result = Valuations.find({ _id: id });
    return result;
  });

  // Meteor.publish('sales_deeds_slug', function publication(id) {
  //     let result = Valuations.find({ slug: id }, { limit: 1 });
  //     if (result.count() == 0) {
  //         result = Valuations.find({ _id: id }, { limit: 1 });
  //     }
  //     return result;
  // });
}

function moveArrayItem(arr, fromIndex, toIndex) {
  const element = arr[fromIndex];
  arr.splice(fromIndex, 1);
  arr.splice(toIndex, 0, element);
}

const generateIdNumber = async () => {
  //Mandate Number
  const date = new Date();
  const year = date.getFullYear() % 100;
  const month = date.getMonth() + 1;
  const day = ("0" + date.getDate()).slice(-2);
  const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const charactersLength = characters.length;

  // 2 figures at the mandate number
  const length = 2;

  let mandateID = String(year) + String(month) + String(day);
  for (var i = 0; i < length; i++) {
    mandateID += characters.charAt(
      Math.floor(Math.random() * charactersLength)
    );
  }
  if (!mandateID.length == 8) {
    throw new Meteor.Error("Mandate has not the correct format");
  }
  const found = await Valuations.findOneAsync(mandateID);

  if (found) {
    return;
  }

  return mandateID;
};

Meteor.methods({
  async "valuations.insert"({ title, assetId, type }) {
    const date = new Date();
    const id = await generateIdNumber();
    const user = await Meteor.userAsync();
    // agent contact
    const contactAgent = user?.contact;

    let object = {
      _id: id,
      type,
      title,
      createdAt: date,
      userId: Meteor.userId(),
      // mandate: mandate,
      contact: {
        agent: contactAgent,
      },
    };

    if (assetId) {
      object.asset = { value: assetId };
    }

    return Valuations.insertAsync(object);
  },

  "valuations.update.type"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "valuations.update.title"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "valuations.update.asset"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        asset: value,
      },
    });
  },

  "valuations.update.seller"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        seller: value,
      },
    });
  },
  //
  "valuations.update.risks.groundMovement"({ id, value }) {
    return Valuations.updateAsync(id, {
      $set: {
        "risks.groundMovement": value,
      },
    });
  },
  "valuations.update.risks.flood"({ id, value }) {
    return Valuations.updateAsync(id, {
      $set: {
        "risks.flood": value,
      },
    });
  },
  "valuations.update.risks.marineSubmersion"({ id, value }) {
    return Valuations.updateAsync(id, {
      $set: {
        "risks.marineSubmersion": value,
      },
    });
  },

  "valuations.update.risks.image"({ id, value }) {
    return Valuations.updateAsync(id, {
      $set: {
        "risks.image": value,
      },
    });
  },
  //

  "valuations.update.priceReference.land.average"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        "priceReference.land.average": value,
      },
    });
  },

  "valuations.update.priceReference.house.average"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        "priceReference.house.average": value,
      },
    });
  },

  "valuations.update.priceReference.apartment.average"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        "priceReference.apartment.average": value,
      },
    });
  },

  "valuations.update.visitDate"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        visitDate: value,
      },
    });
  },

  // Price estimation
  "valuations.update.price.min"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        "price.min": value,
      },
    });
  },

  "valuations.update.price.max"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        "price.max": value,
      },
    });
  },

  "valuations.update.mandate"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  "valuations.push.images"({ id, value }) {
    console.log({ id, value });
    proOrlocalAuthorization();
    let result = [];

    // value is an array
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return Valuations.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
      $set: {
        modifiedAt: new Date(),
      },
    });
  },

  "valuations.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;
    return Valuations.updateAsync(id, update);
  },

  async "valuations.position.images.left"(id, index) {
    proOrlocalAuthorization();
    const valuation = await Valuations.findOneAsync(id);
    const images = valuation?.images;

    const newIndex = Number(index) - 1;

    moveArrayItem(images, index, newIndex);
    await Valuations.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },
  async "valuations.position.images.right"(id, index) {
    proOrlocalAuthorization();

    const valuation = await Valuations.findOneAsync(id);
    const images = valuation?.images;

    const newIndex = Number(index) + 1;

    moveArrayItem(images, index, newIndex);
    await Valuations.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },

  "valuations.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Valuations.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  // Used for geography situation
  "valuations.update.image"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        image: value,
      },
    });
  },

  // For risk
  "valuations.update.riskimage"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        riskImage: value,
      },
    });
  },

  "valuations.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    return Valuations.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "valuations.update.status"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        status: value,
      },
    });
  },

  "valuations.update.contact"(id, name, phone, picture, email) {
    return Valuations.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "valuations.update.saleprice"(id, value) {
    return Valuations.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "valuations.update.livingArea"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "valuations.push.transaction"({ id, value }) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }

    return Valuations.updateAsync(id, {
      $push: {
        transactions: value,
      },
    });
  },

  "valuations.push.tag"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "valuations.push.category"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "valuations.update.description"({ id, description }) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        description: description,
      },
    });
  },

  "valuations.update.geographySituation"({ id, description }) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        geographySituation: description,
      },
    });
  },

  "valuations.update.externalUrl"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  //Boolean
  "valuations.update.isVisible"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Valuations.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
