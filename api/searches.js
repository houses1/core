import { Mongo } from "meteor/mongo";
import { isAdminServer } from "../admin.ts";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Searches = new Mongo.Collection("searches");

// NOT IN USE AT THE MOMENT, SEARCHES ARE ASSOCIATED TO THE USER AT THE MOMENT
// To be used for the admin, searches

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("searches", function searchesPublication() {
    return Searches.find();
  });

  Meteor.publish("mySearches", function searchesPublication() {
    return Searches.find({ userId: Meteor.userId(), type: { $ne: "admin" } });
  });

  //for admin
  Meteor.publish("mySearchesAdmin", function searchesPublication() {
    return Searches.find({ userId: Meteor.userId(), type: "admin" });
  });

  //for admin
  Meteor.publish("searchesAdmin", function searchesPublication() {
    return Searches.find({ type: "admin" });
  });

  //To be implemented
  Meteor.publish("visible_searches", function searchesPublication() {
    return Searches.find({ isVisible: true });
  });
}

Meteor.methods({
  "searches.insert.admin"(title) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.insertAsync({
      title: title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      type: "admin",
      landSurfaceMin: 0,
      livingAreaMin: 0,
      priceMin: 0,
    });
  },

  async "searches.insert.profile"(title) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    const user = await Meteor.userAsync();
    if (!user) return;
    const email = user?.emails?.[0]?.address;
    if (!email) return;

    return Searches.insertAsync({
      title: title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      email: email,
      landSurfaceMin: 0,
      livingAreaMin: 0,
      priceMin: 0,
    });
  },

  "searches.update.isActive"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        isActive: value,
      },
    });
  },

  // From the unsubscribe page
  "searches.update.hasUnsubscribed"({ id, value = true }) {
    return Searches.updateAsync(id, {
      $set: {
        hasUnsubscribed: value,
      },
    });
  },

  "searches.update.title"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "searches.update.email"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        email: value,
      },
    });
  },

  "searches.update.notes"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        notes: value,
      },
    });
  },

  "searches.update.transactionType"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        transactionType: value,
      },
    });
  },

  "searches.update.assetType"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        assetType: value,
      },
    });
  },

  "searches.update.priceMin"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        priceMin: value,
      },
    });
  },

  "searches.update.livingAreaMin"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        livingAreaMin: value,
      },
    });
  },

  "searches.update.landSurfaceMin"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        landSurfaceMin: value,
      },
    });
  },

  "searches.update.cities"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        cities: value,
      },
    });
  },

  "searches.unset.cities"(id) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $unset: {
        cities: "",
      },
    });
  },

  "searches.update.priceMax"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        priceMax: value,
      },
    });
  },

  "searches.push.tag"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  // After sending a notification we record
  // createdAt: new Date(),
  "searches.push.assetId"({ id, assetId }) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $push: {
        notifications: {
          assetId: assetId,
          date: new Date(),
        },
      },
    });
  },
  // Push arrays
  "searches.push.assetIds"({ ids, assetId }) {
    if (!this.userId) throw new Meteor.Error("not-authorized");

    ids.forEach(async (id) => {
      await Searches.updateAsync(id, {
        $push: {
          notifications: {
            assetId: assetId,
            date: new Date(),
          },
        },
      });
    });
  },

  "searches.push.category"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "searches.update.description"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "searches.update.slug"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        slug: value,
      },
    });
  },

  "searches.update.username"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  "searches.update.externalUrl"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  //Boolean
  "searches.update.isVisible"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Searches.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  // Add security
  "searches.remove"(id) {
    // adminOrlocalAuthorization();
    return Searches.removeAsync({ _id: id });
  },
});
