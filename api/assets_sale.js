import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

import dayjs from "dayjs";

const { slugify, addWatermark_url } = require("../commonFunctions.ts");

export const Assets_sale = new Mongo.Collection("for_sale_assets");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("for_sale_assets", function publication() {
    return Assets_sale.find();
  });

  Meteor.publish("for_sale_assets_advisor", function publication(id) {
    return Assets_sale.find(
      {
        $or: [{ userId: id }, { username: id }],
        archived: { $ne: true },
        isVisible: true,
      },
      { sort: { createdAt: -1 } }
    );
  });

  Meteor.publish("my_for_sale_assets", function publication() {
    return Assets_sale.find({ userId: Meteor.userId() });
  });

  Meteor.publish("for_sale_assets_owner", function publication() {
    const assets = Assets_sale.find();
    const users = Meteor.users.find();
    return [assets, users];
  });

  // admin_ForSale_Assets_cards
  Meteor.publish("for_sale_assets_active_owner", function publication() {
    const assets = Assets_sale.find({
      $or: [{ mandates: { $exists: true } }, { isVisible: true }],
    });
    const users = Meteor.users.find();
    return [assets, users];
  });

  Meteor.publish("my_for_sale_assets_owner", function publication() {
    const assets = Assets_sale.find({ userId: Meteor.userId() });
    const users = Meteor.users.find();
    return [assets, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("for_sale_assets_list", function publication() {
    return Assets_sale.find();
  });

  // For the similar assets in sale page
  Meteor.publish(
    "similar_sale_assets",
    function publication(type, city, excludeId) {
      return Assets_sale.find(
        {
          type: type,
          isVisible: true,
          archived: { $ne: true },
          isCancelledBySeller: { $ne: true },
          isSold: { $ne: true },
          isSoldByOthers: { $ne: true },
          _id: { $ne: excludeId },
          "address.city": city,
        },

        {
          limit: 3,
          fields: {
            constructionYear: 0,
            copro: 0,
            mandates: 0,
            pageViews: 0,
            propertyTax: 0,
          },
        }
      );
    }
  );

  // For the the search - sale
  Meteor.publish("visible_for_sale_assets", function publication() {
    return Assets_sale.find(
      {
        isVisible: true,
        archived: { $ne: true },
        isSold: { $ne: true },
        isSoldByOthers: { $ne: true },
        isCancelledBySeller: { $ne: true },
      },
      {
        fields: {
          constructionYear: 0,
          copro: 0,
          mandates: 0,
          pageViews: 0,
          propertyTax: 0,
        },
      }
    );
  });

  // For the recent sales if having mandate is not compulsory
  Meteor.publish(
    "visible_for_sale_assets_limitDate",
    function publication({ months }) {
      let limitDate = dayjs().subtract(months, "month").format("YYYY-MM-DD");
      limitDate = new Date(limitDate);

      return Assets_sale.find(
        {
          isVisible: true,
          archived: { $ne: true },
          isSold: { $ne: true },
          isCancelled: { $ne: true },
          isSoldByOthers: { $ne: true },
          isCancelledBySeller: { $ne: true },
          //modifiedAt: { $gte: limitDate },       ///Putting it make some weird consequence that the items are not displays
        },

        {
          // sort: { modifiedAt: -1 },
          fields: {
            constructionYear: 0,
            copro: 0,
            mandates: 0,
            pageViews: 0,
            propertyTax: 0,
          },
        }
      );
    }
  );

  // For the recent sales if having mandate is  compulsory
  Meteor.publish(
    "visible_for_sale_assets_limitDate_mandateCompulsory",
    function publication() {
      const months = 24;
      let limitDate = dayjs().subtract(months, "month").format("YYYY-MM-DD");
      limitDate = new Date(limitDate);

      return Assets_sale.find(
        {
          isVisible: true,
          archived: { $ne: true },
          mandateRef: { $exists: true },
          isSold: { $ne: true },
          isSoldByOthers: { $ne: true },
          isCancelledBySeller: { $ne: true },
          modifiedAt: { $gte: limitDate },
        },
        {
          // sort: { modifiedAt: -1 },
          fields: {
            constructionYear: 0,
            copro: 0,
            mandates: 0,
            pageViews: 0,
            propertyTax: 0,
            postedFBPages: 0,
            cadastre: 0,
            floorNumber: 0,
            totalFloorNumber: 0,
            youtubeId: 0,
          },
        }
      );
    }
  );

  Meteor.publish("for_sale_asset", function publication(id) {
    const result = Assets_sale.find({ _id: id });
    return result;
  });

  Meteor.publish("for_sale_asset_title", function publication(id) {
    const result = Assets_sale.find({ _id: id }, { fields: { title: 1 } });
    return result;
  });

  Meteor.publish("for_sale_asset_protected", async function publication(id) {
    const user = await Meteor.userAsync({ fields: { roles: 1 } });
    if (user?.roles.includes("admin")) {
      return Assets_sale.find({ _id: id });
    } else {
      return Assets_sale.find({ _id: id, userId: Meteor.userId() });
    }
  });

  Meteor.publish("for_sale_asset_slug", function publication(id) {
    return Assets_sale.find({ $or: [{ slug: id }, { _id: id }] }, { limit: 1 });
  });
}

Meteor.methods({
  async "for_sale_assets.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();

    // agent contact
    const contactAgent = user?.contact;
    //Email
    if (contactAgent && !contactAgent.email) {
      contactAgent.email = user?.emails?.[0]?.address;
    }

    return Assets_sale.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      username: user.username,
      contact: {
        agent: contactAgent,
      },
    });
  },

  async "asset_sale.upsert.externalPartners"({ object, name }) {
    adminOrlocalAuthorization();

    // let resultObject = {
    //   title: title,
    //   createdAt: date,
    //   userId: Meteor.userId(),
    //   username: user.username,
    //   contact: {
    //     agent: contactAgent,
    //   },
    // };
    //Probably need to change ID.......

    // Get values if already there:
    const asset = await Assets_sale.findOneAsync({
      origin: { name, ref: object._id },
    });

    if (asset) {
      //if object exist
      // We dont update those values
      object.createdAt = new Date(asset.createdAt);
      object.modifiedAt = new Date(asset.modifiedAt);
      object.isVisible = asset.isVisible;
    } else {
      // All the objects sent are visible
      // console.log({ object });
      object.isVisible = true;
      // object.createdAt = new Date();

      if (!object.modifiedAt) {
        object.modifiedAt = new Date(object.createdAt);
      }
    }

    // I dont know why, when you import the date format is changing .... meaning the sorting is difficult after
    // We reformat, but I dont think it is the optimal solution :(   )

    object.origin = { name, ref: object._id };

    return Assets_sale.updateAsync(
      { origin: { name, ref: object._id } },
      object,
      { upsert: true }
    );
  },

  // Convert Immobilier neuf -> Ancien
  async "for_sale_assets.insert.fromNewProgram"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();

    if (user) {
      const contactAgent = user?.contact;
      //Email
      if (contactAgent && !contactAgent.email) {
        contactAgent.email = user?.emails?.[0]?.address;
      }

      return Assets_sale.insertAsync({
        title: title,
        createdAt: date,
        userId: Meteor.userId(),
        username: user.username,
        contact: {
          agent: contactAgent,
        },
      });
    }
  },

  // Update
  "asset_sale.update.username"(userId, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(
      { userId: userId },
      {
        $set: {
          username: value,
        },
      },
      { multi: true }
    );
  },

  "asset_sale.update.title"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        title: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_sale.increment.pageviews"(id) {
    // I would use the sessionId instead of IP address I guess, because this.connection.clientAddress always change
    if (Meteor.isServer) {
      const asset = await Assets_sale.findOneAsync({
        $or: [{ _id: id }, { slug: id }],
      });
      if (!asset) return;

      const ip = this.connection?.clientAddress;

      let iparray = [];
      if (asset.pageViews?.ipArray) {
        iparray = [...asset.pageViews.ipArray];
      }

      if (!ip || iparray.includes(ip)) return;

      //Keeping array of length limit
      if (iparray.length >= 10) {
        iparray.shift();
      }
      iparray.push(ip);

      const currentYear = dayjs().year();
      const currentMonth = dayjs().month() + 1; // Month is zero-indexed, so we add 1

      return Assets_sale.updateAsync(asset._id, {
        $inc: {
          "pageViews.count": 1,
          [`pageViews.monthlycount.${currentYear}.${currentMonth}`]: 1, //increase total count
        },
        $set: {
          "pageViews.ipArray": iparray,
        },
      });
    }
  },

  "asset_sale.update.type"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        type: value,
        modifiedAt: new Date(),
      },
    });
  },

  // "asset_sale.update.mandate"(id, value) {
  //   proOrlocalAuthorization();
  //   return Assets_sale.updateAsync(id, {
  //     $set: {
  //       mandate: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  //add an array
  "asset_sale.push.images"(asset_id, array) {
    proOrlocalAuthorization();
    let result = [];
    array = array.forEach((e) => {
      result.push({ title: "", url: addWatermark_url(e), urlNoWatermark: e });
    });

    return Assets_sale.updateAsync(asset_id, {
      $push: {
        images: {
          $each: result,
        },
      },
      $set: {
        modifiedAt: new Date(),
      },
    });
  },

  //add an array
  "asset_sale.push.comment"({ id, text }) {
    proOrlocalAuthorization();
    const date = new Date();
    return Assets_sale.updateAsync(id, {
      $push: {
        comments: {
          text,
          date,
        },
      },
    });
  },

  "asset_sale.pull.comment"({ id, text }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $pull: {
        comments: { text },
      },
    });
  },

  "asset_sale.update.images.title"(asset_id, i, title) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });
    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = title;

    return Assets_sale.updateAsync(asset_id, update);
  },

  "asset_sale.pull.images"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "asset_sale.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        address: {
          country: country,
          city: city,
          postalCode: postalCode,
          streetAddress: streetAddress,
        },
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.address.street"(id, street) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "address.streetAddress": street,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.address.city"(id, city) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "address.city": city,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.address.postalCode"(id, postalCode) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "address.postalCode": postalCode,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.contact"({ id, name, phone, picture, email }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name: name,
            phone: phone,
            picture: picture,
            email: email,
          },
        },
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.saleprice"(id, value) {
    proOrlocalAuthorization();

    return Assets_sale.updateAsync(id, {
      $set: {
        "saleprice.fees_included": Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.saleprice.fees"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "saleprice.fees": Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.feesPaidBy"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "saleprice.feesPaidBy": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.propertyTax"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        propertyTax: value,
        modifiedAt: new Date(),
      },
    });
  },

  //Copro
  "asset_sale.update.copro.ongoingProcedure"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "copro.ongoingProcedure": value,
        modifiedAt: new Date(),
      },
    });
  },
  "asset_sale.update.copro.numberOfLots"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "copro.numberOfLots": Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.copro.annualFees"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "copro.annualFees": Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        livingArea: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.constructionYear"(id, value) {
    proOrlocalAuthorization();
    if (value > 3000) throw new Meteor.Error("wrong date");
    return Assets_sale.updateAsync(id, {
      $set: {
        constructionYear: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.landSurface"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        landSurface: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.cadastre.value"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "cadastre.reference": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.diagnostics.dpe"({ id, dpe, ges }) {
    proOrlocalAuthorization();

    let letter;

    if (ges > 80 || dpe >= 450) {
      letter = "G";
    } else if (ges > 55 || dpe > 331) {
      letter = "F";
    } else if (ges > 35 || dpe >= 231) {
      letter = "E";
    } else if (ges > 20 || dpe >= 151) {
      letter = "D";
    } else if (ges > 10 || dpe >= 91) {
      letter = "C";
    } else if (ges > 6 || dpe > 51) {
      letter = "B";
    } else if (ges < 6 || dpe < 51) {
      letter = "A";
    }

    return Assets_sale.updateAsync(id, {
      $set: {
        "diagnostics.dpe": Number(dpe),
        "diagnostics.dpeLetter": letter,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.diagnostics.date"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "diagnostics.date": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.diagnostics.cost.min"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "diagnostics.cost.min": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.diagnostics.cost.max"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "diagnostics.cost.max": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.diagnostics.ges"(id, ges) {
    proOrlocalAuthorization();

    let letter;

    if (ges < 6) {
      letter = "A";
    } else if (ges >= 6 && ges <= 10) {
      letter = "B";
    } else if (ges >= 11 && ges <= 20) {
      letter = "C";
    } else if (ges >= 21 && ges <= 35) {
      letter = "D";
    } else if (ges >= 36 && ges <= 55) {
      letter = "E";
    } else if (ges >= 56 && ges <= 80) {
      letter = "F";
    } else if (ges > 80) {
      letter = "G";
    }

    return Assets_sale.updateAsync(id, {
      $set: {
        "diagnostics.ges": Number(ges),
        "diagnostics.gesLetter": letter,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_sale.push.tag"(id, value) {
    proOrlocalAuthorization();
    // Add logs
    if (
      value == "Sous compromis" ||
      value == "Baisse de prix" ||
      value == "Sous offre"
    ) {
      await Meteor.callAsync("log.insert.sale.asset.tag", {
        assetId: id,
        tag: value,
      });
    }

    return Assets_sale.updateAsync(id, {
      $addToSet: {
        tags: value,
      },
    });
  },
  "asset_sale.pull.tag"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $pull: {
        tags: value,
      },
    });
  },

  "asset_sale.push.category"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "asset_sale.unset.bedroom"(id) {
    return Assets_sale.updateAsync(id, {
      $unset: {
        "roomsNumber.bedrooms": "",
      },
    });
  },

  "asset_sale.unset.bathroom"(id) {
    return Assets_sale.updateAsync(id, {
      $unset: {
        "roomsNumber.bathrooms": "",
      },
    });
  },

  "asset_sale.unset.kitchen"(id) {
    return Assets_sale.updateAsync(id, {
      $unset: {
        "roomsNumber.kitchens": "",
      },
    });
  },

  "asset_sale.update.floorNumber"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_sale.updateAsync(id, {
      $set: {
        floorNumber: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.totalFloorNumber"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_sale.updateAsync(id, {
      $set: {
        totalFloorNumber: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.roomsNumber.bedrooms"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_sale.updateAsync(id, {
      $set: {
        "roomsNumber.bedrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.roomsNumber.kitchens"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "roomsNumber.kitchens": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.roomsNumber.bathrooms"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "roomsNumber.bathrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.roomsNumber.parkingslots"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "roomsNumber.parkingslots": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.roomsNumber.cellars"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        "roomsNumber.cellars": value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.description"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "asset_sale.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // sharedWith
  "asset_sale.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_sale.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "asset_sale.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_sale.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  // "asset_sale.update.thumbnail"(id, value) {
  //   proOrlocalAuthorization();
  //   return Assets_sale.updateAsync(id, {
  //     $set: {
  //       thumbnail: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  async "asset_sale.position.images"(id, value) {
    proOrlocalAuthorization();

    const asset = await Assets_sale.findOneAsync(id);
    const images = asset?.images;

    const index = images.findIndex((x) => x.url == value);

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, 0);

    return Assets_sale.updateAsync(id, {
      $set: {
        images: images,
      },
    });
  },

  async "asset_sale.position.images.left"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(id);
    const images = asset?.images;
    const newIndex = Number(index) - 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    return Assets_sale.updateAsync(id, {
      $set: {
        images: images,
      },
    });
  },
  async "asset_sale.position.images.right"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(id);
    const images = asset.images;
    const newIndex = Number(index) + 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    return Assets_sale.updateAsync(id, {
      $set: {
        images: images,
      },
    });
  },

  "asset_sale.update.htmlcontent"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        htmlcontent: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_sale.update.slug"(id, value) {
    proOrlocalAuthorization();
    value = slugify(value); //Be sure it is slugify
    const checkAsset = await Assets_sale.findOneAsync({ slug: value });

    if (checkAsset?._id == id) return;
    if (checkAsset) throw new Meteor.Error("slug-taken");

    const currentAsset = await Assets_sale.findOneAsync(id);

    //*** */
    if (currentAsset?.description) {
      let { description } = currentAsset;

      const regex = new RegExp(/http\S*/);
      description = description.replace(
        regex,
        `${Meteor.absoluteUrl()}for-sale/${value}`
      );

      Assets_sale.updateAsync(id, {
        $set: {
          description: description,
        },
      });
    }
    //*** */

    return Assets_sale.updateAsync(id, {
      $set: {
        slug: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        externalUrl: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.youtubeId"(id, value) {
    proOrlocalAuthorization();

    let youtubeId;

    if (value.includes("https://youtu.be/")) {
      youtubeId = value.replace("https://youtu.be/", "");
    } else if (value.includes("youtube.com/shorts/")) {
      const paramString = value.split("/shorts/")[1];
      youtubeId = paramString.split("?")[0];
    } else if (value.includes("youtube.com/")) {
      const paramString = value.split("?")[1];
      const queryString = new URLSearchParams(paramString);
      youtubeId = queryString.get("v");
    } else {
      youtubeId = value;
    }

    if (!youtubeId) {
      return;
    }

    return Assets_sale.updateAsync(id, {
      $set: {
        youtubeId: youtubeId,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_sale.update.archive"({ id, value = true }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        archived: value,
      },
    });
  },

  // value Boolean
  "asset_sale.update.isSold"(id, value = true) {
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    if (value == true) {
      Meteor.callAsync("log.insert.sale.asset.sold", { assetId: id });
    }
    return Assets_sale.updateAsync(id, {
      $set: { isSold: value },
    });
  },

  "asset_sale.update.isSoldByOthers"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;
    // Add logs
    // if (value == true) {
    //   await Meteor.callAsync("log.insert.sale.asset.archive", { assetId: id });
    // }

    return Assets_sale.updateAsync(id, {
      $set: { isSoldByOthers: value },
    });
  },

  "asset_sale.update.isCancelledBySeller"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    // if (value == true) {
    //   await Meteor.callAsync("log.insert.sale.asset.archive", { assetId: id });
    // }

    return Assets_sale.updateAsync(id, {
      $set: { isCancelledBySeller: value },
    });
  },

  "asset_sale.update.isExpired"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    // if (value == true) {
    //  await  Meteor.callAsync("log.insert.sale.asset.archive", { assetId: id });
    // }

    return Assets_sale.updateAsync(id, {
      $set: { isExpired: value },
    });
  },

  async "asset_sale.update.isVisible"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    // Add logs
    if (value == true) {
      await Meteor.callAsync("log.insert.sale.asset.publication", {
        assetId: id,
      });
    }

    return Assets_sale.updateAsync(id, {
      $set: { isVisible: value },
    });
  },

  async "asset_sale.update.mailingSent"(id, value = true) {
    proOrlocalAuthorization();
    if (value == true) {
      await Meteor.callAsync("log.insert", { title: "Sale asset mail sent" }); // Add logs
    }
    return Assets_sale.updateAsync(id, {
      $set: { mailingSent: value },
    });
  },

  // Facebook and IG page
  async "asset_sale.update.postedFBPages"({ id, value, boolean = true }) {
    proOrlocalAuthorization();

    if (boolean == true) {
      await Meteor.callAsync("log.insert", {
        title: `${value} Sale asset FB page posted`, // Add logs
      });
    }

    const date = new Date();

    return Assets_sale.updateAsync(id, {
      $push: { postedFBPages: { id: value, date } },
    });
  },

  // Publication of "sold" or "rented" on IG, FB
  async "asset_sale.update.closedFBPages"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $push: { closedFBPages: value },
    });
  },

  async "admin.for_sale_assets.unset.postedFBPages"(id) {
    adminOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $unset: {
        postedFBPages: "",
      },
    });
  },

  //Manage publications
  async "asset_sale.push.publications"(id, website, value) {
    proOrlocalAuthorization();
    const date = new Date();
    let update;

    if (value == true) {
      update = {
        //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
        // Otherwise, we need to create a new DB for "publications"
        $addToSet: {
          publishedOn: website.id,
        },
        $push: {
          publications: {
            published: value,
            websiteId: website.id,
            publicationDate: date,
            cost: website.cost,
            name: website.name,
          },
        },
      };
    } else {
      // We remove all
      update = {
        $pull: {
          publishedOn: website.id,
          publications: { websiteId: website.id },
        },
      };
    }

    return Assets_sale.updateAsync(id, update);
  },

  // "asset_sale.pull.publications"(id, website) {
  //   proOrlocalAuthorization();
  //   const date = new Date();
  //   let update;

  //   if (value == true) {
  //     update = {
  //       //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
  //       // Otherwise, we need to create a new DB for "publications"
  //       $addToSet: {
  //         publishedOn: website.id,
  //       },
  //       $push: {
  //         publications: {
  //           published: value,
  //           websiteId: website.id,
  //           publicationDate: date,
  //           cost: website.cost,
  //           name: website.name,
  //         },
  //       },
  //     };
  //   } else {
  //     update = {
  //       //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
  //       // Otherwise, we need to create a new DB for "publications"
  //       $pull: {
  //         publishedOn: websiteId,
  //       },
  //       $push: {
  //         publications: {
  //           published: value,
  //           websiteId: websiteId,
  //           publicationDate: date,
  //           name: name,
  //         },
  //       },
  //     };
  //   }
  //   return Assets_sale.updateAsync(id, update);
  // },

  async "for_sale_assets.remove"(id) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(id);

    if (asset.mandates) {
      throw new Meteor.Error("mandate attached");
    }

    if (asset?.userId && asset?.userId == Meteor.userId()) {
      return Assets_sale.removeAsync(id);
    }
  },

  /**
   * ADMIN
   */

  "admin.for_sale_assets.unset.publications"(id) {
    adminOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $unset: {
        publications: "",
      },
    });
  },

  "admin.for_sale_assets.unset.mandate"(id) {
    if (!Meteor.isServer) return;
    adminOrlocalAuthorization();

    return Assets_sale.updateAsync(
      { _id: id },
      {
        $unset: {
          mandates: "",
        },
      }
    );
  },

  async "admin.for_sale_assets.remove"(id) {
    adminOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(id);
    // if (asset?.userId && asset?.userId == Meteor.userId()) {
    return Assets_sale.removeAsync(id);
    // }
  },

  //// Danger
  // "admin.for_sale_assets.reset"(id) {
  //   adminOrlocalAuthorization();
  //   return Assets_sale.removeAsync({_id:id}, { multi: true });
  // },
  // "admin.for_sale_assets.all.reset"() {
  //   adminOrlocalAuthorization();
  //   return Assets_sale.removeAsync({}, { multi: true });
  // },

  "admin.for_sale_assets.addToSet.mandate"(id, value) {
    adminOrlocalAuthorization();
    Assets_sale.updateAsync(id, {
      $addToSet: {
        mandates: String(value),
      },
    });
  },

  "admin.for_sale_assets.pull.mandate"(id, value) {
    adminOrlocalAuthorization();
    Assets_sale.updateAsync(id, {
      $pull: {
        mandates: String(value),
      },
    });
  },

  "admin.for_sale_assets.update.mandateRef"(id, value) {
    adminOrlocalAuthorization();
    return Assets_sale.updateAsync(id, {
      $set: {
        mandateRef: value,
      },
    });
  },

  async "admin.all.for_sale_assets.format.modifiedAt"() {
    adminOrlocalAuthorization();
    const assets = await Assets_sale.find().fetch();
    if (!assets) return;

    assets.forEach(async (x) => {
      await Meteor.callAsync("admin.for_sale_assets.format.modifiedAt", x._id);
    });
  },

  async "admin.for_sale_assets.format.modifiedAt"(id) {
    adminOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(id);

    if (!asset) return;

    let date = asset.modifiedAt || asset.createdAt;

    return Assets_sale.updateAsync(id, {
      $set: {
        modifiedAt: new Date(date),
      },
    });
  },

  ///Temporary

  // "asset_sale.unset.saleprice"(id) {
  //   if (!this.userId) {
  //     throw new Meteor.Error("not-authorized");
  //   }
  // adminOrlocalAuthorization();
  //   return Assets_sale.updateAsync(id, {
  //     $unset: {
  //       saleprice: "",
  //     },
  //   });
  // },
});
