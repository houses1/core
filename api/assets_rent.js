import { Mongo } from "meteor/mongo";

import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

import dayjs from "dayjs";

const { addWatermark_url } = require("../commonFunctions.ts");

export const Assets_rent = new Mongo.Collection("for_rent_assets");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("for_rent_assets", function () {
    return Assets_rent.find();
  });

  Meteor.publish("for_rent_assets_advisor", function (id) {
    return Assets_rent.find(
      {
        $or: [{ userId: id }, { username: id }],
        archived: { $ne: true },
        isVisible: true,
      },
      { sort: { createdAt: -1 } }
    );
  });

  Meteor.publish("my_for_rent_assets", function () {
    return Assets_rent.find({ userId: Meteor.userId() });
  });

  Meteor.publish("for_rent_assets_owner", function () {
    const items = Assets_rent.find();
    const users = Meteor.users.find();
    return [items, users];
  });

  // admin_ForRent_Assets_cards
  Meteor.publish("for_rent_assets_active_owner", function () {
    const items = Assets_rent.find({
      $or: [{ mandates: { $exists: true } }, { isVisible: true }],
    });
    const users = Meteor.users.find();
    return [items, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("for_rent_assets_list", function () {
    return Assets_rent.find();
  });

  // For the similar assets in sale page
  Meteor.publish("similar_rent_assets", function (type, city, excludeId) {
    return Assets_rent.find(
      {
        type: type,
        isVisible: true,
        archived: { $ne: true },
        isCancelled: { $ne: true },
        isRented: { $ne: true },
        isRentedByOthers: { $ne: true },
        _id: { $ne: excludeId },
        "address.city": city,
      },
      {
        limit: 3,
        fields: {
          copro: 0,
          mandates: 0,
          pageViews: 0,
          propertyTax: 0,
        },
      }
    );
  });

  Meteor.publish("visible_for_rent_assets", function () {
    return Assets_rent.find({
      isVisible: true,
      archived: { $ne: true },
      isCancelled: { $ne: true },
      isRented: { $ne: true },
      isRentedByOthers: { $ne: true },
    });
  });

  Meteor.publish("visible_for_rent_assets_limitDate", function ({ months }) {
    let limitDate = dayjs().subtract(months, "month").format("YYYY-MM-DD");
    limitDate = new Date(limitDate);

    return Assets_rent.find({
      isVisible: true,
      archived: { $ne: true },
      isCancelled: { $ne: true },
      isRented: { $ne: true },
      isRentedByOthers: { $ne: true },
      // modifiedAt: { $gte: limitDate },   ///Putting it make some weird consequence that the items are not displays
    });
  });

  Meteor.publish(
    "visible_for_rent_assets_mandateCompulsory",
    function publication() {
      return Assets_rent.find({
        isVisible: true,
        mandateRef: { $exists: true },
        archived: { $ne: true },
        isCancelled: { $ne: true },
        isRented: { $ne: true },
        isRentedByOthers: { $ne: true },
      });
    }
  );

  Meteor.publish("for_rent_asset", function (id) {
    const result = Assets_rent.find({ _id: id });
    return result;
  });

  Meteor.publish("for_rent_asset_title", function (id) {
    const result = Assets_rent.find({ _id: id }, { fields: { title: 1 } });
    return result;
  });

  Meteor.publish("for_rent_asset_protected", async (id) => {
    let result;
    const user = await Meteor.userAsync({ fields: { roles: 1 } });

    if (user?.roles?.includes("admin")) {
      return await Assets_rent.find({ _id: id });
    } else {
      return await Assets_rent.find({
        _id: id,
        userId: Meteor.userId(),
      });
    }
  });

  Meteor.publish("for_rent_asset_slug", function (id) {
    return Assets_rent.find({ $or: [{ slug: id }, { _id: id }] }, { limit: 1 });
  });
}

Meteor.methods({
  async "asset_rent.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();
    const contactAgent = user.contact;

    return await Assets_rent.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      username: user.username,
      contact: {
        agent: contactAgent,
      },
    });
  },

  async "asset_rent.upsert.externalPartners"({ object, name }) {
    adminOrlocalAuthorization();

    // let resultObject = {
    //   title: title,
    //   createdAt: date,
    //   userId: Meteor.userId(),
    //   username: user.username,
    //   contact: {
    //     agent: contactAgent,
    //   },
    // };
    //Probably need to change ID.......

    const asset = await Assets_rent.findOneAsync({
      origin: { name, ref: object._id },
    });

    if (asset) {
      // We dont update those values
      object.createdAt = asset.createdAt;
      object.isVisible = asset.isVisible;
    } else {
      // All the objects sent are visible
      object.isVisible = true;
      object.createdAt = new Date();
      object.modifiedAt = new Date();
    }

    object.origin = { name, ref: object._id };

    return Assets_rent.updateAsync(
      { origin: { name, ref: object._id } },
      object,
      { upsert: true }
    );
  },

  "asset_rent.update.title"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        title: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.increment.pageviews"(id) {
    // I would use the sessionId instead of IP address I guess, because this.connection.clientAddress always change
    if (Meteor.isServer) {
      const asset = await Assets_rent.findOneAsync({
        $or: [{ _id: id }, { slug: id }],
      });
      if (!asset) return;

      const ip = this.connection?.clientAddress;

      let iparray = [];
      if (asset.pageViews?.ipArray) {
        iparray = [...asset.pageViews.ipArray];
      }

      if (!ip || iparray.includes(ip)) {
        return;
      } else {
        //Keeping array of length limit
        if (iparray.length >= 10) {
          iparray.shift();
        }
        iparray.push(ip);
      }

      const currentYear = dayjs().year();
      const currentMonth = dayjs().month() + 1; // Month is zero-indexed, so we add 1

      return Assets_rent.updateAsync(asset._id, {
        $inc: {
          "pageViews.count": 1, //increase total count
          [`pageViews.monthlycount.${currentYear}.${currentMonth}`]: 1, //increase total count
        },
        $set: {
          "pageViews.ipArray": iparray,
        },
      });
    }
  },

  "asset_rent.update.type"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        type: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.transactiontype"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        transactiontype: value,
        modifiedAt: new Date(),
      },
    });
  },

  // "asset_rent.update.mandate"(id, value) {
  //   proOrlocalAuthorization();
  //   return Assets_rent.updateAsync(id, {
  //     $set: {
  //       mandate: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  //add an array
  "asset_rent.push.comment"({ id, text }) {
    proOrlocalAuthorization();
    const date = new Date();
    return Assets_rent.updateAsync(id, {
      $push: {
        comments: {
          text,
          date,
        },
      },
    });
  },

  "asset_rent.pull.comment"({ id, text }) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $pull: {
        comments: { text },
      },
    });
  },

  "asset_rent.push.images"({ id, value }) {
    proOrlocalAuthorization();
    let result = [];

    if (!value) return;

    value = value.forEach((e) => {
      result.push({ title: "", url: addWatermark_url(e), urlNoWatermark: e });
    });

    return Assets_rent.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
      $set: {
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;
    return Assets_rent.updateAsync(id, update);
  },

  "asset_rent.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "asset_rent.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.address.street"(id, street) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "address.streetAddress": street,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.address.city"(id, city) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "address.city": city,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.address.postalCode"(id, postalCode) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "address.postalCode": postalCode,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
        modifiedAt: new Date(),
      },
    });
  },

  "for_rent_assets.unset.bedroom"(id) {
    return Assets_rent.updateAsync(id, {
      $unset: {
        "roomsNumber.bedrooms": "",
      },
    });
  },

  "for_rent_assets.unset.bathroom"(id) {
    return Assets_rent.updateAsync(id, {
      $unset: {
        "roomsNumber.bathrooms": "",
      },
    });
  },

  "for_rent_assets.unset.kitchen"(id) {
    return Assets_rent.updateAsync(id, {
      $unset: {
        "roomsNumber.kitchens": "",
      },
    });
  },

  "asset_rent.update.rentpermonth"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        rentpermonth: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        livingArea: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "asset_rent.pull.tag"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $pull: {
        tags: value,
      },
    });
  },

  "asset_rent.push.category"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "asset_rent.update.description"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        description: value,
        modifiedAt: new Date(),
      },
    });
  },

  "asset_rent.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // sharedWith
  async "asset_rent.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_rent.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  async "asset_rent.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_rent.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  async "asset_rent.update.floorNumber"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_rent.updateAsync(id, {
      $set: {
        floorNumber: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.totalFloorNumber"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_rent.updateAsync(id, {
      $set: {
        totalFloorNumber: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.roomsNumber.bedrooms"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_rent.updateAsync(id, {
      $set: {
        "roomsNumber.bedrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.roomsNumber.kitchens"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "roomsNumber.kitchens": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.roomsNumber.bathrooms"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "roomsNumber.bathrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.roomsNumber.parkingslots"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "roomsNumber.parkingslots": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.roomsNumber.cellars"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        "roomsNumber.cellars": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.thumbnail"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        thumbnail: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "for_rent_assets.position.images"(id, value) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(id);
    const images = asset?.images;
    const index = images.findIndex((x) => x.url == value);

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, 0);
    Assets_rent.updateAsync(id, {
      $set: {
        images: images,
      },
    });

    return;
  },

  async "for_rent_assets.position.images.left"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(id);
    const images = asset?.images;
    const newIndex = Number(index) - 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    Assets_rent.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },
  async "for_rent_assets.position.images.right"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(id);
    const images = asset?.images;
    const newIndex = Number(index) + 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    Assets_rent.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },

  async "asset_rent.update.htmlcontent"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        htmlcontent: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.slug"({ id, value }) {
    proOrlocalAuthorization();

    const checkAsset = await Assets_rent.findOneAsync({ slug: value });
    if (checkAsset) throw new Meteor.Error("slug-taken");

    return Assets_rent.updateAsync(id, {
      $set: {
        slug: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.heating"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        heating: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.hotwater"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        hotwater: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        externalUrl: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.youtubeId"(id, value) {
    proOrlocalAuthorization();

    let youtubeId;
    if (value.includes("https://youtu.be/")) {
      youtubeId = value.replace("https://youtu.be/", "");
    } else if (value.includes("youtube.com/shorts/")) {
      const paramString = value.split("/shorts/")[1];
      youtubeId = paramString.split("?")[0];
    } else if (value.includes("youtube.com/")) {
      const paramString = value.split("?")[1];
      const queryString = new URLSearchParams(paramString);
      youtubeId = queryString.get("v");
    } else {
      youtubeId = value;
    }

    if (!youtubeId) {
      return;
    }

    return Assets_rent.updateAsync(id, {
      $set: {
        youtubeId: youtubeId,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.constructionYear"(id, value) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        constructionYear: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "asset_rent.update.isRented"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    if (value == true) {
      await Meteor.callAsync("log.insert.rent.asset.rented", { assetId: id });
    }

    return Assets_rent.updateAsync(id, {
      $set: {
        isRented: value,
      },
    });
  },

  async "asset_rent.update.isRentedByOthers"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    if (value == true) {
      await Meteor.callAsync("log.insert.rent.asset.rented", { assetId: id });
    }

    return Assets_rent.updateAsync(id, {
      $set: {
        isRentedByOthers: value,
      },
    });
  },

  async "asset_rent.update.isCancelled"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    // if (value == true) {
    //   Meteor.callAsync("log.insert.rent.asset.rented", { assetId: id });
    // }

    return Assets_rent.updateAsync(id, {
      $set: {
        isCancelled: value,
      },
    });
  },

  async "asset_rent.update.isExpired"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    // Add logs
    // if (value == true) {
    //   Meteor.callAsync("log.insert.rent.asset.rented", { assetId: id });
    // }

    return Assets_rent.updateAsync(id, {
      $set: {
        isExpired: value,
      },
    });
  },

  async "asset_rent.update.isVisible"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    // Save for logs
    if (value == true) {
      await Meteor.callAsync("log.insert.rent.asset.publication", {
        assetId: id,
      });
    }

    return Assets_rent.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  async "asset_rent.update.mailingSent"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    // Save for logs
    if (value == true) {
      await Meteor.callAsync("log.insert", {
        title: `Location ${id} ${value} mail sent`,
      });
    }

    return Assets_rent.updateAsync(id, {
      $set: {
        mailingSent: value,
      },
    });
  },

  // Facebook and IG page
  async "asset_rent.update.postedFBPages"({ id, value, boolean = true }) {
    proOrlocalAuthorization();

    // Add logs
    if (boolean == true) {
      await Meteor.callAsync("log.insert", {
        title: `${value} Rent asset FB page posted`,
      });
    }

    const date = new Date();

    return Assets_rent.updateAsync(id, {
      $push: { postedFBPages: { id: value, date } },
    });
  },

  async "admin.for_rent_assets.unset.postedFBPages"(id) {
    adminOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $unset: {
        postedFBPages: "",
      },
    });
  },

  ////////////////////////////////////

  async "asset_rent.update.archive"({ id, value = true }) {
    proOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        archived: value,
      },
    });
  },

  //Manage publications
  async "asset_rent.push.publications"(id, website, value) {
    proOrlocalAuthorization();
    const date = new Date();
    let update;

    if (value == true) {
      update = {
        //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
        // Otherwise, we need to create a new DB for "publications"
        $addToSet: {
          publishedOn: website.id,
        },
        $push: {
          publications: {
            published: value,
            websiteId: website.id,
            publicationDate: date,
            cost: website.cost,
            name: website.name,
          },
        },
      };
    } else {
      update = {
        //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
        // Otherwise, we need to create a new DB for "publications"
        $pull: {
          publishedOn: website.id,
          publications: { websiteId: website.id },
        },
        // $push: {
        //   publications: {
        //     published: value,
        //     websiteId: website.id,
        //     publicationDate: date,
        //     name: website.name,
        //   },
        // },
      };
    }

    return Assets_rent.updateAsync(id, update);
  },

  async "asset_rent.remove"(id) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(id);

    if (asset.mandates) {
      throw new Meteor.Error("mandate attached");
    }

    if (asset?.userId && asset?.userId == Meteor.userId()) {
      return Assets_rent.removeAsync(id);
    }
  },

  /** ADMIN */

  async "admin.asset_rent.addToSet.mandate"(id, value) {
    adminOrlocalAuthorization();
    await Assets_rent.updateAsync(id, {
      $addToSet: {
        mandates: String(value),
      },
    });
  },

  async "admin.asset_rent.pull.mandate"(id, value) {
    adminOrlocalAuthorization();
    await Assets_rent.updateAsync(id, {
      $pull: {
        mandates: String(value),
      },
    });
  },

  "admin.asset_rent.update.mandateRef"(id, value) {
    adminOrlocalAuthorization();
    return Assets_rent.updateAsync(id, {
      $set: {
        mandateRef: value,
      },
    });
  },

  async "admin.asset_rent.remove"(id) {
    adminOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(id);
    if (asset?.userId && asset?.userId == Meteor.userId()) {
      return Assets_rent.removeAsync(id);
    }
  },

  // "admin.asset_rent.removeURL"(id) {
  //   // adminOrlocalAuthorization();
  //   const assets = Assets_rent.find({});
  //   assets.forEach((x) => {
  //     if (x.images) {
  //       x.images.forEach((image) => {
  //         console.log(image.url);
  //       });
  //     }
  //   });
  // },
});
