import { Mongo } from "meteor/mongo";
// import { isAdminServer } from "../admin.ts";
import { adminOrlocalAuthorization } from "../admin.ts";
export const New_properties = new Mongo.Collection("new_properties");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("new_properties", function publication() {
    return New_properties.find();
  });

  Meteor.publish("new_properties_notsold", function publication() {
    return New_properties.find({
      "lots.number_available": { $ne: 0 },
    });
  });

  Meteor.publish("new_properties_sorted", function publication(limit) {
    return New_properties.find(
      {},
      { sort: { publication_date: -1 }, limit: limit }
    );
  });

  Meteor.publish("visible_new_properties", function publication() {
    return New_properties.find({ isVisible: true });
  });
  Meteor.publish("new_property", async function publication(id) {
    const result = await New_properties.find({ _id: id });
    return result;
  });
  Meteor.publish("new_property_slug", async function publication(id) {
    let result = await New_properties.find({ slug: id }, { limit: 1 });
    if (result.countAsync() == 0) {
      result = New_properties.find({ _id: id }, { limit: 1 });
    }
    return result;
  });
}

Meteor.methods({
  "new_properties.reset.DirectProduitImmo"() {
    return New_properties.removeAsync({ origin: "www.direct-produit-immo.fr" });
  },

  "new_properties.insertArray"(array) {
    array.forEach((object) => {
      New_properties.insertAsync(object);
    });
  },

  "new_properties.insert"(title) {
    adminOrlocalAuthorization();
    const date = new Date();

    return New_properties.insertAsync({
      title: title,
      createdAt: date,
      // userId: Meteor.userId(),
    });
  },

  "new_property.update.title"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "new_property.update.mandate"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  "new_properties.push.images"(id, value) {
    adminOrlocalAuthorization();
    let result = [];
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return New_properties.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
    });
  },
  "new_property.update.images.title"(id, i, value) {
    adminOrlocalAuthorization();
    // let result = [];
    // value = value.forEach((e) => {
    //   result.push({ title: "", url: e });
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;

    return New_properties.updateAsync(id, update);
  },

  "new_properties.pull.images"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "new_property.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        address: {
          city,
          postalCode,
          streetAddress,
          country,
        },
      },
    });
  },

  "new_property.update.contact"(id, name, phone, picture, email) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "new_property.update.saleprice"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "new_property.update.livingArea"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "new_property.update.diagnostics"(id, dpe, ges) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "new_properties.push.tag"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "new_properties.push.category"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "new_property.update.description"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "new_property.update.thumbnail"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        thumbnail: value,
      },
    });
  },
  "new_property.update.htmlcontent"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        htmlcontent: value,
      },
    });
  },

  "new_property.update.slug"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        slug: value,
      },
    });
  },

  "new_property.update.username"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  "new_property.update.externalUrl"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "new_property.update.youtubeId"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        youtubeId: value,
      },
    });
  },

  //Boolean
  "new_property.update.isVisible"(id, value) {
    adminOrlocalAuthorization();
    return New_properties.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
