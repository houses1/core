import { Mongo } from "meteor/mongo";

import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Blogs = new Mongo.Collection("blogs");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("blogs", function blogsPublication() {
    return Blogs.find();
  });
  //To be implemented
  Meteor.publish("visible_blogs", function blogsPublication() {
    return Blogs.find({ isVisible: true });
  });
  Meteor.publish("blog", function blogPublication(id) {
    const result = Blogs.find({ _id: id });
    return result;
  });
  Meteor.publish("blogslug", function blogPublication(id) {
    let result = Blogs.find({ slug: id }, { limit: 1 });
    if (result.count() == 0) {
      result = Blogs.find({ _id: id }, { limit: 1 });
    }
    return result;
  });
}

Meteor.methods({
  "blog.insert"(title) {
    adminOrlocalAuthorization();
    return Blogs.insertAsync({
      title: title,
      createdAt: new Date(),
      userId: Meteor.userId(),
    });
  },

  "blog.update.title"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "blog.push.tag"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "blog.push.category"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "blog.update.description"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "blog.update.thumbnail"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        thumbnail: value,
      },
    });
  },
  "blog.update.htmlcontent"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        htmlcontent: value,
      },
    });
  },

  "blog.update.slug"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        slug: value,
      },
    });
  },

  "blog.update.externalUrl"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "blog.update.youtubeId"(id, value) {
    adminOrlocalAuthorization();
    // Keep only youtubeId
    if (value.includes("https://www.youtube.com/watch?v=")) {
      value = value.replace("https://www.youtube.com/watch?v=", "");
    }

    return Blogs.updateAsync(id, {
      $set: {
        youtubeId: value,
      },
    });
  },

  //Boolean
  "blog.update.isVisible"(id, value) {
    adminOrlocalAuthorization();
    return Blogs.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
