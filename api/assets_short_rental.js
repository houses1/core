import { Mongo } from "meteor/mongo";

import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

import dayjs from "dayjs";

const { addWatermark_url } = require("../commonFunctions.ts");

export const Assets_short_rental = new Mongo.Collection("short_rental_assets");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("short_rental_assets", function () {
    return Assets_short_rental.find();
  });

  Meteor.publish("my_short_rental_assets", function () {
    return Assets_short_rental.find({ userId: Meteor.userId() });
  });

  Meteor.publish("short_rental_assets_owner", function () {
    const items = Assets_short_rental.find();
    const users = Meteor.users.find();
    return [items, users];
  });

  // For the similar assets in sale page
  Meteor.publish(
    "similar_short_rental_assets",
    function (type, city, excludeId) {
      return Assets_short_rental.find(
        {
          type: type,
          isVisible: true,
          archived: { $ne: true },
          _id: { $ne: excludeId },
          "address.city": city,
        },
        {
          limit: 3,
          fields: {
            copro: 0,
            mandates: 0,
            pageViews: 0,
            propertyTax: 0,
          },
        }
      );
    }
  );

  Meteor.publish("visible_short_rental_assets", function () {
    return Assets_short_rental.find({
      isVisible: true,
      archived: { $ne: true },
    });
  });

  Meteor.publish("short_rental_asset", function (id) {
    const result = Assets_short_rental.find({ _id: id });
    return result;
  });

  Meteor.publish("short_rental_asset_title", function (id) {
    const result = Assets_short_rental.find(
      { _id: id },
      { fields: { title: 1 } }
    );
    return result;
  });

  Meteor.publish("short_rental_asset_protected", async (id) => {
    let result;
    const user = await Meteor.userAsync({ fields: { roles: 1 } });

    if (user?.roles?.includes("admin")) {
      return await Assets_short_rental.find({ _id: id });
    } else {
      return await Assets_short_rental.find({
        _id: id,
        userId: Meteor.userId(),
      });
    }
  });

  Meteor.publish("short_rental_asset_slug", function (id) {
    return Assets_short_rental.find(
      { $or: [{ slug: id }, { _id: id }] },
      { limit: 1 }
    );
  });
}

Meteor.methods({
  async "short_rental_assets.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();
    const contactAgent = user.contact;

    return await Assets_short_rental.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      username: user.username,
      contact: {
        agent: contactAgent,
      },
    });
  },

  "short_rental_assets.update.title"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        title: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.increment.pageviews"(id) {
    // I would use the sessionId instead of IP address I guess, because this.connection.clientAddress always change
    if (Meteor.isServer) {
      const asset = await Assets_short_rental.findOneAsync({
        $or: [{ _id: id }, { slug: id }],
      });
      if (!asset) return;

      const ip = this.connection?.clientAddress;

      let iparray = [];
      if (asset.pageViews?.ipArray) {
        iparray = [...asset.pageViews.ipArray];
      }

      if (!ip || iparray.includes(ip)) {
        return;
      } else {
        //Keeping array of length limit
        if (iparray.length >= 10) {
          iparray.shift();
        }
        iparray.push(ip);
      }

      const currentYear = dayjs().year();
      const currentMonth = dayjs().month() + 1; // Month is zero-indexed, so we add 1

      return Assets_short_rental.updateAsync(asset._id, {
        $inc: {
          "pageViews.count": 1, //increase total count
          [`pageViews.monthlycount.${currentYear}.${currentMonth}`]: 1, //increase total count
        },
        $set: {
          "pageViews.ipArray": iparray,
        },
      });
    }
  },

  "short_rental_assets.update.type"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        type: value,
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.transactiontype"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        transactiontype: value,
        modifiedAt: new Date(),
      },
    });
  },

  //add an array
  "short_rental_assets.push.comment"({ id, text }) {
    proOrlocalAuthorization();
    const date = new Date();
    return Assets_short_rental.updateAsync(id, {
      $push: {
        comments: {
          text,
          date,
        },
      },
    });
  },

  "short_rental_assets.pull.comment"({ id, text }) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $pull: {
        comments: { text },
      },
    });
  },

  "short_rental_assets.push.images"({ id, value }) {
    proOrlocalAuthorization();
    let result = [];

    if (!value) return;

    value = value.forEach((e) => {
      result.push({ title: "", url: addWatermark_url(e), urlNoWatermark: e });
    });

    return Assets_short_rental.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
      $set: {
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;
    return Assets_short_rental.updateAsync(id, update);
  },

  "short_rental_assets.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "short_rental_assets.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.address.street"(id, street) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "address.streetAddress": street,
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.address.city"(id, city) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "address.city": city,
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.address.postalCode"(id, postalCode) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "address.postalCode": postalCode,
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.unset.bedroom"(id) {
    return Assets_short_rental.updateAsync(id, {
      $unset: {
        "roomsNumber.bedrooms": "",
      },
    });
  },

  "short_rental_assets.unset.bathroom"(id) {
    return Assets_short_rental.updateAsync(id, {
      $unset: {
        "roomsNumber.bathrooms": "",
      },
    });
  },

  "short_rental_assets.unset.kitchen"(id) {
    return Assets_short_rental.updateAsync(id, {
      $unset: {
        "roomsNumber.kitchens": "",
      },
    });
  },

  "short_rental_assets.update.rentperday"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        rentpermonth: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        livingArea: Number(value),
        modifiedAt: new Date(),
      },
    });
  },

  // "short_rental_assets.update.diagnostics"(id, dpe, ges) {
  //   proOrlocalAuthorization();
  //   return Assets_short_rental.updateAsync(id, {
  //     $set: {
  //       diagnostics: { dpe: dpe, ges: ges },
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  "short_rental_assets.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "short_rental_assets.pull.tag"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $pull: {
        tags: value,
      },
    });
  },

  "short_rental_assets.push.category"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "short_rental_assets.update.description"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        description: value,
        modifiedAt: new Date(),
      },
    });
  },

  "short_rental_assets.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // sharedWith
  async "short_rental_assets.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_short_rental.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  async "short_rental_assets.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Assets_short_rental.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  // async "short_rental_assets.update.floorNumber"(id, value) {
  //   proOrlocalAuthorization();
  //   // Careful Value should be a number but we cannot enfore if because we have value like "4+"
  //   return Assets_short_rental.updateAsync(id, {
  //     $set: {
  //       floorNumber: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  // async "short_rental_assets.update.totalFloorNumber"(id, value) {
  //   proOrlocalAuthorization();
  //   // Careful Value should be a number but we cannot enfore if because we have value like "4+"
  //   return Assets_short_rental.updateAsync(id, {
  //     $set: {
  //       totalFloorNumber: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  async "short_rental_assets.update.roomsNumber.bedrooms"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "roomsNumber.bedrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.roomsNumber.kitchens"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "roomsNumber.kitchens": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.roomsNumber.bathrooms"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "roomsNumber.bathrooms": value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.roomsNumber.parkingslots"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        "roomsNumber.parkingslots": value,
        modifiedAt: new Date(),
      },
    });
  },

  // async "short_rental_assets.update.roomsNumber.cellars"(id, value) {
  //   proOrlocalAuthorization();
  //   return Assets_short_rental.updateAsync(id, {
  //     $set: {
  //       "roomsNumber.cellars": value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  async "short_rental_assets.update.thumbnail"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        thumbnail: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.position.images"(id, value) {
    proOrlocalAuthorization();
    const asset = await Assets_short_rental.findOneAsync(id);
    const images = asset?.images;
    const index = images.findIndex((x) => x.url == value);

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, 0);
    Assets_short_rental.updateAsync(id, {
      $set: {
        images: images,
      },
    });

    return;
  },

  async "short_rental_assets.position.images.left"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_short_rental.findOneAsync(id);
    const images = asset?.images;
    const newIndex = Number(index) - 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    Assets_short_rental.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },
  async "short_rental_assets.position.images.right"(id, index) {
    proOrlocalAuthorization();
    const asset = await Assets_short_rental.findOneAsync(id);
    const images = asset?.images;
    const newIndex = Number(index) + 1;

    function arraymove(arr, fromIndex, toIndex) {
      var element = arr[fromIndex];
      arr.splice(fromIndex, 1);
      arr.splice(toIndex, 0, element);
    }

    arraymove(images, index, newIndex);
    Assets_short_rental.updateAsync(id, {
      $set: {
        images: images,
      },
    });
    return;
  },

  async "short_rental_assets.update.htmlcontent"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        htmlcontent: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.slug"({ id, value }) {
    proOrlocalAuthorization();

    const checkAsset = await Assets_short_rental.findOneAsync({ slug: value });
    if (checkAsset) throw new Meteor.Error("slug-taken");

    return Assets_short_rental.updateAsync(id, {
      $set: {
        slug: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.heating"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        heating: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.hotwater"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        hotwater: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        externalUrl: value,
        modifiedAt: new Date(),
      },
    });
  },

  async "short_rental_assets.update.youtubeId"(id, value) {
    proOrlocalAuthorization();

    let youtubeId;
    if (value.includes("https://youtu.be/")) {
      youtubeId = value.replace("https://youtu.be/", "");
    } else if (value.includes("youtube.com/shorts/")) {
      const paramString = value.split("/shorts/")[1];
      youtubeId = paramString.split("?")[0];
    } else if (value.includes("youtube.com/")) {
      const paramString = value.split("?")[1];
      const queryString = new URLSearchParams(paramString);
      youtubeId = queryString.get("v");
    } else {
      youtubeId = value;
    }

    if (!youtubeId) return;

    return Assets_short_rental.updateAsync(id, {
      $set: {
        youtubeId: youtubeId,
        modifiedAt: new Date(),
      },
    });
  },

  // async "short_rental_assets.update.constructionYear"(id, value) {
  //   proOrlocalAuthorization();
  //   return Assets_short_rental.updateAsync(id, {
  //     $set: {
  //       constructionYear: value,
  //       modifiedAt: new Date(),
  //     },
  //   });
  // },

  async "short_rental_assets.update.isVisible"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    // Save for logs
    if (value == true) {
      await Meteor.callAsync("log.insert.rent.asset.publication", {
        assetId: id,
      });
    }

    return Assets_short_rental.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  async "short_rental_assets.update.mailingSent"(id, value) {
    // value Boolean
    proOrlocalAuthorization();
    // Save for logs
    if (value == true) {
      await Meteor.callAsync("log.insert", {
        title: `Location ${id} ${value} mail sent`,
      });
    }

    return Assets_short_rental.updateAsync(id, {
      $set: {
        mailingSent: value,
      },
    });
  },

  // Facebook and IG page
  async "short_rental_assets.update.postedFBPages"({
    id,
    value,
    boolean = true,
  }) {
    proOrlocalAuthorization();

    // Add logs
    if (boolean == true) {
      await Meteor.callAsync("log.insert", {
        title: `${value} Rent asset FB page posted`,
      });
    }

    const date = new Date();

    return Assets_short_rental.updateAsync(id, {
      $push: { postedFBPages: { id: value, date } },
    });
  },

  async "admin.short_rental_assets.unset.postedFBPages"(id) {
    adminOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $unset: {
        postedFBPages: "",
      },
    });
  },

  ////////////////////////////////////////////
  async "short_rental_assets.update.archive"({ id, value = true }) {
    proOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        archived: value,
      },
    });
  },

  //Manage publications
  async "short_rental_assets.push.publications"(id, website, value) {
    proOrlocalAuthorization();
    const date = new Date();
    let update;

    if (value == true) {
      update = {
        //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
        // Otherwise, we need to create a new DB for "publications"
        $addToSet: {
          publishedOn: website.id,
        },
        $push: {
          publications: {
            published: value,
            websiteId: website.id,
            publicationDate: date,
            cost: website.cost,
            name: website.name,
          },
        },
      };
    } else {
      update = {
        //Add to set for Array ["leboncoin", "bienici,..."], it is for the passerelle, so it is easier for MongoDb to query
        // Otherwise, we need to create a new DB for "publications"
        $pull: {
          publishedOn: website.id,
          publications: { websiteId: website.id },
        },
        // $push: {
        //   publications: {
        //     published: value,
        //     websiteId: website.id,
        //     publicationDate: date,
        //     name: website.name,
        //   },
        // },
      };
    }

    return Assets_short_rental.updateAsync(id, update);
  },

  async "short_rental_assets.remove"(id) {
    proOrlocalAuthorization();
    const asset = await Assets_short_rental.findOneAsync(id);

    if (asset.mandates) {
      throw new Meteor.Error("mandate attached");
    }

    if (asset?.userId && asset?.userId == Meteor.userId()) {
      return Assets_short_rental.removeAsync(id);
    }
  },

  /** ADMIN */

  async "admin.short_rental_asset.addToSet.mandate"(id, value) {
    adminOrlocalAuthorization();
    await Assets_short_rental.updateAsync(id, {
      $addToSet: {
        mandates: String(value),
      },
    });
  },

  async "admin.short_rental_asset.pull.mandate"(id, value) {
    adminOrlocalAuthorization();
    await Assets_short_rental.updateAsync(id, {
      $pull: {
        mandates: String(value),
      },
    });
  },

  "admin.short_rental_asset.update.mandateRef"(id, value) {
    adminOrlocalAuthorization();
    return Assets_short_rental.updateAsync(id, {
      $set: {
        mandateRef: value,
      },
    });
  },

  async "admin.short_rental_asset.remove"(id) {
    adminOrlocalAuthorization();
    const asset = await Assets_short_rental.findOneAsync(id);
    if (asset?.userId && asset?.userId == Meteor.userId()) {
      return Assets_short_rental.removeAsync(id);
    }
  },

  // "admin.short_rental_asset.removeURL"(id) {
  //   // adminOrlocalAuthorization();
  //   const assets = Assets_short_rental.find({});
  //   assets.forEach((x) => {
  //     if (x.images) {
  //       x.images.forEach((image) => {
  //         console.log(image.url);
  //       });
  //     }
  //   });
  // },
});
