export const Emails = new Mongo.Collection("emails");
// For the send Verification email
import { Accounts } from "meteor/accounts-base";

import { Mandates_For_Sale } from "./mandates_sale.js";
import { Mandates_For_Rent } from "./mandates_rent.js";
import { Contacts } from "./contacts.js";
import { Assets_sale } from "./assets_sale.js";
import { Assets_rent } from "./assets_rent.js";
import { Searches } from "./searches.js";
const { formatAndcurrency } = require("../commonFunctions.ts");

import enterprise_customization from "../../enterprise_customization.ts";
const { sendingEmail, enterprise_name, website } = enterprise_customization;

Meteor.methods({
  "email.insert"(emailsArray, template, subject, description) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    const date = new Date();
    return Emails.insertAsync({
      createdAt: date,
      userId: Meteor.userId(),
      template: template,
      emails: emailsArray,
      subject: subject,
      description: description,
    });
  },

  "email.change.description"(id, description) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Emails.updateAsync(
      { _id: id },
      {
        $set: { description: description },
      }
    );
  },

  "email.remove"(id) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return Emails.removeAsync(id);
  },

  async sendMailing({ assetId, transactionType }) {
    if (!Meteor.isServer) return;

    let asset;

    if (transactionType == "sale") {
      asset = await Assets_sale.findOneAsync(assetId);
      if (!asset) return;
    }
    if (transactionType == "rent") {
      asset = await Assets_rent.findOneAsync(assetId);
      if (!asset) return;
    }

    // Contact section starts
    let contactFilter = {
      createdBy: Meteor.userId(),
      hasUnsubscribed: { $ne: true },
      "searches.isActive": true,
      "searches.cities.value": { $in: [asset.address.city] },
      "searches.type": transactionType,
      "searches.assetType": asset.type,
      emails: { $exists: true },
    };

    if (asset.livingArea) {
      contactFilter["searches.livingAreaMin"] = {
        $lte: asset.livingArea,
      };
    }

    if (asset.landSurface) {
      contactFilter["searches.landSurfaceMin"] = {
        $lte: asset.landSurface,
      };
    }

    if (transactionType == "sale") {
      contactFilter["searches.priceMax"] = {
        $gte: asset.saleprice.fees_included,
      };
    }

    if (transactionType == "rent") {
      contactFilter["searches.priceMax"] = { $gte: asset.rentpermonth };
    }

    const userContacts = await Contacts.find(contactFilter).fetch();
    // console.log({ userContacts });

    userContacts.forEach(async (contact) => {
      await Meteor.callAsync("contacts.push.notifications", {
        id: contact._id,
        assetId,
      });
    });

    let contactEmails;
    contactEmails = userContacts.map((x) => x.emails);
    // Probably could also use the flatMap feature contactEmails.flat(1)
    const merged = [].concat.apply([], contactEmails);
    // Contact section ends

    // Searches ADMIN
    let searchFilter = {
      "cities.value": { $in: [asset.address.city] },
      assetType: asset.type,
      transactionType: transactionType,
      isActive: true,
      hasUnsubscribed: { $ne: true },
    };

    if (transactionType == "sale") {
      searchFilter["priceMax"] = { $gte: asset.saleprice.fees_included };
    }

    if (transactionType == "rent") {
      searchFilter["priceMax"] = { $gte: asset.rentpermonth };
    }

    if (asset.livingArea) {
      searchFilter["livingAreaMin"] = {
        $lte: asset.livingArea,
      };
    }

    if (asset.landSurface) {
      searchFilter["landSurfaceMin"] = {
        $lte: asset.landSurface,
      };
    }

    const searches = await Searches.find(searchFilter).fetch();
    const searchesIds = searches.map((x) => x._id);

    await Meteor.callAsync("searches.push.assetIds", {
      ids: searchesIds,
      assetId: assetId,
    });

    let searchesEmails = searches.map((x) => x.email);

    // concat with contacts + searches emails
    searchesEmails = searchesEmails.concat(merged);

    // Removing duplicates
    searchesEmails = [...new Set(searchesEmails)];

    //Preparation email
    const subject = `Notification - Recherche - Anoa Home - ${
      asset.title || ""
    }`;
    let html = "";

    html += "Nous avons un nouveau bien qui peut vous correspondre";
    html += `<br/>`;
    html += `<br/>${asset.title}`;

    if (transactionType == "sale") {
      html += `<br/>${formatAndcurrency(asset.saleprice.fees_included)}`;
    }
    if (transactionType == "rent") {
      html += `<br/>${formatAndcurrency(asset.rentpermonth)} /mois`;
    }

    html += `<br/>${asset.address.city}`;

    if (asset.livingArea) {
      html += `<br/> Surface habitable : ${asset.livingArea} m2`;
    }
    if (asset.landSurface) {
      html += `<br/> Surface terrain : ${asset.landSurface} m2`;
    }

    if (asset.description) {
      html += `<br/>${asset.description}`;
    }

    html += `<br/><a href="${website}">${enterprise_name}</a>`;
    //email preparation ends

    // We are forced to send one email by one email because the unsubscription link is different for all emails
    searchesEmails.forEach(async (email) => {
      html += `<br/><br/> Vous recevez ce message car vous êtes dans la liste de diffusion. Vous pouvez vous <a href="${Meteor.absoluteUrl()}unsubscribe/${email}">désinscrire</a>`;

      await Meteor.callAsync("sendEmail", {
        bcc: searchesEmails,
        subject: subject,
        html: html,
      });
    });

    return searchesEmails;
  },

  sendVerificationLink() {
    if (!Meteor.isServer) return;

    let userId = Meteor.userId();
    if (!userId) {
      console.log("No user");
      return;
    }
    console.log("Email has been sent");
    return Accounts.sendVerificationEmail(userId);
  },

  async notifyMandateToValidate({ mandateId, type }) {
    if (!Meteor.isServer) return;

    let html = `Vous avez un mandat à valider<br/> <a href="${Meteor.absoluteUrl()}admin">Dashboard Admin</a>`;
    let subject = `Mandat à valider`;
    let mandate, asset, user;

    if (type == "sale") {
      mandate = await Mandates_For_Sale.findOneAsync(mandateId);
      asset = await Assets_sale.findOneAsync(mandate?.asset?.value);
    } else if (type == "rent") {
      mandate = await Mandates_For_Rent.findOneAsync(mandateId);
      asset = await Assets_rent.findOneAsync(mandate?.asset?.value);
    }

    if (!mandate) return;

    if (mandate.userId) {
      const userId = mandate?.userId;
      user = await Meteor.users.findOneAsync(userId);
    }

    if (asset?.title) {
      subject += ` ${asset.title}`;
      html += `<br/>${asset.title}`;
    }

    if (asset?.address?.city) {
      html += `<br/>${asset.address.city}`;
    }

    subject += ` Ref ${mandateId}`;
    html += `<br/> ${user?.contact?.firstname || ""} ${
      user?.contact?.surname || ""
    }`;

    // if (type == "sale") {
    //   html += ` <a href="${Meteor.absoluteUrl()}pro/for-sale-mandates/edit/${
    //     mandate._id
    //   }">Mandat de vente</a>`;
    // }

    // if (type == "rent") {
    //   html += ` <a href="${Meteor.absoluteUrl()}pro/for-rent-mandates/edit/${
    //     rentmandate._id
    //   }">Mandat de location</a>`;
    // }

    const result = await Meteor.callAsync("sendToTeam", {
      subject: subject,
      html: html,
    });

    return result;
  },

  async notifyRefusedMandateAgent({ from = sendingEmail, mandateId, type }) {
    if (!Meteor.isServer) return;

    let html = "";
    let mandateLink, assetLink;
    let mandate, asset;

    // Find the mandat

    if (type == "sale") {
      mandate = await Mandates_For_Sale.findOneAsync(mandateId);
      if (!mandate) return console.log("No mandate");
      asset = await Assets_sale.findOneAsync(mandate?.asset?.value);
      mandateLink = `<a href="${Meteor.absoluteUrl()}pro/for-sale-mandates/edit/${
        mandate._id
      }">Mandat de vente</a> <br/> `;

      assetLink = `<a href="${Meteor.absoluteUrl()}pro/for-sale/edit/${
        asset._id
      }">${asset.title || "Sans titre"} </a> <br/> `;
    } else if (type == "rent") {
      mandate = await Mandates_For_Rent.findOneAsync(mandateId);
      if (!mandate) return console.log("No mandate");
      asset = await Assets_rent.findOneAsync(mandate?.asset?.value);
      mandateLink = `<a href="${Meteor.absoluteUrl()}pro/for-rent-mandates/edit/${
        mandate._id
      }">Mandat de location</a> <br/> `;

      assetLink = `<a href="${Meteor.absoluteUrl()}pro/for-rent/edit/${
        asset._id
      }">${asset.title || "Sans titre"} </a> <br/> `;
    }

    if (!mandate?.userId) return;

    const user = await Meteor.users.findOneAsync(mandate?.userId);
    const to = user?.contact?.email || user?.emails?.[0].address;

    if (user?.contact?.firstname) {
      html += `${user?.contact?.firstname},<br/>`;
    }

    let subject = `Numéro de mandat refusé`;

    html += `Numéro de mandat refusé ${mandateLink}`;

    if (asset?.title) {
      subject += ` ${asset.title}`;
      html += `<br/>${asset.title}`;
    }

    if (asset?.address?.city) {
      html += `<br/>${asset?.address?.city}`;
    }

    html += `<br/>Vous devez remplir les informations et re-demander un numéro maintenant <br/>${assetLink}`;
    if (!to) {
      return;
    }

    const res = await Meteor.callAsync(
      "sendToAgent",
      { to, subject, html },
      () => {}
    );
    return res;
  },

  async notifyMandateNumberAgent({
    from = sendingEmail,
    mandateId,
    mandateNumber,
    type,
  }) {
    if (!Meteor.isServer) return;
    if (!mandateId) return;
    if (!mandateNumber) return;

    let html = "";
    let mandateLink;
    let assetLink;

    let mandate, asset;

    // Find the mandat
    if (type == "sale") {
      mandate = await Mandates_For_Sale.findOneAsync(mandateId);
      asset = await Assets_sale.findOneAsync(mandate?.asset?.value);
      mandateLink = `<a href="${Meteor.absoluteUrl()}pro/for-sale-mandates/edit/${
        mandate._id
      }">Mandat de vente ${mandate.mandate.number}</a> <br/> `;

      assetLink = `<a href="${Meteor.absoluteUrl()}pro/for-sale/edit/${
        asset._id
      }">${asset.title || "Sans titre"} </a> <br/> `;
    }

    if (type == "rent") {
      mandate = await Mandates_For_Rent.findOneAsync(mandateId);
      asset = await Assets_rent.findOneAsync(mandate?.asset?.value);
      mandateLink = `<a href="${Meteor.absoluteUrl()}pro/for-rent-mandates/edit/${
        mandate._id
      }">Mandat de location ${mandate.mandate.number}</a> <br/> `;

      assetLink = `<a href="${Meteor.absoluteUrl()}pro/for-rent/edit/${
        asset._id
      }">${asset.title || "Sans titre"} </a> <br/> `;
    }

    if (!mandate) return;
    if (!mandate?.userId) return;

    const user = await Meteor.users.findOneAsync(mandate?.userId);
    const to = user?.contact?.email || user?.emails?.[0].address;

    if (user?.contact?.firstname) {
      html += `${user?.contact?.firstname},<br/>`;
    }

    let subject = `Numéro de mandat obtenu ${mandateNumber}`;

    html += `Numéro de mandat obtenu ${mandateLink}`;

    if (asset?.title) {
      subject += ` ${asset.title}`;
      html += `<br/>${asset.title}`;
    }

    if (asset?.address?.city) {
      html += `<br/>${asset?.address?.city}`;
    }

    html += `<br/>Vous pouvez maintenant partager votre annonce <br/>${assetLink}`;

    if (!to) {
      return;
    }

    return await Meteor.callAsync(
      "sendToAgent",
      { to, subject, html },
      () => {}
    );
  },
});
