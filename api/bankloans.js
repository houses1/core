import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Bankloans = new Mongo.Collection("bankloans");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("bankloans", function publication() {
    return Bankloans.find();
  });

  Meteor.publish("bankloans_owned_and_public", function publication() {
    return Bankloans.find({
      $or: [
        {
          userId: Meteor.userId(),
        },
        {
          isPublic: true,
        },
      ],
    });
  });

  Meteor.publish("bankloan", function publication(id) {
    return Bankloans.find({ _id: id });
  });
}

Meteor.methods({
  "bankloan.insert"() {
    proOrlocalAuthorization();
    const date = new Date();
    return Bankloans.insertAsync({
      // title: title,
      createdAt: date,
      userId: Meteor.userId(),
      // username: user.username,
      // contact: {
      //   agent: contactAgent,
      // },
    });
  },

  "bankloan.update.title"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "bankloan.update.status"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        status: value,
      },
    });
  },

  "bankloan.update.financing"({ id, object }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        financing: object,
      },
    });
  },

  // soulte
  "bankloan.update.compensation"({ id, object }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        compensation: object,
      },
    });
  },

  // contacts
  "bankloan.update.contacts.contactA"({ id, object }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "contacts.contactA": object,
      },
    });
  },

  "bankloan.update.contacts.contactB"({ id, object }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "contacts.contactB": object,
      },
    });
  },

  // Asset
  "bankloan.update.asset"({ id, object }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        asset: object,
      },
    });
  },

  "bankloan.update.isPublic"({ id, value }) {
    proOrlocalAuthorization();
    // Get a boolean
    value = value === "true";

    return Bankloans.updateAsync(id, {
      $set: {
        isPublic: value,
      },
    });
  },

  "bankloan.update.hasUnemploymentInsuranceA"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "insurance.hasUnemploymentInsuranceA": value,
      },
    });
  },
  "bankloan.update.hasUnemploymentInsuranceB"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "insurance.hasUnemploymentInsuranceB": value,
      },
    });
  },

  "bankloan.update.hasInsuranceDelegationA"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "insurance.hasDelegationA": value,
      },
    });
  },
  "bankloan.update.hasInsuranceDelegationB"({ id, value }) {
    proOrlocalAuthorization();
    return Bankloans.updateAsync(id, {
      $set: {
        "insurance.hasDelegationB": value,
      },
    });
  },

  "bankloan.remove"(id, value) {
    proOrlocalAuthorization();
    return Bankloans.removeAsync(id);
  },
});
