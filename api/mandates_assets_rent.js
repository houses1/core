import { Mandates_For_Rent } from "./mandates_rent";
import { Assets_rent } from "./assets_rent";

if (Meteor.isServer) {
  Meteor.publish("mandate__asset__for_rent_id", async function publication(id) {
    const mandates = Mandates_For_Rent.find({ _id: id }, { limit: 1 });
    const mandateSingle = await mandates.fetchAsync();

    if (!mandateSingle) {
      return;
    }

    const assetId = mandateSingle[0].asset.value;
    let assets = Assets_rent.find({ _id: assetId }, { limit: 1 });

    return [mandates, assets];
  });
}
