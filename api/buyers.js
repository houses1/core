import { Mongo } from "meteor/mongo";

// import {
//   isAdminServer,
//   adminOrlocalAuthorization,
//   proOrlocalAuthorization,
// } from "../admin";

export const Buyers = new Mongo.Collection("buyers");

// NOT IN USE AT THE MOMENT, SEARCHES ARE ASSOCIATED TO THE USER AT THE MOMENT
// To be used for the admin, buyers

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("buyers", function publications() {
    return Buyers.find();
  });

  Meteor.publish("buyersLimit5", function publications() {
    return Buyers.find({}, { sort: { createdAt: -1 }, limit: 5 });
  });

  Meteor.publish("myBuyers", function publications() {
    return Buyers.find({ userId: Meteor.userId(), type: { $ne: "admin" } });
  });

  //for admin
  Meteor.publish("myBuyersAdmin", function publications() {
    return Buyers.find({ userId: Meteor.userId(), type: "admin" });
  });

  //for admin
  Meteor.publish("buyersAdmin", function publications() {
    return Buyers.find({ type: "admin" });
  });

  //To be implemented
  Meteor.publish("visible_buyers", function publications() {
    return Buyers.find({ isVisible: true });
  });
}

Meteor.methods({
  "buyer.insert.admin"(title) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.insertAsync({
      title: title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      type: "admin",
      landSurfaceMin: 0,
      livingAreaMin: 0,
      priceMin: 0,
    });
  },

  "buyer.insert.profile"({
    type,
    email,
    assetType,
    cities,
    landSurfaceMin = 0,
    livingAreaMin = 0,
    priceMin = 0,
    priceMax,
    phone,
    name,
    notes,
  }) {
    return Buyers.insertAsync({
      type,
      assetType,
      createdAt: new Date(),
      email,
      landSurfaceMin,
      livingAreaMin,
      priceMin,
      cities,
      phone,
      name,
      notes,
      priceMax,
      isActive: false,
      activeMonthDuration: 6,
    });
  },

  "buyer.update.isActive"({ id, isActive }) {
    return Buyers.updateAsync(id, {
      $set: {
        isActive: isActive,
      },
    });
  },

  // From the unsubscribe page
  "buyer.update.hasUnsubscribed"({ id, value = true }) {
    return Buyers.updateAsync(id, {
      $set: {
        hasUnsubscribed: value,
      },
    });
  },

  "buyer.update.title"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "buyer.update.email"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $set: {
        email: value,
      },
    });
  },

  "buyer.update.notes"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $set: {
        notes: value,
      },
    });
  },

  // "buyer.update.priceMin"(id, value) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $set: {
  //       priceMin: value,
  //     },
  //   });
  // },

  // "buyer.update.livingAreaMin"(id, value) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $set: {
  //       livingAreaMin: value,
  //     },
  //   });
  // },

  // "buyer.update.landSurfaceMin"(id, value) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $set: {
  //       landSurfaceMin: value,
  //     },
  //   });
  // },

  // "buyer.update.cities"(id, value) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $set: {
  //       cities: value,
  //     },
  //   });
  // },

  // "buyer.unset.cities"(id) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $unset: {
  //       cities: "",
  //     },
  //   });
  // },

  // "buyer.update.priceMax"(id, value) {
  //   if (!this.userId) throw new Meteor.Error("not-authorized");
  //   return Buyers.updateAsync(id, {
  //     $set: {
  //       priceMax: value,
  //     },
  //   });
  // },

  "buyer.push.tag"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  // After sending a notification we record
  // createdAt: new Date(),
  "buyer.push.assetId"({ id, assetId }) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $push: {
        notifications: {
          assetId: assetId,
          date: new Date(),
        },
      },
    });
  },
  // Push arrays
  "buyer.push.assetIds"({ ids, assetId }) {
    if (!this.userId) throw new Meteor.Error("not-authorized");

    ids.forEach((id) => {
      Buyers.updateAsync(id, {
        $push: {
          notifications: {
            assetId: assetId,
            date: new Date(),
          },
        },
      });
    });
  },

  "buyer.push.category"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "buyer.update.description"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  //Boolean
  "buyer.update.isVisible"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Buyers.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  // Add security
  "buyer.remove"(id) {
    // adminOrlocalAuthorization();
    return Buyers.removeAsync({ _id: id });
  },
});
