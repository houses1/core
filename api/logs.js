import { Mongo } from "meteor/mongo";
// import { Mandates_For_Rent } from "./mandates_rent.js";
// import { Mandates_For_Sale } from "./mandates_sale.js";
import { Assets_rent } from "./assets_rent.js";
import { Assets_sale } from "./assets_sale.js";

import enterprise_customization from "../../enterprise_customization.ts";
const { currency } = enterprise_customization;

import dayjs from "dayjs";

const { formatNumber, formatAndcurrency } = require("../commonFunctions.ts");

import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Logs = new Mongo.Collection("logs");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("logs", function logsPublication() {
    return Logs.find();
  });

  Meteor.publish("logs_dashboard", function logsPublication() {
    const days = 20;
    let limitDate = dayjs().subtract(days, "day").format("YYYY-MM-DD");
    limitDate = new Date(limitDate);

    return Logs.find(
      {
        createdAt: { $gte: limitDate },
        type: { $nin: ["logs", "log", "estimation", "search"] },
      },
      { limit: 8, sort: { createdAt: -1 } }
    );
  });

  // Meteor.publish("visible_logs", function logsPublication() {
  //   return Logs.find({ isVisible: true });
  // });

  Meteor.publish("log", function logPublication(id) {
    const result = Logs.find({ _id: id });
    return result;
  });
}

Meteor.methods({
  "log.insert"({ title, type = "log", tag, details = "" }) {
    return Logs.insertAsync({
      title,
      details,
      type,
      tag,
      createdAt: new Date(),
      userId: Meteor.userId(),
    });
  },

  // When is published on the website
  async "log.insert.sale.asset.publication"({ assetId }) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(assetId);

    console.log({ asset });

    if (!asset) {
      throw new Meteor.Error("asset-not-found", "Asset not found");
    }

    const user = await Meteor.userAsync();
    const link = `/for-sale/${assetId}`;
    const title = `<a href="/for-sale/${assetId}">${asset?.title}</a>`;
    const price = `${formatAndcurrency(asset.saleprice?.fees_included)}`;

    let logObject = {
      price,
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag: "Nouveau",
      type: "Vente",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      link,
    };

    if (asset?.images) {
      logObject.images = asset?.images;
    }

    return Logs.insertAsync(logObject);
  },

  async "log.insert.rent.asset.publication"({ assetId }) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(assetId);
    if (!asset) return;

    const user = await Meteor.userAsync();
    const link = `/for-rent/${assetId}`;
    const title = `<a href="/for-rent/${assetId}">${asset?.title}</a>`;
    const price = `${asset.rentpermonth} ${currency} / mois`;

    let object = {
      price,
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag: "Nouveau",
      type: "Location",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      link,
    };

    if (asset?.images) {
      object.images = asset?.images;
    }

    return Logs.insertAsync(object);
  },

  async "log.insert.sale.asset.sold"({ assetId }) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(assetId);
    if (!asset) return;

    const user = await Meteor.userAsync();

    const link = `/for-sale/${assetId}`;
    const title = `<a href="/for-sale/${assetId}">${asset?.title}</a>`;

    let object = {
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag: "Vendu",
      type: "Vente",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      link,
    };

    if (asset?.images) {
      object.images = asset?.images;
    }

    return Logs.insertAsync(object);
  },

  async "log.insert.sale.asset.archive"({ assetId }) {
    proOrlocalAuthorization();
    const asset = await Assets_sale.findOneAsync(assetId);
    if (!asset) return;

    const user = await Meteor.userAsync();
    const link = `/for-sale/${assetId}`;
    const title = `<a href="/for-sale/${assetId}">${asset?.title}</a>`;

    let object = {
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag: "Archivé",
      type: "Vente",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      link,
    };

    if (asset?.images) {
      object.images = asset?.images;
    }

    return Logs.insertAsync(object);
  },

  async "log.insert.sale.asset.tag"({ assetId, tag }) {
    proOrlocalAuthorization();

    const asset = await Assets_sale.findOneAsync(assetId);
    if (!asset) return;

    const user = await Meteor.userAsync();
    const link = `/for-sale/${assetId}`;
    const title = `<a href="/for-sale/${assetId}">${asset?.title}</a>`;

    let object = {
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag,
      type: "Vente",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      link,
    };

    if (asset?.images) {
      object.images = asset?.images;
    }
    return Logs.insertAsync(object);
  },

  async "log.insert.rent.asset.rented"({ assetId }) {
    proOrlocalAuthorization();
    const asset = await Assets_rent.findOneAsync(assetId);
    if (!asset) return;

    const user = await Meteor.userAsync();
    const link = `/for-rent/${assetId}`;
    const title = `<a href="/for-rent/${assetId}">${asset?.title}</a>`;

    return Logs.insertAsync({
      title,
      createdAt: new Date(),
      userId: Meteor.userId(),
      tag: "Loué",
      type: "Location",
      username: user?.contact?.firstname,
      userpicture: user?.contact?.picture,
      images: asset?.images,
      link,
    });
  },

  async "log.update.title"(id, value) {
    adminOrlocalAuthorization();
    return Logs.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  async "log.push.tag"(id, value) {
    adminOrlocalAuthorization();
    return Logs.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  async "log.push.category"(id, value) {
    adminOrlocalAuthorization();
    return Logs.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  async "log.update.description"(id, value) {
    adminOrlocalAuthorization();
    return Logs.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  async "log.remove"(id) {
    proOrlocalAuthorization();
    return Logs.removeAsync(id);
  },

  async "log.clean"() {
    proOrlocalAuthorization();
    const pastDate = new Date();
    const NumberOfDays = 30;
    pastDate.setDate(pastDate.getDate() - NumberOfDays);

    return Logs.removeAsync({
      createdAt: { $lt: pastDate },
    });
  },

  // "logs.reset.all"() {
  //   return Logs.removeAsync({});
  // },
});
