import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Sale_history = new Mongo.Collection("sale_history");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("sale_history", function publication() {
    return Sale_history.find();
  });

  Meteor.publish("my_sale_history", function publication() {
    return Sale_history.find({ userId: Meteor.userId() });
  });

  Meteor.publish("sale_history_owner", function publication() {
    const assets = Sale_history.find();
    const users = Meteor.users.find();
    return [assets, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("sale_history_list", function publication() {
    return Sale_history.find();
  });

  Meteor.publish("visible_sale_history", function publication() {
    return Sale_history.find({ isVisible: true });
  });
  Meteor.publish("this_sale_history", function publication(id) {
    const result = Sale_history.find({ _id: id });
    return result;
  });
}

Meteor.methods({
  async "sale_history.insert"({
    title,
    city,
    postalCode,
    type,
    livingArea,
    landSurface,
    soldPrice,
    soldDate,
    description,
  }) {
    // console.log(this.userId);
    proOrlocalAuthorization();
    const date = new Date();
    const user = await Meteor.userAsync();

    // agent contact
    const contactAgent = user && user.contact;
    //Email
    if (contactAgent && !contactAgent.email) {
      contactAgent.email = user?.emails?.[0]?.address;
    }

    return Sale_history.insertAsync({
      address: { city: city, postalCode: postalCode },
      title: title,
      type: type,
      createdAt: date,
      userId: Meteor.userId(),
      livingArea: Number(livingArea),
      landSurface: Number(landSurface),
      soldPrice: Number(soldPrice),
      soldDate: soldDate,
      description: description,
      contact: {
        agent: contactAgent,
      },
    });
  },

  "sale_history.update.title"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "sale_history.update.type"(id, value) {
    proOrlocalAuthorization();

    return Sale_history.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "sale_history.update.mandate"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  ////add an array
  "sale_history.push.images"(id, value) {
    proOrlocalAuthorization();
    let result = [];
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return Sale_history.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
    });
  },
  "sale_history.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;

    return Sale_history.updateAsync(id, update);
  },

  "sale_history.pull.images"(id, value) {
    proOrlocalAuthorization();

    return Sale_history.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "sale_history.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "sale_history.update.address.city"(id, city) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        address: {
          city: city,
        },
      },
    });
  },

  "sale_history.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "sale_history.update.saleprice"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "sale_history.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        livingArea: Number(value),
      },
    });
  },

  "sale_history.update.constructionYear"(id, value) {
    proOrlocalAuthorization();
    if (value > 3000) {
      throw new Meteor.Error("wrong date");
    }

    return Sale_history.updateAsync(id, {
      $set: {
        constructionYear: value,
      },
    });
  },

  "sale_history.update.landSurface"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        landSurface: Number(value),
      },
    });
  },

  "sale_history.cadastre.value"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "cadastre.reference": value,
      },
    });
  },

  "sale_history.update.diagnostics.dpe"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "diagnostics.dpe": value,
      },
    });
  },

  "sale_history.update.diagnostics.ges"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "diagnostics.ges": value,
      },
    });
  },

  "sale_history.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "sale_history.push.category"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "sale_history.update.roomsNumber.bedrooms"(id, value) {
    proOrlocalAuthorization();
    // Careful Value should be a number but we cannot enfore if because we have value like "4+"
    return Sale_history.updateAsync(id, {
      $set: {
        "roomsNumber.bedrooms": value,
      },
    });
  },

  "sale_history.update.roomsNumber.kitchens"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "roomsNumber.kitchens": value,
      },
    });
  },

  "sale_history.update.roomsNumber.bathrooms"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "roomsNumber.bathrooms": value,
      },
    });
  },

  "sale_history.update.roomsNumber.parkingslots"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "roomsNumber.parkingslots": value,
      },
    });
  },

  "sale_history.update.roomsNumber.cellars"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        "roomsNumber.cellars": value,
      },
    });
  },

  "sale_history.update.description"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "sale_history.update.thumbnail"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        thumbnail: value,
      },
    });
  },

  "sale_history.update.slug"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        slug: value,
      },
    });
  },

  "sale_history.update.username"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  "sale_history.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  async "sale_history.remove"(id) {
    proOrlocalAuthorization();
    const asset = await Sale_history.findOneAsync(id);
    if (asset?.userId && asset.userId == Meteor.userId()) {
      return Sale_history.removeAsync(id);
    }
  },

  //Boolean
  "sale_history.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Sale_history.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
