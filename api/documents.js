import { Mongo } from "meteor/mongo";
import { isAdminServer } from "../admin.ts";

export const Documents = new Mongo.Collection("documents");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("documents", function Publication() {
    return Documents.find();
  });

  Meteor.publish("thisuser", function Publication() {
    return Documents.find(Meteor.userId(), {
      limit: 1,
      fields: { createdAt: 0 },
    });
  });
}

Meteor.methods({
  "document.insert"(title) {
    if (!isAdminServer(this.userId)) {
      throw new Meteor.Error("not-authorized");
    }
    return Documents.insertAsync({
      title: title,
      createdAt: new Date(),
      userId: Meteor.userId(),
    });
  },

  "document.update.name"(value) {
    return Documents.updateAsync(Meteor.userId(), {
      $set: {
        "contact.name": value,
      },
    });
  },
});
