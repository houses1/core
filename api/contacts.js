import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const Contacts = new Mongo.Collection("contacts");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("contacts", function () {
    return Contacts.find();
  });

  Meteor.publish("mycontacts", function () {
    return Contacts.find({ createdBy: Meteor.userId() });
  });

  Meteor.publish("landlords", function () {
    return Contacts.find({ type: "landlord" });
  });

  Meteor.publish("mylandlords", function () {
    return Contacts.find({ type: "landlord", createdBy: Meteor.userId() });
  });

  Meteor.publish("contactsandowners", function () {
    const contacts = Contacts.find();
    const users = Meteor.users.find();
    return [contacts, users];
  });

  Meteor.publish("mycontactsandowners", function () {
    // OLD
    // const contacts = Contacts.find({ createdBy: Meteor.userId() });

    // NEW
    const contacts = Contacts.find({
      $or: [
        { createdBy: Meteor.userId() },
        { sharedWith: Meteor.userId() },
        { userId: Meteor.userId() },
      ],
    });

    const users = Meteor.users.find();
    return [contacts, users];
  });

  Meteor.publish("contacts_sorted", function (limit) {
    return Contacts.find({}, { sort: { publication_date: -1 }, limit: limit });
  });

  Meteor.publish("visible_contacts", function () {
    return Contacts.find({ isVisible: true });
  });
  Meteor.publish("contact", function (id) {
    const result = Contacts.find({ _id: id });
    return result;
  });

  // Sale
  //   Meteor.publish("contactsInSearchSale", function publication(id) {
  //     const result = Contacts.find({ searches: { $exists: true } });
  //     return result;
  //   });
  // }

  Meteor.publish(
    "contactsInSearchSale",
    function ({ assetPrice, assetType, location }) {
      // location missing....

      const result = Contacts.find({
        searches: {
          $elemMatch: {
            type: "sale",
            priceMax: { $gt: assetPrice },
            assetType,
          },
        },
      });
      return result;
    }
  );
}

Meteor.methods({
  "contact.insert.creation"(contactObject) {
    const date = new Date();
    return Contacts.insertAsync({
      gender: contactObject.gender,
      surname: contactObject.surname,
      firstname: contactObject.firstname,
      emails: [contactObject.email],
      phone: [contactObject.phone],
      address: contactObject.address,
      birthdate: contactObject.birthdate,
      nationality: contactObject.nationality,
      createdAt: date,
      createdBy: Meteor.userId(),
      userId: Meteor.userId(),
    });
  },

  "contact.insert"() {
    const date = new Date();
    return Contacts.insertAsync({
      createdAt: date,
      createdBy: Meteor.userId(),
      userId: Meteor.userId(),
    });
  },

  "contact.update"({
    id,
    gender,
    surname,
    firstname,
    email,
    phone,
    address,
    birthdate,
    birthplace,
    iban,
    bic,
    representedBy,
    nationality,
    notes,
  }) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        gender,
        surname,
        firstname,
        emails: [email],
        phone: [phone],
        address,
        birth: { birthdate: birthdate, birthplace: birthplace },
        bank: { bic: bic, iban: iban },
        representedBy,
        nationality,
        notes: notes,
      },
    });
  },

  // Duplicate
  async "contact.duplicate"(id) {
    proOrlocalAuthorization();
    const contact = await Contacts.findOneAsync(id);
    if (!contact) return;

    const date = new Date();
    contact.createdAt = date;

    delete contact["_id"];
    delete contact["mandate"];
    delete contact["status"];

    return Contacts.insertAsync(contact);
  },

  "contact.update.title"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "contact.update.gender"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        gender: value,
      },
    });
  },

  "contact.update.maritalstatus"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        maritalstatus: value,
      },
    });
  },

  "contact.update.mandate"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  "contact.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  "contact.update.hasUnsubscribed"({ id, value = true }) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(
      id,
      {
        $set: {
          hasUnsubscribed: value,
        },
      },
      { multi: true }
    );
  },

  "contact.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Contacts.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "contact.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Contacts.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  "contact.push.type"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $addToSet: {
        type: value,
      },
    });
  },

  "contact.pull.type"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $pull: {
        type: value,
      },
    });
  },

  "contacts.push.notifications"({ id, assetId }) {
    proOrlocalAuthorization();
    const createdAt = new Date();
    return Contacts.updateAsync(id, {
      $push: {
        notifications: {
          createdAt,
          assetId,
        },
      },
    });
  },

  "contacts.push.searches"({
    id,
    priceMin,
    priceMax,
    cities,
    type,
    assetType,
    landSurfaceMin,
    livingAreaMin,
  }) {
    proOrlocalAuthorization();
    const createdAt = new Date();
    return Contacts.updateAsync(id, {
      $push: {
        searches: {
          priceMin,
          priceMax,
          cities,
          createdAt,
          type,
          assetType,
          landSurfaceMin,
          livingAreaMin,
        },
      },
    });
  },

  async "contacts.pull.searches"({ id, index }) {
    proOrlocalAuthorization();
    const contact = await Contacts.findOneAsync(id);
    const toberemoved = contact?.searches?.[index];
    console.log({ contact });

    if (!toberemoved) return;

    return Contacts.updateAsync(id, {
      $pull: {
        searches: toberemoved,
      },
    });
  },

  async "contacts.searches.isActive"({ id, index, value = true }) {
    proOrlocalAuthorization();
    const contact = await Contacts.findOneAsync(id);
    const searchItem = contact?.searches?.[index];
    if (!searchItem) return;
    searchItem.isActive = value;

    const field = [`searches.$[${index}]`];

    let newSearchesArray = contact?.searches;
    newSearchesArray[index] = searchItem;

    return Contacts.updateAsync(id, {
      $set: {
        searches: newSearchesArray,
      },
    });
  },

  "contact.push.images"(id, value) {
    proOrlocalAuthorization();
    let result = [];
    value = value.forEach((e) => {
      result.push({ title: "", url: e });
    });

    return Contacts.updateAsync(id, {
      $push: {
        images: {
          $each: result,
        },
      },
    });
  },
  "contact.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;

    return Contacts.updateAsync(id, update);
  },

  "contact.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "contact.update.address"(id, postalCode, streetAddress, city, country = "") {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "contact.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "contact.update.saleprice"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "contact.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "contact.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "contact.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "contact.push.category"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "contact.update.description"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "contact.update.thumbnail"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        thumbnail: value,
      },
    });
  },

  "contact.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  // Add security
  "contact.remove"(id) {
    proOrlocalAuthorization();
    return Contacts.removeAsync({ _id: id });
  },

  //Boolean
  "contact.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Contacts.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
