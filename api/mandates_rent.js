import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";
import enterprise_customization from "../../enterprise_customization.ts";
export const Mandates_For_Rent = new Mongo.Collection("mandates_for_rent");

import { Mandates_For_Sale } from "./mandates_sale.js";
import { Assets_rent } from "./assets_rent.js";

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("mandates_for_rent_all", function publication() {
    return Mandates_For_Rent.find();
  });

  Meteor.publish("mandates_for_rent_seller", function publication(id) {
    return Mandates_For_Rent.find({ landlord: id });
  });

  //Update with a list, an array of IDs
  Meteor.publish("mandates_for_rent_list", function publication() {
    return Mandates_For_Rent.find();
  });

  Meteor.publish("mandates_for_rent_owner", function publication() {
    const mandates = Mandates_For_Rent.find();
    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });

  Meteor.publish("mandates_for_rent_record", function publication() {
    return Mandates_For_Rent.find({ mandate: { $exists: true } });
  });

  Meteor.publish("mandates_for_rent_statistics", function publication(year) {
    if (year) {
      return Mandates_For_Rent.find({
        mandate: { $exists: true },
        createdAt: {
          $gte: new Date(year, 1, 1, 0, 0, 0),
          $lt: new Date(Number(year) + Number(1), 1, 1, 0, 0, 0),
        },
      });
    } else {
      return Mandates_For_Rent.find({ mandate: { $exists: true } });
    }
  });

  Meteor.publish("mandatesForRent_from_asset", function publication(assetId) {
    return Mandates_For_Rent.find({
      "asset.value": assetId,
    });
  });

  // Meteor.publish("visible_mandates_for_rent", function publication() {
  //   return mandates_for_rent.find({ isVisible: true });
  // });
  // Meteor.publish("mandate_for_rent", function publication(id) {
  //   const result = mandates_for_rent.find({ _id: id });
  //   return result;
  // });

  Meteor.publish("mandate_for_rent_id", function publication(id) {
    return Mandates_For_Rent.find({ _id: id }, { limit: 1 });
  });

  Meteor.publish(
    "mandate_for_rent_id_protected",
    async function publication(id) {
      let result;

      const user = await Meteor.userAsync({ fields: { roles: 1 } });

      if (user?.roles?.includes("admin")) {
        result = Mandates_For_Rent.find({ _id: id }, { limit: 1 });
      } else {
        result = Mandates_For_Rent.find(
          { _id: id, userId: Meteor.userId() },
          { limit: 1 }
        );
      }
      return result;
    }
  );

  Meteor.publish("mandate_for_rent_arrayid", function publication(array) {
    let result = Mandates_For_Rent.find({ _id: { $in: array } });
    // if (result.count() == 0) {
    //   result = Mandates_For_Rent.find({ _id: id }, { limit: 1 });
    // }

    return result;
  });

  Meteor.publish("mandate_for_rent_number", function (id) {
    return Mandates_For_Rent.find({ mandate: id }, { limit: 1 });
  });

  Meteor.publish("my_mandates_for_rent", function publication() {
    const mandates = Mandates_For_Rent.find({ userId: Meteor.userId() });
    return mandates;
  });

  Meteor.publish("my_mandates_for_rent_owner", function publication() {
    const mandates = Mandates_For_Rent.find({ userId: Meteor.userId() });
    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });
}

Meteor.methods({
  async "mandates_for_rent.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();
    // agent contact
    const user = await Meteor.userAsync();
    const contactAgent = user?.contact;

    return Mandates_For_Rent.insertAsync({
      // _id: mandate,
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      // mandate: mandate,
      contact: {
        agent: contactAgent,
      },
    });
  },

  async "mandates_for_rent.insertwithasset"(assetId) {
    proOrlocalAuthorization();
    const date = new Date();

    // agent contact
    const user = await Meteor.userAsync();
    const contactAgent = user?.contact;

    const asset = await Assets_rent.findOneAsync(assetId);

    let rent_without_charges;
    if (asset?.rentpermonth) {
      rent_without_charges = asset?.rentpermonth;
    }

    const resultId = await Mandates_For_Rent.insertAsync({
      // _id: mandate,
      // title: title,
      createdAt: date,
      userId: Meteor.userId(),
      asset: { value: assetId },
      contact: {
        agent: contactAgent,
      },
      rent: { rent_without_charges: Number(rent_without_charges) },
    });

    await Assets_rent.updateAsync(assetId, {
      $push: {
        mandates: String(resultId),
      },
    });

    return resultId;
  },

  // Duplicate - clone
  async "mandates_for_rent.duplicate"(id) {
    proOrlocalAuthorization();
    const mandate = await Mandates_For_Rent.findOneAsync(id);

    if (!mandate) return;

    const date = new Date();
    mandate.createdAt = date;

    delete mandate["_id"];
    delete mandate["mandate"];
    delete mandate["status"];
    delete mandate["signatureDate"];
    delete mandate["startingDate"];

    return Mandates_For_Rent.insertAsync(mandate);
  },

  // For give
  "mandates_for_rent.update.userId"({ id, value }) {
    proOrlocalAuthorization();

    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // For share
  "mandates_for_rent.update.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        sharedWith: [value],
      },
    });
  },

  "mandates_for_rent.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Mandates_For_Rent.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "mandates_for_rent.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();

    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");

    return Mandates_For_Rent.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  "mandates_for_rent.update.rent_without_charges"(id, rent_without_charges) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "rent.rent_without_charges": Number(rent_without_charges),
      },
    });
  },
  "mandates_for_rent.update.charges"(id, charges) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "rent.charges": Number(charges),
      },
    });
  },

  "mandates_for_rent.update.deposit"(id, deposit) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "rent.deposit": Number(deposit),
      },
    });
  },

  "mandates_for_rent.update.isFurnished"(id, boolean = true) {
    proOrlocalAuthorization();
    if (boolean == "false") boolean = false;
    if (boolean == "true") boolean = true;

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isFurnished: boolean,
      },
    });
  },

  async "mandates_for_rent.update.isRented"(id, boolean = true) {
    proOrlocalAuthorization();
    if (boolean == "false") boolean = false;
    if (boolean == "true") boolean = true;

    const mandate = await Mandates_For_Rent.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_rent.update.isRented", asset, boolean);
    }

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isRented: boolean,
      },
    });
  },

  async "mandates_for_rent.update.isRentedByOthers"(id, boolean = true) {
    proOrlocalAuthorization();
    if (boolean == "false") boolean = false;
    if (boolean == "true") boolean = true;

    const mandate = await Mandates_For_Rent.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync(
        "asset_rent.update.isRentedByOthers",
        asset,
        boolean
      );
    }

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isRentedByOthers: boolean,
      },
    });
  },

  async "mandates_for_rent.update.isCancelled"(id, boolean = true) {
    proOrlocalAuthorization();
    if (boolean == "false") boolean = false;
    if (boolean == "true") boolean = true;

    const mandate = Mandates_For_Rent.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_rent.update.isVisible", asset, !boolean);
    }

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isCancelled: boolean,
      },
    });
  },

  async "mandates_for_rent.update.isExpired"(id, boolean = true) {
    proOrlocalAuthorization();
    if (boolean == "false") boolean = false;
    if (boolean == "true") boolean = true;

    const mandate = Mandates_For_Rent.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_rent.update.isVisible", asset, !boolean);
    }

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isExpired: boolean,
      },
    });
  },

  "mandates_for_rent.update.notes"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        notes: value,
      },
    });
  },

  "mandates_for_rent.update.provisions"(id, deposit) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "rent.provisions": Number(deposit),
      },
    });
  },

  //Fees
  "mandates_for_rent.update.fees.rental"(id, value) {
    proOrlocalAuthorization();

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "fees.rental": Number(value),
      },
    });
  },

  "mandates_for_rent.update.fees.lease"(id, value) {
    proOrlocalAuthorization();

    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "fees.lease": Number(value),
      },
    });
  },

  "mandates_for_rent.update.fees.propertystate"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        "fees.propertystate": Number(value),
      },
    });
  },

  "mandates_for_rent.update.startingDate"({ mandateId, date }) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(mandateId, {
      $set: {
        startingDate: date,
      },
    });
  },

  "mandates_for_rent.unset.startingDate"({ mandateId }) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(mandateId, {
      $unset: {
        startingDate: "",
      },
    });
  },

  "mandates_for_rent.update.signatureDate"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        signatureDate: value,
      },
    });
  },

  "mandates_for_rent.update.validityDuration"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        validityDuration: value,
      },
    });
  },

  "mandates_for_rent.update.type"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "mandates_for_rent.unset.type"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $unset: {
        type: "",
      },
    });
  },

  async "mandates_for_rent.update.landlord"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        landlord: value,
      },
    });
  },

  async "mandates_for_rent.update.asset"(id, asset) {
    proOrlocalAuthorization();
    // Push a reference on the asset
    await Assets_rent.updateAsync(asset.value, {
      $push: {
        mandates: String(id),
      },
    });
    //Update Mandate for rent
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        asset: asset,
      },
    });
  },

  async "mandates_for_rent.unset.asset"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $unset: {
        asset: "",
      },
    });
  },

  async "mandates_for_rent.update.deed"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        deed: value,
      },
    });
  },

  async "mandates_for_rent.update.title"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  // generation number
  async "mandates_for_rent.add.mandatenumber"(id) {
    proOrlocalAuthorization();

    const mandateNumber =
      await enterprise_customization.generateMandateNumber();
    if (!mandateNumber) return;

    const mandate = await Mandates_For_Rent.findOneAsync(id);
    if (mandate?.mandate) {
      throw new Meteor.Error("Error", "Mandate  already has a number");
    }
    //Check
    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });

    const saleMandateNumerCheck = await Mandates_For_Sale.findOneAsync({
      "mandate.number": mandateNumber,
    });

    if (rentMandateNumerCheck || saleMandateNumerCheck) {
      throw new Meteor.Error("Error", "Mandate number already used");
    }

    await Mandates_For_Rent.updateAsync(
      { _id: id },
      {
        $set: {
          "mandate.number": mandateNumber,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );
    // Update mandate ref for asset
    if (mandate?.asset?.value) {
      await Assets_rent.updateAsync(mandate.asset.value, {
        $set: {
          mandateRef: mandateNumber,
        },
      });
    }

    return mandateNumber;
  },

  async "mandates_for_rent.update.status"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        status: value,
      },
    });
  },

  async "mandates_for_rent.update.mandate"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        mandate: value,
      },
    });
  },

  async "mandates_for_rent.remove"(id) {
    proOrlocalAuthorization();
    //A mandate with mandate number cannot be deleted
    const thismandate = await Mandates_For_Rent.findOneAsync(id);
    if (thismandate?.mandate) throw new Meteor.Error("Mandate has number");
    return Mandates_For_Rent.removeAsync(id);
  },

  "mandates_for_rent.update.isArchived"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isArchived: true,
      },
    });
  },

  async "mandates_for_rent.push.links"(id, title, url) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $push: {
        links: {
          title: title,
          url: url,
        },
      },
    });
  },

  // "mandates_for_rent.push.images"(id, value) {
  //   proOrlocalAuthorization();
  //   let result = [];
  //   value = value.forEach((e) => {
  //     result.push({ title: "", url: e });
  //   });
  //   return Mandates_For_Rent.updateAsync(id, {
  //     $push: {
  //       images: {
  //         $each: result,
  //       },
  //     },
  //   });
  // },

  "mandates_for_rent.update.images.title"(id, i, value) {
    proOrlocalAuthorization();
    // let result = [];
    // value = value.forEach(e => {
    //     result.push({ title: "", url: e })
    // });

    let update = { $set: {} };
    update["$set"]["images." + i + ".title"] = value;

    return Mandates_For_Rent.updateAsync(id, update);
  },

  "mandates_for_rent.pull.images"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(
      id,
      { $pull: { images: { url: value } } },
      false,
      false
    );
  },

  "mandates_for_rent.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "mandates_for_rent.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  // "mandates_for_rent.update.saleprice"(id, value) {
  //   if (!this.userId) {
  //     throw new Meteor.Error("not-authorized");
  //   }
  //   return mandates_for_rent.updateAsync(id, {
  //     $set: {
  //       saleprice: value,
  //     },
  //   });
  // },

  "mandates_for_rent.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "mandates_for_rent.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "mandates_for_rent.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "mandates_for_rent.push.category"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "mandates_for_rent.update.description"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "mandates_for_rent.update.slug"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        slug: value,
      },
    });
  },

  "mandates_for_rent.update.username"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  "mandates_for_rent.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "mandates_for_rent.push.amendments"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $push: {
        amendments: value,
      },
    });
  },

  //Boolean
  "mandates_for_rent.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  /**
   * ADMIN
   */

  "admin.mandates_for_rent.remove"(id) {
    adminOrlocalAuthorization();
    return Mandates_For_Rent.removeAsync(id);
  },

  "admin.mandates_for_rent.unset.mandate"(id) {
    adminOrlocalAuthorization();
    return Mandates_For_Rent.updateAsync(id, {
      $unset: {
        mandate: "",
      },
    });
  },

  async "admin.mandates_for_rent.update.mandatenumber"({ mandateId, value }) {
    adminOrlocalAuthorization();
    const mandate = Mandates_For_Rent.findOneAsync(mandateId);
    if (!mandate) throw new Meteor.Error("Error", "Mandate does not exist");

    const mandateNumber = value;

    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });
    const saleMandateNumerCheck = await Mandates_For_Sale.findOneAsync({
      "mandate.number": mandateNumber,
    });

    if (saleMandateNumerCheck || rentMandateNumerCheck)
      throw new Meteor.Error("Error", "Mandate Number already used");

    const res = await Mandates_For_Rent.updateAsync(
      { _id: mandateId },
      {
        $set: {
          "mandate.number": value,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );

    return res;
  },

  // reset
  // "admin.mandates_for_rent.reset"() {
  //   adminOrlocalAuthorization();
  //   return Mandates_For_Rent.removeAsync({}, { multi: true });
  // },

  /**
   * DEPRECATED
   */

  async "mandates_for_rent.add.mandatenumber_and_replace"(id) {
    proOrlocalAuthorization();

    const lastMandate = Mandates_For_Rent.find(
      // { status: { $ne: "draft" } },
      { sort: { mandate: -1 }, limit: 1, fields: { mandate: 1 } }
    ).fetch();

    let lastnumber = lastMandate?.[0]?._id || 0;
    lastnumber = Number(lastnumber) + Number(1);

    const doc = await Mandates_For_Rent.findOneAsync({ _id: id });
    if (!doc) return;

    if (doc.mandate) {
      throw new Meteor.Error("Error", "This mandate has already a number");
    }

    // set a new _id on the document
    doc._id = String(lastnumber);
    doc.status = "";
    doc.mandate = lastnumber;

    // insert the document, using the new _id
    const result = Mandates_For_Rent.insertAsync(doc).then(
      // remove the document with the old _id
      await Mandates_For_Rent.removeAsync({ _id: id })
    );

    return result;
  },
});
