import { Mongo } from "meteor/mongo";
// import { isAdminServer } from "../admin.ts";
import { Leases } from "./leases.js";
import { Assets_rent } from "./assets_rent.js";

export const RentTransactions = new Mongo.Collection("renttransactions");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("renttransactions", function () {
    return RentTransactions.find();
  });

  Meteor.publish("renttransactions_lease", function (leaseID) {
    const transactions = RentTransactions.find({ lease: leaseID });
    return transactions;
  });

  Meteor.publish("renttransaction_lease", function (transactionId, leaseId) {
    const transaction = RentTransactions.find({ _id: transactionId });
    const lease = Leases.find({ _id: leaseId });
    return [transaction, lease];
  });

  Meteor.publish(
    "renttransaction_lease_asset",
    function (transactionId, leaseId, assetId) {
      const transactions = RentTransactions.find({ _id: transactionId });
      const leases = Leases.find({ _id: leaseId });
      const assets = Assets_rent.find({ _id: assetId });
      return [transactions, leases, assets];
    }
  );

  Meteor.publish("renttransactions_owner", function () {
    const renttransactions = RentTransactions.find();
    const users = Meteor.users.find();
    return [renttransactions, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("renttransactions_list", function () {
    return RentTransactions.find();
  });

  Meteor.publish("visible_renttransactions", function () {
    return RentTransactions.find({ isVisible: true });
  });
  Meteor.publish("renttransaction", function (id) {
    const result = RentTransactions.find({ _id: id });
    return result;
  });
}

Meteor.methods({
  "renttransaction.insert"(leaseId, amount, month, year, type) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    const date = new Date();

    return RentTransactions.insertAsync({
      lease: leaseId,
      createdAt: date,
      userId: Meteor.userId(),
      amount: amount,
      month: month,
      year: year,
      type: type,
    });
  },

  "renttransaction.update.title"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "renttransaction.update.type"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "renttransaction.update.saleprice"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "renttransactions.push.tag"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "renttransactions.push.category"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "renttransaction.update.description"(id, value) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "renttransactions.remove"(id) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return RentTransactions.removeAsync({ _id: id });
  },
});
