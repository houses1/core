import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";
export const User_Transactions = new Mongo.Collection("user_transactions");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("usertransactions", function () {
    return User_Transactions.find();
  });

  Meteor.publish("usertransactions_user", function (id) {
    const usertransactions = User_Transactions.find({ userId: id });
    return usertransactions;
  });

  Meteor.publish("usertransactions_user_limit", function (id) {
    const usertransactions = User_Transactions.find(
      { userId: id },
      { limit: 20 }
    );
    return usertransactions;
  });

  //Update with a list, an array of IDs
  Meteor.publish("usertransactions_list", function () {
    return User_Transactions.find();
  });

  Meteor.publish("visible_usertransactions", function () {
    return User_Transactions.find({ isVisible: true });
  });
  Meteor.publish("usertransaction", function (id) {
    const result = User_Transactions.find({ _id: id });
    return result;
  });
}

Meteor.methods({
  async "usertransactions.insert"(userId, amount, notes) {
    if (amount > 0) {
      adminOrlocalAuthorization();

      await Meteor.callAsync("user.admin.increment.wallet", {
        id: userId,
        value: amount,
        notes: notes,
      });
    } else {
      proOrlocalAuthorization();

      await Meteor.callAsync("user.substract.wallet", {
        id: userId,
        value: amount,
        notes: notes,
      });
    }

    const date = new Date();

    return User_Transactions.insertAsync({
      createdAt: date,
      userId: userId,
      addedBy: Meteor.userId(),
      amount: amount,
      notes: notes,
    });
  },

  "usertransactions.update.title"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "usertransactions.update.type"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "usertransactions.update.saleprice"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $set: {
        saleprice: value,
      },
    });
  },

  "usertransactions.push.tag"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "usertransactions.push.category"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "usertransactions.update.description"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "usertransactions.remove"(id) {
    adminOrlocalAuthorization();
    console.log(id);
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.removeAsync({ _id: id });
  },

  //Boolean
  "usertransactions.update.isVisible"(id, value) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }
    return User_Transactions.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
