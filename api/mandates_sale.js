import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";
import enterprise_customization from "../../enterprise_customization.ts";
import { Mandates_For_Rent } from "./mandates_rent.js";
import { Assets_sale } from "./assets_sale.js";
import { Contacts } from "./contacts.js";

export const Mandates_For_Sale = new Mongo.Collection("mandates_for_sale");
// This code only runs on the server
if (Meteor.isServer) {
  Meteor.publish("mandatesForSale_from_asset", function (assetId) {
    return Mandates_For_Sale.find({
      "asset.value": assetId,
    });
  });

  Meteor.publish("mandates_for_sale_all", function () {
    return Mandates_For_Sale.find();
  });

  Meteor.publish("mandates_for_sale_seller", function (id) {
    return Mandates_For_Sale.find({ seller: id });
  });

  Meteor.publish("mandates_for_sale_record", function () {
    return Mandates_For_Sale.find({ mandate: { $exists: true } });
  });

  Meteor.publish("mandates_for_sale_statistics", function (year) {
    let result;
    if (year) {
      result = Mandates_For_Sale.find({
        mandate: { $exists: true },
        createdAt: {
          $gte: new Date(year, 1, 1, 0, 0, 0),
          $lt: new Date(Number(year) + 1, 1, 1, 0, 0, 0),
        },
      });
    } else {
      result = Mandates_For_Sale.find({ mandate: { $exists: true } });
    }

    return result;
  });

  Meteor.publish("my_mandates_for_sale", function () {
    return Mandates_For_Sale.find({ userId: Meteor.userId() });
  });

  Meteor.publish("mandates_for_sale_confirmed", function () {
    return Mandates_For_Sale.find({ mandate: { $exists: true } });
  });

  Meteor.publish("mandates_for_sale_owner", function () {
    const mandates = Mandates_For_Sale.find();
    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });

  Meteor.publish("my_mandates_for_sale_owner", function () {
    // const mandates = Mandates_For_Sale.find({ userId: Meteor.userId() });
    const mandates = Mandates_For_Sale.find({
      $or: [
        { userId: Meteor.userId() },
        { sharedWith: { $in: [Meteor.userId()] } },
      ],
    });

    // To have the names..... to be optimized
    const users = Meteor.users.find();
    return [mandates, users];
  });

  //Update with a list, an array of IDs
  Meteor.publish("mandates_for_sale_list", function () {
    return Mandates_For_Sale.find();
  });

  // Meteor.publish('visible_mandates_for_sale', function publication() {
  //     return Mandates_For_Sale.find({ isVisible: true },);
  // });
  // Meteor.publish('mandate_for_sale', function publication(id) {
  //     const result = Mandates_For_Sale.find({ _id: id });
  //     return result
  // });

  Meteor.publish("mandate_for_sale_id", function (id) {
    return Mandates_For_Sale.find({ _id: id }, { limit: 1 });
  });

  Meteor.publish("mandate_for_sale_number_id", function (id) {
    return Mandates_For_Sale.find(
      { _id: id },
      { limit: 1, fields: { mandate: 1 } }
    );
  });

  Meteor.publish(
    "mandate_for_sale_id_protected",
    async function publication(id) {
      const user = await Meteor.userAsync();

      if (user?.roles?.includes("admin")) {
        return Mandates_For_Sale.find({ _id: id }, { limit: 1 });
      } else {
        return Mandates_For_Sale.find(
          {
            _id: id,
            $or: [
              { userId: Meteor.userId() },
              { sharedWith: { $in: [Meteor.userId()] } },
            ],
          },
          { limit: 1 }
        );
      }
    }
  );

  Meteor.publish("mandate_for_sale_id_complete", async function (id) {
    let mandateSingle = await Mandates_For_Sale.findOneAsync(
      { _id: id },
      { limit: 1 }
    );

    let contactsArray = [];
    // let asset = [];

    // if (!mandate?.[0]) return;
    if (mandateSingle.seller) {
      contactsArray.push(mandateSingle.seller);
    }

    if (mandateSingle.buyer) {
      contactsArray.push(mandateSingle.buyer);
    }

    if (mandateSingle.buyerNotary) {
      contactsArray.push(mandateSingle.buyerNotary);
    }

    let asset = Assets_sale.find(
      { _id: mandateSingle?.asset?.value },
      { limit: 1 }
    );
    const mandate = Mandates_For_Sale.find({ _id: id }, { limit: 1 });
    const contacts = Contacts.find({ _id: { $in: contactsArray } });

    return [mandate, contacts, asset];
    // return [mandate, contacts];
  });

  Meteor.publish("mandate_for_sale_arrayid", function (array) {
    let result = Mandates_For_Sale.find({ _id: { $in: array } });
    // if (result.count() == 0) {
    //     result = Mandates_For_Sale.find({ _id: id }, { limit: 1 });
    // }
    return result;
  });

  Meteor.publish("mandate_for_sale_number", function (id) {
    let result = Mandates_For_Sale.find({ mandate: id }, { limit: 1 });
    // if (result.count() == 0) {
    //     result = Mandates_For_Sale.find({ _id: id }, { limit: 1 });
    // }
    return result;
  });
}

Meteor.methods({
  async "mandates_for_sale.insert"(title) {
    proOrlocalAuthorization();
    const date = new Date();

    const user = await Meteor.userAsync();
    // agent contact
    const contactAgent = user?.contact;

    //Email
    if (contactAgent && !contactAgent.email) {
      contactAgent.email = user?.emails?.[0]?.address;
    }

    return Mandates_For_Sale.insertAsync({
      title: title,
      createdAt: date,
      userId: Meteor.userId(),
      status: "draft",
      contact: {
        agent: contactAgent,
      },
    });
  },

  // Duplicate
  async "mandates_for_sale.duplicate"(id) {
    proOrlocalAuthorization();
    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (!mandate) return;

    const date = new Date();
    mandate.createdAt = date;

    delete mandate["_id"];
    delete mandate["mandate"];
    delete mandate["status"];
    delete mandate["signatureDate"];
    delete mandate["startingDate"];
    delete mandate["deed"];

    return Mandates_For_Sale.insertAsync(mandate);
  },

  async "mandates_for_sale.insertwithasset"(assetId) {
    proOrlocalAuthorization();
    const date = new Date();
    // agent contact
    const user = await Meteor.userAsync();
    const contactAgent = user?.contact;
    const asset = await Assets_sale.findOneAsync(assetId);

    // Careful because mandate and assets dont use the same .... saleprice object....
    let saleprice;
    if (asset?.saleprice) {
      saleprice = asset?.saleprice;
    }

    const resultId = await Mandates_For_Sale.insertAsync({
      // _id: mandate,
      // title: title,
      createdAt: date,
      userId: Meteor.userId(),
      asset: { value: assetId },
      contact: {
        agent: contactAgent,
      },
      saleprice: saleprice,
    });

    await Assets_sale.updateAsync(assetId, {
      $push: {
        mandates: String(resultId),
      },
    });

    return resultId;
  },

  "mandates_for_sale.update.saleprice"(
    id,
    price_feesincluded,
    price_feesnotincluded
  ) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        "saleprice.fees_included": Number(price_feesincluded),
        "saleprice.fees_not_included": Number(price_feesnotincluded),
      },
    });
  },

  async "mandates_for_sale.update.isSold"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isSold", asset, value);
    }

    return Mandates_For_Sale.updateAsync(id, {
      $set: { isSold: value },
    });
  },

  async "mandates_for_sale.update.isSoldByOthers"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isSoldByOthers", asset, value);
    }

    return Mandates_For_Sale.updateAsync(id, {
      $set: { isSoldByOthers: value },
    });
  },

  async "mandates_for_sale.update.isCancelledBySeller"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync(
        "asset_sale.update.isCancelledBySeller",
        asset,
        value
      );
    }

    return Mandates_For_Sale.updateAsync(id, {
      $set: { isCancelledBySeller: value },
    });
  },

  async "mandates_for_sale.update.isExpired"(id, value = true) {
    // value Boolean
    proOrlocalAuthorization();
    if (value == "false") value = false;
    if (value == "true") value = true;

    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (mandate?.asset?.value) {
      const asset = mandate?.asset?.value;
      await Meteor.callAsync("asset_sale.update.isVisible", asset, !value);
    }

    return Mandates_For_Sale.updateAsync(id, {
      $set: { isExpired: value },
    });
  },

  "mandates_for_sale.update.financing"(id, result) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        financing: result,
      },
    });
  },

  // For give
  "mandates_for_sale.update.userId"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value)) {
      throw new Meteor.Error("Error", "User not found");
    }

    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        userId: value,
      },
    });
  },

  // For share
  "mandates_for_sale.update.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        sharedWith: [value],
      },
    });
  },

  "mandates_for_sale.push.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        sharedWith: value,
      },
    });
  },

  "mandates_for_sale.pull.sharedWith"({ id, value }) {
    proOrlocalAuthorization();
    if (!Meteor.users.findOneAsync(value))
      throw new Meteor.Error("Error", "User not found");
    return Mandates_For_Sale.updateAsync(id, {
      $pull: {
        sharedWith: value,
      },
    });
  },

  "mandates_for_sale.update.furnitureValue"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        furnitureValue: value,
      },
    });
  },

  "mandates_for_sale.update.feesPaidBy"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        "saleprice.feesPaidBy": value,
      },
    });
  },

  "mandates_for_sale.update.startingDate"({ mandateId, date }) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(mandateId, {
      $set: {
        startingDate: date,
      },
    });
  },

  "mandates_for_sale.unset.startingDate"({ mandateId }) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(mandateId, {
      $unset: {
        startingDate: "",
      },
    });
  },

  "mandates_for_sale.unset.validityDuration"({ mandateId }) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(mandateId, {
      $unset: {
        validityDuration: "",
      },
    });
  },

  "mandates_for_sale.update.signatureDate"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        signatureDate: value,
      },
    });
  },

  "mandates_for_sale.update.type"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "mandates_for_sale.unset.type"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $unset: {
        type: "",
      },
    });
  },

  "mandates_for_sale.update.seller"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        seller: value,
      },
    });
  },

  "mandates_for_sale.update.buyer"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        buyer: value,
      },
    });
  },

  "mandates_for_sale.update.buyerNotary"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        buyerNotary: value,
      },
    });
  },

  // Notaire du mandant / Notary of the seller
  "mandates_for_sale.update.notary"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        notary: value,
      },
    });
  },

  "mandates_for_sale.update.validityDuration"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        validityDuration: value,
      },
    });
  },

  async "mandates_for_sale.update.asset"(id, asset) {
    proOrlocalAuthorization();
    // Push a reference on the asset
    await Assets_sale.updateAsync(asset.value, {
      $push: {
        mandates: String(id),
      },
    });
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        asset: asset,
      },
    });
  },

  "mandates_for_sale.unset.asset"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $unset: {
        asset: "",
      },
    });
  },

  "mandates_for_sale.update.rentalSituation"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        rentalSituation: value,
      },
    });
  },

  async "mandates_for_sale.update.status"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        status: value,
      },
    });
  },

  "mandates_for_sale.update.deed"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        deed: value,
      },
    });
  },

  "mandates_for_sale.update.deedDate"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        deedDate: value,
      },
    });
  },

  "mandates_for_sale.update.saleAgreementDate"({ id, saleAgreementDate }) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        saleAgreementDate: saleAgreementDate,
      },
    });
  },

  "mandates_for_sale.update.title"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  async "mandates_for_sale.add.mandatenumber"(id) {
    proOrlocalAuthorization();

    const mandateNumber =
      await enterprise_customization.generateMandateNumber();
    if (!mandateNumber) return;

    const mandate = await Mandates_For_Sale.findOneAsync(id);
    if (mandate?.mandate)
      throw new Meteor.Error("Error", "Mandate  already has a number");

    // Check
    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });
    const saleMandateNumerCheck = await Mandates_For_Sale.findOneAsync({
      "mandate.number": mandateNumber,
    });

    if (rentMandateNumerCheck || saleMandateNumerCheck)
      throw new Meteor.Error(
        "Error",
        `Mandate Number ${mandateNumber} already used`
      );

    await Mandates_For_Sale.updateAsync(
      { _id: id },
      {
        $set: {
          "mandate.number": mandateNumber,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );

    // Update asset ref
    // const asset = Assets_sale.findOneAsync(mandate.asset);

    if (mandate?.asset?.value) {
      await Assets_sale.updateAsync(mandate.asset.value, {
        $set: {
          mandateRef: mandateNumber,
        },
      });
    }

    return mandateNumber;
  },

  async "mandates_for_sale.remove"(id) {
    proOrlocalAuthorization();
    //A mandate with mandate number cannot be deleted
    const thismandate = await Mandates_For_Sale.findOneAsync(id);

    if (!thismandate) {
      throw new Meteor.Error("Error", "Mandate not found");
    }

    if (thismandate.mandate) {
      throw new Meteor.Error("Error", "Mandate has number");
    }

    return Mandates_For_Sale.removeAsync(id);
  },

  "mandates_for_sale.update.isArchived"(id) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        isArchived: true,
      },
    });
  },

  "mandates_for_sale.push.links"(id, title, url) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        links: {
          title: title,
          url: url,
        },
      },
    });
  },

  "mandates_for_sale.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        address: {
          country,
          city,
          postalCode,
          streetAddress,
        },
      },
    });
  },

  "mandates_for_sale.update.contact"(id, name, phone, picture, email) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name,
            phone,
            picture,
            email,
          },
        },
      },
    });
  },

  "mandates_for_sale.update.livingArea"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        livingArea: value,
      },
    });
  },

  "mandates_for_sale.update.diagnostics"(id, dpe, ges) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        diagnostics: { dpe: dpe, ges: ges },
      },
    });
  },

  "mandates_for_sale.push.tag"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "mandates_for_sale.push.offer"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        offers: value,
      },
    });
  },

  "mandates_for_sale.push.category"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "mandates_for_sale.update.description"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  "mandates_for_sale.update.username"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        username: value,
      },
    });
  },

  "mandates_for_sale.update.externalUrl"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        externalUrl: value,
      },
    });
  },

  "mandates_for_sale.push.amendments"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $push: {
        amendments: value,
      },
    });
  },

  "mandates_for_sale.update.amendments.saleprice"(id, index, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        [`amendments.${index}.price_feesincluded`]: value,
      },
    });
  },

  "mandates_for_sale.update.amendments.fees"(id, index, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        [`amendments.${index}.fees`]: value,
      },
    });
  },

  //Boolean
  "mandates_for_sale.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },

  /**
   * ADMIN
   */

  async "admin.mandates_for_sale.update.mandatenumber"({ mandateId, value }) {
    adminOrlocalAuthorization();
    const mandate = Mandates_For_Sale.findOneAsync(mandateId);
    if (!mandate) throw new Meteor.Error("Error", "Mandate does not exist");

    const mandateNumber = value;

    const rentMandateNumerCheck = await Mandates_For_Rent.findOneAsync({
      "mandate.number": mandateNumber,
    });
    const saleMandateNumerCheck = await Mandates_For_Sale.findOneAsync({
      "mandate.number": mandateNumber,
    });

    if (saleMandateNumerCheck || rentMandateNumerCheck)
      throw new Meteor.Error("Error", "Mandate Number already used");

    const res = await Mandates_For_Sale.updateAsync(
      { _id: mandateId },
      {
        $set: {
          "mandate.number": value,
          "mandate.date": new Date(),
          "mandate.userId": Meteor.userId(),
        },
      }
    );

    return res;
  },

  //unset
  "admin.mandates_for_sale.unset.mandate"(id) {
    if (!Meteor.isServer) return;
    adminOrlocalAuthorization();
    return Mandates_For_Sale.updateAsync(id, {
      $unset: {
        mandates: "",
      },
    });
  },

  /**
   * DEPRECATED
   */

  //Reset
  // "mandates_for_sale.reset"() {
  //   adminOrlocalAuthorization();
  //   return Mandates_For_Sale.removeAsync({}, { multi: true });
  // },

  //Remove
  // "mandates_for_sale.remove"(id) {
  //   adminOrlocalAuthorization();
  //   return Mandates_For_Sale.removeAsync({ _id: id });
  // },

  async "mandates_for_sale.add.mandatenumber_replace"(id) {
    proOrlocalAuthorization();
    const lastMandate = Mandates_For_Sale.find(
      { status: { $ne: "draft" } },
      { sort: { mandate: -1 }, limit: 1, fields: { mandate: 1 } }
    ).fetch();

    let lastnumber = lastMandate?.[0]?._id || 0;
    lastnumber = Number(lastnumber) + Number(1);
    const thismandate = await Mandates_For_Sale.findOneAsync(id);

    // store the document in a variable
    const doc = Mandates_For_Sale.findOneAsync({ _id: id });
    if (!doc) return;
    if (doc.mandate) {
      throw new Meteor.Error("Error", "This mandate has already a number");
    }

    // set a new _id on the document
    doc._id = String(lastnumber);
    doc.status = "";
    doc.mandate = lastnumber;

    // insert the document, using the new _id
    const res = await Mandates_For_Sale.insertAsync(doc);
    await Mandates_For_Sale.removeAsync({ _id: id });
  },

  // "mandates.change"(id, num) {
  //   //Was used to change the number of mandate, in the context of FR
  //   adminOrlocalAuthorization();

  //   let number = Number(num);
  //   const thismandate = await Mandates_For_Sale.findOneAsync(id);
  //   // store the document in a variable
  //   const doc = await Mandates_For_Sale.findOneAsync({ _id: id });

  //   if (!doc) return;
  //   if (doc.mandate)
  //     throw new Meteor.Error("Error", "This mandate has already a number");

  //   // set a new _id on the document
  //   doc._id = String(number);
  //   doc.status = "";
  //   doc.mandate = number;

  //   // insert the document, using the new _id
  //   return Mandates_For_Sale.insertAsync(doc, function (err, res) {
  //     if (err) {
  //       console.log(err);
  //     } else {
  //       // remove the document with the old _id
  //       Mandates_For_Sale.removeAsync({ _id: id });
  //     }
  //   });
  // },
});
