import { Mandates_For_Sale } from "./mandates_sale";
import { Assets_sale } from "./assets_sale";

if (Meteor.isServer) {
  Meteor.publish("mandate__asset__for_sale_id", async function (mandateId) {
    // Need to wait for Mandates_For_Sale to get the assetId BUT i do not know how to do

    // const mandates = Mandates_For_Sale.find({ _id: mandateId }, { limit: 1 });
    const mandates = Mandates_For_Sale.find({ _id: mandateId });
    const mandate = mandates.fetch()[0];

    // if (!mandate) return;
    // const assetId = mandate.asset.value;

    // const assets = Assets_sale.find({ _id: assetId }, { limit: 1 });
    const assets = Assets_sale.find();

    return [mandates, assets];
  });
}
