import { Accounts } from "meteor/accounts-base";
import enterprise_customization from "../../enterprise_customization";
const {
  sendingEmail,
  enterprise_name,
  adminEmails,
  adminEmailsLocal,
  emailSystem,
} = enterprise_customization;
//POUR CHANGER L URL DANS LE MAIL DE LOST PASSWORD

Accounts.urls.resetPassword = function (token) {
  return Meteor.absoluteUrl("reset-password/" + token);
};

Accounts.emailTemplates.from = sendingEmail;

// For sendVerificationLink // Accounts.sendVerificationEmail
Accounts.emailTemplates.verifyEmail = {
  subject() {
    return `${enterprise_name} - Vérifier votre email`;
  },
  text(user, url) {
    return `Vous pouvez vérifier votre email avec ce lien : ${url}`;
  },
};

Accounts.urls.verifyEmail = function (token) {
  return Meteor.absoluteUrl("verify-email/" + token);
};

Meteor.methods({
  async sendToAgent({ from = sendingEmail, to, subject, html, replyTo }) {
    const local = Meteor.absoluteUrl() == "http://localhost:3000/";

    let emailsArray = [...adminEmails];
    if (local) {
      emailsArray = [...adminEmailsLocal];
    }

    if (emailSystem == "brevo") {
      if (to && !emailsArray.includes({ email: to })) {
        emailsArray.push({ email: to });
      }
    } else {
      // Sendgrid
      // array => [{email: xxxxxx}]
      emailsArray = emailsArray.map((x) => {
        return x.email;
      });

      if (to && !emailsArray.includes(to)) {
        emailsArray.push(to);
      }
    }

    // Production
    const result = await Meteor.callAsync("sendEmail", {
      bcc: emailsArray,
      fromAddress: from,
      subject,
      html,
      replyTo,
    });

    return result;
  },

  // Standard meteor email
  async sendToTeam({ from = sendingEmail, subject, html, replyTo }) {
    const isLocal = Meteor.absoluteUrl() == "http://localhost:3000/";

    let bcc;
    if (isLocal) {
      bcc = [...adminEmailsLocal];
    } else {
      bcc = [...adminEmails]; // Production
    }

    return await Meteor.callAsync("sendEmail", {
      bcc,
      fromAddress: from,
      subject,
      html,
      replyTo,
    });
  },
});
