import { Mongo } from "meteor/mongo";
import {
  adminOrlocalAuthorization,
  proOrlocalAuthorization,
} from "../admin.ts";

export const BlackList = new Mongo.Collection("blackList");

if (Meteor.isServer) {
  // This code only runs on the server
  Meteor.publish("blackList", function publication() {
    return BlackList.find();
  });

  Meteor.publish("my_blackList", function publication() {
    return BlackList.find({ userId: Meteor.userId() });
  });

  Meteor.publish("blackList_owner", function publication() {
    const assets = BlackList.find();
    const users = Meteor.users.find();
    return [assets, users];
  });

  Meteor.publish("visible_blackList", function publication() {
    return BlackList.find({ isVisible: true });
  });
}

Meteor.methods({
  "blackList.insert"({
    firstname,
    surname,
    birthdate,
    incidentDate,
    city,
    description,
  }) {
    proOrlocalAuthorization();
    const date = new Date();

    return BlackList.insertAsync({
      name: {
        firstname: firstname,
        surname: surname,
      },
      birthdate: birthdate,
      incidentDate: incidentDate,
      city: city,
      description: description,
      createdAt: date,
      userId: Meteor.userId(),
    });
  },

  "blackList.update.title"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        title: value,
      },
    });
  },

  "blackList.update.type"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        type: value,
      },
    });
  },

  "blackList.update.address"(
    id,
    postalCode,
    streetAddress,
    city,
    country = ""
  ) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        address: {
          country: country,
          city: city,
          postalCode: postalCode,
          streetAddress: streetAddress,
        },
      },
    });
  },

  "blackList.update.contact"({ id, name, phone, picture, email }) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        contact: {
          agent: {
            name: name,
            phone: phone,
            picture: picture,
            email: email,
          },
        },
      },
    });
  },

  "blackList.push.tag"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $push: {
        tags: value,
      },
    });
  },

  "blackList.push.category"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $push: {
        categories: value,
      },
    });
  },

  "blackList.update.description"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        description: value,
      },
    });
  },

  async "blackList.remove"(id) {
    proOrlocalAuthorization();
    const item = await BlackList.findOneAsync(id);

    if (item?.userId && item.userId == Meteor.userId()) {
      return BlackList.removeAsync(id);
    }
  },

  //Boolean
  "blackList.update.isVisible"(id, value) {
    proOrlocalAuthorization();
    return BlackList.updateAsync(id, {
      $set: {
        isVisible: value,
      },
    });
  },
});
