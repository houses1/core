import { Mongo } from "meteor/mongo";

export const Singles = new Mongo.Collection("singles");

// This code only runs on the server
if (Meteor.isServer) {
  Meteor.publish("singles", function Publication() {
    return Singles.find();
  });

  Meteor.publish("single", function Publication(id) {
    return Singles.find({ _id: id });
  });
}

Meteor.methods({
  "single.remove.all"() {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    return Singles.removeAsync({}, { multi: true });
  },

  "single.update.socialmedia_id"({ facebookId, instagramId }) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }

    let resultObject = {
      createdAt: new Date(),
      ...(instagramId && { instagramId }), // If value is truthy, add token property
      ...(facebookId && { facebookId }), // If value is truthy, add token property
    };

    if (Meteor.userId()) {
      resultObject.userId = Meteor.userId();
    }

    return Singles.updateAsync({ _id: "socialmedia_id" }, resultObject, {
      upsert: true,
    });
  },

  "single.update.facebook_token"({ value, expiredAt }) {
    if (!this.userId) {
      throw new Meteor.Error("not-authorized");
    }

    let resultObject = {
      createdAt: new Date(),
      value,
      ...(expiredAt && { expiredAt: expiredAt }), // If value is truthy, add token property
    };

    if (Meteor.userId()) {
      resultObject.userId = Meteor.userId();
    }

    return Singles.updateAsync({ _id: "facebook_token" }, resultObject, {
      upsert: true,
    });
  },

  "single.add.file"(object) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    const resultObject = { $push: { object } };
    // We should not add if title are the same ....
    return Singles.updateAsync({ _id: "files" }, resultObject, {
      upsert: true,
    });
  },

  "single.remove.file"(object) {
    if (!this.userId) throw new Meteor.Error("not-authorized");
    const resultObject = { $pull: { object } };
    // We should not add if title are the same ....
    return Singles.updateAsync({ _id: "files" }, resultObject);
  },
});
