import { fetch } from "meteor/fetch";
import { Assets_sale } from "./assets_sale.js";
import { Assets_rent } from "./assets_rent.js";

const postData = { username: "IMMOPO", password: "JAKOjklazs1" };

const sendPostRequest = async (url) => {
  return await fetch(url, {
    method: "POST",
    body: JSON.stringify(postData),
    headers: postData ? { "Content-Type": "application/json" } : {},
  }).then((response) => {
    return response.json();
  });
};

const externalPartners = [
  {
    name: "anoa",
    APIpath_sale: "https://www.anoahome.com/api/immopo.1.0/sale",
    APIpath_rent: "https://www.anoahome.com/api/immopo.1.0/rent",
  },
  {
    name: "lexinesia",
    APIpath_sale:
      "https://lexinesia-ae148dcb5e98.herokuapp.com/api/immopo.1.0/sale",
    APIpath_rent:
      "https://lexinesia-ae148dcb5e98.herokuapp.com/api/immopo.1.0/rent",
  },
];

const cleanPartnerData = async (partnerName) => {
  await Promise.all([
    await Assets_sale.removeAsync(
      { "origin.name": partnerName },
      { multi: true }
    ),
    await Assets_rent.removeAsync(
      { "origin.name": partnerName },
      { multi: true }
    ),
  ]);
};

Meteor.methods({
  async "external.get.all"() {
    await Promise.all(
      externalPartners.map((partner) => cleanPartnerData(partner.name))
    );

    const promises = externalPartners.flatMap(async (x) => [
      await Meteor.callAsync("external.get.sale", x.name),
      await Meteor.callAsync("external.get.rent", x.name),
    ]);
    await Promise.all(promises);
    console.log("Sync done");
  },

  async "external.get.sale"(partnerName) {
    const partner = externalPartners.find((o) => o.name === partnerName);
    if (!partner?.APIpath_sale) return;

    const responseData = await sendPostRequest(partner?.APIpath_sale);

    await Promise.all(
      responseData.map(
        async (asset) =>
          await Meteor.callAsync("asset_sale.upsert.externalPartners", {
            object: asset,
            name: partnerName,
          })
      )
    );
  },

  async "external.get.rent"(partnerName) {
    const partner = externalPartners.find((o) => o.name === partnerName);
    if (!partner?.APIpath_rent) return;

    const responseData = await sendPostRequest(partner?.APIpath_rent);

    await Promise.all(
      responseData.map((asset) =>
        Meteor.callAsync("asset_rent.upsert.externalPartners", {
          object: asset,
          name: partnerName,
        })
      )
    );
  },
});
