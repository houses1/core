// Last update 16/07/2022

// https://en.wikipedia.org/wiki/List_of_cities_in_French_Polynesia
// https://www.code-postal.com/polynesie-francaise-d987.html
export const values = [
  // Tahiti
  { value: "Faa'a", label: "Faa'a", group: "Tahiti", code: "98702" },
  { value: "Papeete", label: "Papeete", group: "Tahiti", code: "98714" },
  { value: "Punaauia", label: "Punaauia", group: "Tahiti", code: "98718" },
  { value: "Pirae", label: "Pirae", group: "Tahiti", code: "98716" },
  { value: "Mahina", label: "Mahina", group: "Tahiti", code: "98709" },
  { value: "Paea", label: "Paea", group: "Tahiti", code: "98711" },
  { value: "Papara", label: "Papara", group: "Tahiti", code: "98712" },
  { value: "Arue", label: "Arue", group: "Tahiti", code: "98701" },
  { value: "Papeari", label: "Papeari", group: "Tahiti", code: "98727" },
  { value: "Mataiea", label: "Mataiea", group: "Tahiti", code: "98726" },
  // { value: "Atimaono", label: "Atimaono", group: "Tahiti", code: "98726" },

  // Hitiaa O Te Ra //    code: "98706",
  { value: "Papenoo", label: "Papenoo", group: "Tahiti", code: "98707" },
  { value: "Hitia’a", label: "Hitia’a", group: "Tahiti", code: "98705" },
  { value: "Mahaena", label: "Mahaena", group: "Tahiti", code: "98706" },
  { value: "Tiarei", label: "Tiarei", group: "Tahiti", code: "98708" },

  // Taiarapu-Ouest
  { value: "Taravao", label: "Taravao", group: "Tahiti", code: "98719" },
  { value: "Teahupo'o", label: "Teahupo'o", group: "Tahiti", code: "98719" },

  // Taiarapu-Est
  { value: "Faaone", label: "Faaone", group: "Tahiti", code: "98720" },
  { value: "Afaahiti", label: "Afaahiti", group: "Tahiti", code: "98719" },
  { value: "Tautira", label: "Tautira", group: "Tahiti", code: "98722" },

  // Moorea
  { value: "Moorea", label: "Moorea", group: "Moorea", code: "98728" },

  //Société
  { value: "Raiatea", label: "Raiatea", group: "Société" },
  { value: "Tumaraa", label: "Tumaraa", group: "Société", code: "98735" },
  { value: "Huahine", label: "Huahine", group: "Société", code: "98731" },
  { value: "Taha'a", label: "Taha'a", group: "Société", code: "98734" },
  { value: "Bora Bora", label: "Bora Bora", group: "Société" },
  { value: "Maupiti", label: "Maupiti", group: "Société", code: "98732" },

  // Marquises
  { value: "Nuku Hiva", label: "Nuku Hiva", group: "Marquises", code: "98742" },
  { value: "Hiva Oa", label: "Hiva Oa", group: "Marquises" },
  { value: "Ua Huka", label: "Ua Huka", group: "Marquises" },
  { value: "Fatu Hiva", label: "Fatu Hiva", group: "Marquises" },
  { value: "Tahuata", label: "Tahuata", group: "Marquises", code: "98743" },
  { value: "Ua Pou", label: "Ua Pou", group: "Marquises", code: "98745" },

  //Tuamotu
  { value: "Rangiroa", label: "Rangiroa", group: "Tuamotu", code: "98776" },
  { value: "Tikehau", label: "Tikehau", group: "Tuamotu", code: "98778" },
  { value: "Fakarava", label: "Fakarava", group: "Tuamotu", code: "98763" },
  { value: "Aratika", label: "Aratika", group: "Tuamotu" },
  { value: "Napuka", label: "Napuka", group: "Tuamotu", code: "98772" },
  { value: "Pukapuka", label: "Pukapuka", group: "Tuamotu", code: "98774" },
  { value: "Tureia", label: "Tureia", group: "Tuamotu", code: "98784" },
  { value: "Makemo", label: "Makemo", group: "Tuamotu", code: "98790" },

  //Australes
  { value: "Tubuai", label: "Tubuai", group: "Australes", code: "98754" },
  { value: "Rurutu", label: "Rurutu", group: "Australes", code: "98753" },
  { value: "Rimatara", label: "Rimatara", group: "Australes", code: "98752" },
  { value: "Raivavae", label: "Raivavae", group: "Australes" },
  { value: "Rapa", label: "Rapa", group: "Australes", code: "98794" },

  //Gambier
  { value: "Mangareva", label: "Mangareva", group: "Gambier" },
];
