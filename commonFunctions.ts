import enterprise_customization from "../enterprise_customization";
const { currency, countryConfiguration, cloudinary, socialMedia } =
  enterprise_customization;

exports.slugify = (string) => {
  const a =
    "àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;";
  const b =
    "aaaaaaaaaacccddeeeeeeeegghiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------";
  const p = new RegExp(a.split("").join("|"), "g");
  const regex = /(\d)\s+(?=\d)/g;
  const regex2 = /(\d)(?= \d) /g; // Check if useful?????? typescript said it is unused

  return (
    string
      .toString()
      .toLowerCase()
      .replace("²", "2") // Added by Vaan **
      // .replace(/(?<=\d) +(?=\d)/g, "") // Added by Vaan ** / removes space between numbers BREAKS SAFARI ON MAC AND IOS
      .replace(regex, `$1`)
      .replace(regex2, "$1") // Added by Vaan ** / removes space between numbers
      .replace(/\s+/g, "-") // Replace spaces with -
      .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters
      .replace(/&/g, "-and-") // Replace & with 'and'
      .replace(".", "-") // Added by Vaan ** Otherwise non-word will remove the dots and 11.1m2 becomes 111m2
      .replace(/[^\w\-]+/g, "") // Remove all non-word characters
      .replace(/\-\-+/g, "-") // Replace multiple - with single -
      .replace(/^-+/, "") // Trim - from start of text
      .replace(/-+$/, "")
  ); // Trim - from end of text
};

const formatNumber = (num) => {
  if (num == undefined) return;
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1 ");
};
exports.formatNumber = formatNumber;

const formatAndcurrency = (num = 0) => {
  let result = formatNumber(num);
  result += " " + currency;
  return result;
};
exports.formatAndcurrency = formatAndcurrency;

const roundNumber = (number) => {
  if (isNaN(number) || number == 0) return 0;
  if (number == "" || number == undefined) return "";

  if (countryConfiguration == "fr") {
    return number.toFixed(2);
  } else {
    return number.toFixed();
  }
};

exports.roundNumber = roundNumber;

exports.roundNumberFormat = (number) => {
  let result = roundNumber(number);
  return formatAndcurrency(result);
};

exports.validateEmail = (email) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
};

//lower resolution
exports.lowres = ({ url, size = 400 }) => {
  if (!url) return;

  if (!url.includes("https://res.cloudinary.com")) return url;

  const limitation = `/upload/w_${size},h_${size},c_limit/`;
  url = url.replace("/upload/", limitation);

  // Remove watermark
  const regex = new RegExp("/[^/]*,[^/]*l_[^/]*");
  url = url.replace(regex, "");
  url = url.replaceAll(".jpg", ".webp");
  url = url.replaceAll(".png", ".webp");

  return url;
};

// Common functions for facebook pages
exports.formatDescription = ({ page, description }) => {
  if (!description) return "";

  description = description.replaceAll("\n", "%0A");
  description = description.replaceAll("+", "%2b");

  if (page.publishLink == false) {
    // REMOVE THE URL //Everything that is after http
    const regex = new RegExp(/http\S*/);
    description = description.replace(regex, "");
    description = description.replace("Plus de détails/photos :", "");
  }

  return description;
};

exports.uploadPicturesFunction = async ({ images, version, pageId, token }) => {
  let promises = [];
  let urlToUpload;
  let photoUrl;
  images.forEach((image, index) => {
    photoUrl = image.url;
    urlToUpload = `https://graph.facebook.com/${version}/${pageId}/photos?url=${photoUrl}&access_token=${token}&published=false`;

    console.log({ urlToUpload });
    promises.push(fetchDataFunction(urlToUpload));
  });

  const promiseResults = await Promise.all(promises);
  return promiseResults;
};

// Instagram //
// https://developers.facebook.com/docs/instagram-api/guides/content-publishing

exports.createContainerInstagram = async ({
  images,
  version,
  pageId,
  token,
  description,
  city,
  assetType,
}) => {
  let hashtag = "%0A"; // For the return to the line

  // Always keep a space between the #tags
  hashtag += `%23immobilier `;
  hashtag += `%23agenceimmobiliere `;

  if (countryConfiguration == "pf") {
    hashtag += `%23tahiti `;
    hashtag += `%23polynesie `;
    hashtag += `%23immobiliertahiti `;
    hashtag += `%23immobilierpolynesie `;
    hashtag += `%23agenceimmobilieretahiti `;
  }

  if (socialMedia.hashtag) {
    hashtag += ` %23${socialMedia.hashtag}`;
  }

  if (city) {
    // ENLEVER les maj et les autres symbole
    hashtag += ` %23${city.toLowerCase()}`;
  }

  // Ajouter vente / location
  if (assetType) {
    if (assetType == "house") {
      hashtag += `%23maison `;
    } else if (assetType == "building") {
      hashtag += `%23immeuble `;
    } else if (assetType == "apartment") {
      hashtag += `%23appartement `;
    } else if (assetType == "land") {
      hashtag += `%23terrain `;
    }
  }

  //First, the first pic
  let photoUrl = images[0].url;
  const urlToUpload = `https://graph.facebook.com/${version}/${pageId}/media?image_url=${photoUrl}&caption=${description}${hashtag}&access_token=${token}`;
  const container = await fetchDataFunction(urlToUpload);
  return container;
};

exports.publishContainerInstagram = async ({
  containerId,
  version,
  pageId,
  token,
}) => {
  const urlToUpload = `https://graph.facebook.com/${version}/${pageId}/media_publish?creation_id=${containerId}&access_token=${token}`;
  const result = await fetchDataFunction(urlToUpload);
  return result;
};
// end test instagram

// upload
const fetchDataFunction = async (url) => {
  let data;
  await fetch(url, {
    method: "POST",
  }).then(async (response) => {
    data = await response.json();
  });
  return data;
};

// watermark
exports.addSoldWatermark = (images) => {
  images?.forEach((x) => {
    if (cloudinary?.watermark_sale) {
      x.url = x.url.replace(cloudinary?.watermark_sale, ""); // Need to remove the current watermark
      //legacy (better to do this with regex.....)
      // x.url = x.url.replace("", "");
    }

    if (cloudinary?.watermark_sale) {
      x.url = x.url.replace(
        "image/upload/",
        `image/upload/${cloudinary?.watermark_sold}`
      ); // Need to add the  watermark SOLD
    }
    // Pour info
    // watermark_sale: "c_scale,l_logos:logoano.png,w_300,o_20",
    // https://res.cloudinary.com/anoa-immo/image/upload/c_scale,g_south,l_logos:logoano.png,w_300,o_30/v1651979955/properties/itm13b.jpg
  });
  return images;
};

exports.addRentedWatermark = (images) => {
  images?.forEach((x) => {
    if (cloudinary?.watermark) {
      x.url = x.url.replace(cloudinary?.watermark, ""); // Need to remove the current watermark
      //legacy (better to do this with regex.....)
      // x.url = x.url.replace("", "");
    }

    if (cloudinary?.watermark_rented) {
      x.url = x.url.replace(
        "image/upload/",
        `image/upload/${cloudinary?.watermark_rented}`
      ); // Need to add the  watermark SOLD
    }
  });

  return images;
};

// Adding the watermark on array of images (both sale and rent)
exports.addWatermark_images = (images) => {
  images?.forEach((x) => {
    x.url = addWatermark_url(x.url);
  });
  return images;
};

// Adding the watermark on image url (both sale and rent)
const addWatermark_url = (url: string) => {
  const local = Meteor.absoluteUrl() == "http://localhost:3000/";

  const watermark = local
    ? cloudinary.watermark_local
    : cloudinary.watermark_sale;

  let result = url;

  // Adding watermark and f_auto for WebP (should not use otherwise users cannot download the image with the watermark)
  if (watermark && !url.includes(watermark)) {
    result = url.replace("/image/upload/", `/image/upload/${watermark}/`);
  } else {
    // secure_url_watermark = secure_url.replace(
    //   "/image/upload/",
    //   `/image/upload/f_auto/`
    // );
  }

  return result || url;
};

exports.addWatermark_url = addWatermark_url;
