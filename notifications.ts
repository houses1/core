// Discovered from the page: https://www.jqueryscript.net/blog/Best-Toast-Notification-jQuery-Plugins.html
// https://github.com/apvarun/toastify-js

import Toastify from "toastify-js";

export const saved_notification = (text = "Enregistré") => {
  return Toastify({
    text: text,
    duration: 3000,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    style: { background: "#00b09b" },
  }).showToast();
};

export const error_notification = (text = "Erreur") => {
  return Toastify({
    text: text,
    duration: 4000,
    close: true,
    gravity: "top", // `top` or `bottom`
    position: "right", // `left`, `center` or `right`
    style: { background: "#dc3545" },
  }).showToast();
};
