import { saved_notification, error_notification } from "../../../notifications";

// import enterprise_customization from "../../../../enterprise_customization.ts";

export let isPublishable = (item) => {
  if (!item.title) {
    error_notification("Il manque un titre");
    throw new Meteor.Error("field missing");
  }

  if (!item.address) {
    error_notification("Il manque l'adresse");
    throw new Meteor.Error("field missing");
  }

  if (!item.saleprice) {
    error_notification("Il manque le prix");
    throw new Meteor.Error("field missing");
  }

  if (!item.images) {
    error_notification("Il manque une image/photo");
    throw new Meteor.Error("field missing");
  }

  // if (enterprise_customization.countryConfiguration == "fr") {
  //   if (!item.diagnostics) {
  //     error_notification("Il manque le diagnostic");
  //     throw new Meteor.Error("field missing");
  //   }
  // }

  return;
};
