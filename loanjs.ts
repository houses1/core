/*
 * Originally LoanJS modified by Vaan
 * Calculating loan in equal or diminishing installments
 * https://github.com/kfiku/LoanJS
 */

import dayjs from "dayjs";
// require("dayjs/locale/fr");
// dayjs.locale("fr"); // use locale globally

function rnd(num) {
  return Math.round(num * 100) / 100;
}

const getNextInstalment = ({
  i,
  amount,
  installmentsNumber,
  interestRate,
  insuranceRate,
  diminishing,
  capitalSum,
  interestSum,
}) => {
  let capital, interest, insurance, installment, irmPow;
  const interestRateMonth = interestRate / (12 * 100);
  const insuranceRateMonth = insuranceRate / (12 * 100);
  const totalInterestRateMonth = interestRateMonth + insuranceRateMonth;

  if (diminishing || interestRate == 0) {
    capital = rnd(amount / installmentsNumber);
    interest = rnd((amount - capitalSum) * interestRateMonth);
    insurance = rnd((amount - capitalSum) * insuranceRateMonth);
    installment = capital + interest - insurance;
  } else {
    irmPow = Math.pow(1 + totalInterestRateMonth, installmentsNumber);
    installment = amount * ((totalInterestRateMonth * irmPow) / (irmPow - 1));
    interest = (amount - capitalSum) * interestRateMonth;
    insurance = (amount - capitalSum) * insuranceRateMonth;
    capital = installment - interest - insurance;
  }

  return {
    capital: rnd(capital),
    interest: rnd(interest),
    installment: rnd(installment),
    remain: rnd(amount - capitalSum - capital),
    interestSum: rnd(interestSum + interest),
    insurance: rnd(insurance),
  };
};

exports.Loan = ({
  amount,
  installmentsNumber,
  interestRate = 0,
  insuranceRate = 0,
  diminishing,
  startDate,
}) => {
  //make sure that diminishing is a boolean
  diminishing = diminishing ? true : false;

  /** Checking params */
  if (amount < 0 || installmentsNumber < 0 || interestRate < 0) {
    throw new Error(
      "wrong parameters: "
        .concat(amount, " ")
        .concat(installmentsNumber, " ")
        .concat(interestRate)
    );
  }

  let installments = [];
  let interestSum = 0;
  let capitalSum = 0;
  let sum = 0;

  for (let i = 0; i < installmentsNumber; i++) {
    let inst = getNextInstalment({
      i,
      amount,
      installmentsNumber,
      interestRate,
      insuranceRate,
      diminishing,
      capitalSum,
      interestSum,
    });

    // inst.paymentDate = startDate;
    inst.paymentDate = dayjs(startDate).add(i, "month").format("DD/MM/YYYY");

    sum += inst.installment;
    capitalSum += inst.capital;
    interestSum += inst.interest;
    /** adding lost sum on rounding */

    if (i === installmentsNumber - 1) {
      capitalSum += inst.remain;
      sum += inst.remain;
      inst.remain = 0;
    }
    installments.push(inst);
  }

  return {
    installments: installments,
    amount: rnd(amount),
    interestSum: rnd(interestSum),
    capitalSum: rnd(capitalSum),
    sum: rnd(sum),
  };
};
